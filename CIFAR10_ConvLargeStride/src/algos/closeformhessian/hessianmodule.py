import os, sys
import torch
import numpy as np
from .utils import *
from .dp import *
from .decomposition import *
from .measures import Measures
from .decomposition import Decomp

from .tsa import *
from .visualization import vis
from .layerwisefeature import IntermediateFeatureExtractor

class HessianModule():

    def __init__(self, net, dataset, fc_seq, use_gpu=True, RAM_cap=64, net_eval=True, net_prepare=True, device=None, remain_labels=None, on_device_dataloader=True):
        
        self.dataset = dataset
        if not net_prepare:
            assert device is not None
            self.device = device
        else:
            net, self.device, _ = prepare_net(net, use_gpu)
        if net_eval:
            net.eval()

        self.fc_seq = fc_seq
        self.sd = net.state_dict()
        self.ife = IntermediateFeatureExtractor(net, self.fc_seq)

        self.Ws = None
        self.RAM_cap = RAM_cap * (2 ** 30)
        self.load_Ws()

        self.measure = Measures(self.device)
        self.vis = vis(self.device)
        self.utils = Utils(self, self.device)
        self.decomp = Decomp()
        self.tsa = TSA()

        self.cache = {}
        self.remain_labels = remain_labels
        self.dl = OnDeviceDataLoader(self.dataset, 2048, self.device)

        self.tsa_sm = self.tsa.stats(self, fc_seq, 'cpu')
        gpu_memory()
        print("Layers to be evaluated: {}".format(fc_seq))
    
    def load_Ws(self):
        sample_data = sample_input(self.dataset)[0].to(self.device)
        _, sample_out = self.ife(sample_data)
        self.Ws = weight_load(self.ife.net.state_dict(), self.fc_seq, sample_out)
        del sample_data, sample_out
        empty_cache(self.device)

    def load_sd(self, sd):
        log("Loaded state dict")
        self.ife.net.load_state_dict(sd)
        self.clear_cache()
        self.load_Ws()
    
    def set_remain_labels(self, remain_labels):
        self.dl.set_remain_labels(remain_labels)
    
    def clear_cache(self):
        log('cache cleared')
        self.cache = {}
    
    def config_stats_module(self, comp_layers, out_device):
        self.tsa_sm.comp_layers = comp_layers
        self.tsa_sm.out_device = out_device

    def sample_output(self, func, comp_layers, sample_count=1, out_device='cpu', batch_sum=False, **kwargs):
        inputs = sample_input(self.dataset, sample_count, remain_labels=self.remain_labels).to(self.device)
        return func(self.ife, comp_layers, inputs, self.device, self.Ws, out_device=out_device, batch_sum=batch_sum, **kwargs)
    
    def expectation(self, func, comp_layers, out_device='cpu', dscrop=1, from_cache=True, to_cache=True, print_log=True, **kwargs):
        self.config_stats_module(comp_layers, out_device)
        return self.tsa_sm.expectation(func, dscrop=dscrop, from_cache=from_cache, to_cache=to_cache, print_log=print_log, **kwargs)
    
    def variance(self, func, comp_layers, out_device, dscrop=1, from_cache=True, to_cache=True, print_log=True, **kwargs):
        self.config_stats_module(comp_layers, out_device)
        return self.tsa_sm.variance(func, dscrop=dscrop, from_cache=from_cache, to_cache=to_cache, print_log=print_log, **kwargs)

    def covariance(self, func, comp_layers, dim, out_device='cpu', dscrop=1, from_cache=True, to_cache=True, print_log=True, **kwargs):
        """row vectors: dim=1, col vectors: dim=0"""
        self.config_stats_module(comp_layers, out_device)
        return self.tsa_sm.covariance_matrix(func, dim, dscrop=dscrop, from_cache=from_cache, to_cache=to_cache, print_log=print_log, **kwargs)

    def get_HDC_iterator(self, comp_layers, sample_func, out_device, dataset_crop=1):
        batchsize = self.utils.auto_batch_size(sample_func, comp_layers)
        ds = crop_dataset(self.dataset, dataset_crop)
        dl = dataloader_gen(ds, batchsize, remain_labels=self.remain_labels)
        HDC = HDC_iterator(dl, sample_func, self.device, comp_layers, out_device)
        return HDC
        
    def E_UxT(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        ret = self.expectation(self.decomp.UxT_comp, comp_layers, out_device=out_device, dscrop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        return ret

    def E_UxT_norm(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        ret = self.expectation(self.decomp.UxT_norm_comp, comp_layers, out_device=out_device, dscrop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        return ret
    
    def E_UxT_dotexp(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        E_UxTs = self.E_UxT(comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        ret = {}
        for layer in comp_layers:
            E_UxT = E_UxTs[layer]
            ret[layer] = E_UxT.matmul(E_UxT.transpose(-1,-2))
        return ret
    
    def E_UxT_norm_dotexp(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        E_UxTs = self.E_UxT_norm(comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        ret = {}
        for layer in comp_layers:
            E_UxT = E_UxTs[layer]
            ret[layer] = E_UxT.matmul(E_UxT.transpose(-1,-2))
        return ret

    def E_UxT_cosexp(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        E_UxTs = self.E_UxT_norm(comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        ret = {}
        for layer in comp_layers:
            E_UxT = E_UxTs[layer]
            E_UxT = E_UxT.div_(E_UxT.norm(dim=-1).unsqueeze(-1))
            ret[layer] = E_UxT.matmul(E_UxT.transpose(-1,-2))
        return ret

    def E_U_dotexp(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        E_Us = self.E_U(comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        ret = {}
        for layer in comp_layers:
            E_U = E_Us[layer]
            ret[layer] = E_U.matmul(E_U.transpose(-1,-2))
        return ret

    def E_U_norm_dotexp(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        E_Us = self.E_U_norm(comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        ret = {}
        for layer in comp_layers:
            E_U = E_Us[layer]
            ret[layer] = E_U.matmul(E_U.transpose(-1,-2))
        return ret

    def E_U_cosexp(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        E_Us = self.E_U_norm(comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache)
        ret = {}
        for layer in comp_layers:
            E_U = E_Us[layer]
            E_U = E_U.div_(E_U.norm(dim=-1).unsqueeze(-1))
            ret[layer] = E_U.matmul(E_U.transpose(-1,-2))
        return ret

    def E_UxT_gram(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        ret = self.expectation('UxT_gram', func_gram(UxT_comp), self.UxT_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
        return ret

    def E_UxT_norm_gram(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        ret = self.expectation('UxT_norm_gram', func_gram(UxT_norm_comp), self.UxT_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
        return ret

    def E_U_gram(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        ret = self.expectation('U_gram', func_gram(U_comp), self.U_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
        return ret

    def E_U_norm_gram(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        ret = self.expectation('U_norm_gram', func_gram(U_norm_comp), self.U_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
        return ret

    def Cov_UxT(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        return self.covariance('UxT', UxT_comp, self.UxT_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
    
    def Cov_UxT_norm(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        return self.covariance('UxT_norm', UxT_norm_comp, self.UxT_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
    
    def Cov_U(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        return self.covariance('U', U_comp, self.U_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)

    def Cov_U_norm(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        return self.covariance('U_norm', U_norm_comp, self.U_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
    
    def CovN_UxT_norm(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        return self.covariance_norm('UxT', UxT_norm_comp, self.UxT_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)
    
    def CovN_U_norm(self, comp_layers, out_device='cpu', dataset_crop=1, reload_cache=False, to_cache=True):
        return self.covariance_norm('U', U_norm_comp, self.U_sample, comp_layers, out_device=out_device, dataset_crop=dataset_crop, reload_cache=reload_cache, to_cache=to_cache, single=False)

    def eigenthings_tensor(self, t, device=None, out_device='cpu', symmetric=False, topn=-1):
        return eigenthings_tensor_utils(t, device, out_device, symmetric, topn)
    
    def hessian_eigenthings_estimate(self, comp_layers, out_device='cpu', dataset_crop=1, num_eigenthings=100, seed=None):
        torch.manual_seed(get_time_seed() if seed is None else seed)
        E_UTAUs = self.E_UTAU(comp_layers, out_device, dataset_crop)
        E_xxTs = self.E_xxT(comp_layers, out_device, dataset_crop)
        ret = {layer: eigenthings_exp_hessian_approx(E_UTAUs[layer], E_xxTs[layer], num_eigenthings, self.device, out_device) for layer in comp_layers}
        return ret

    def inp_hessian_eigenthings(self, comp_layers, inputs, out_device='cpu', num_eigenthings=100, timer_on=False):
        st = datetime.datetime.now()
        E_UTAUs = self.UTAU_sample(comp_layers, out_device=out_device, inputs=inputs, batch_sum=True)
        E_xxTs = self.xxT_sample(comp_layers, out_device=out_device, inputs=inputs, batch_sum=True)
        ret = {layer: eigenthings_exp_hessian_approx(E_UTAUs[layer], E_xxTs[layer], num_eigenthings, self.device, out_device, timer_on=timer_on, symmetric_exact=True) for layer in comp_layers}
        return ret
    