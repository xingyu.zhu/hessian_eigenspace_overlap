#!/bin/bash
python3 sbatch_run.py -x="train-s" --exclude=gpu-compute[1-3],linux56 -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_1/models/epoch0.pth'"
python3 sbatch_run.py -x="train-s" --exclude=gpu-compute[1-3],linux56 -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_2/models/epoch0.pth'"
python3 sbatch_run.py -x="train-s" --exclude=gpu-compute[1-3],linux56 -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_3/models/epoch0.pth'"
python3 sbatch_run.py -x="train-s" --exclude=gpu-compute[1-3],linux56 -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_4/models/epoch0.pth'"
python3 sbatch_run.py -x="train-s" --exclude=gpu-compute[1-3],linux56 -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_5/models/epoch0.pth'"