import os, sys
import torch
import numpy as np

dirc = '../LeNet5_BN_nl_fixlr0.01'
dirc = '../LeNet5_fixlr0.01/experiment_log/run_{}/models/final.pth'
runs = list(range(1, 6))

sds = [torch.load(dirc.format(run), map_location='cpu') for run in runs]


def sim(layer):
    print(sds[0][layer].shape)
    dots = []
    for i in range(len(sds)):
        for j in range(i):
            v1 = sds[i][layer].view(-1)
            v1.div_(v1.norm())
            v2 = sds[j][layer].view(-1)
            v2.div_(v2.norm())
            dots.append(v1.dot(v2))
    return dots

for k in sds[0]:
    if 'weight' in k:
        print(k)
        dots = sim(k)
        print(dots)
        print(np.mean(np.abs(dots)))