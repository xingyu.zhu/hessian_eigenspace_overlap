#!/bin/bash
python3 sbatch_run.py -x="train-s" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_1/models/epoch0.pth' -lw" --specialinfo="init_eval"
python3 sbatch_run.py -x="train-s" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_2/models/epoch0.pth' -lw" --specialinfo="init_eval"
python3 sbatch_run.py -x="train-s" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_3/models/epoch0.pth' -lw" --specialinfo="init_eval"
python3 sbatch_run.py -x="train-s" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_4/models/epoch0.pth' -lw" --specialinfo="init_eval"
python3 sbatch_run.py -x="train-s" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_5/models/epoch0.pth' -lw" --specialinfo="init_eval"