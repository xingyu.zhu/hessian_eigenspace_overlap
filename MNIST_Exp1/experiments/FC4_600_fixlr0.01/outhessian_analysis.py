import os, sys
import torch
import numpy as np
sys.path.append(os.getcwd())
from config import Config # pylint: disable=no-name-in-module
from collections import OrderedDict
from algos.closeformhessian import hessianmodule

sd = "./experiment_log/run_1/models/final.pth"
sd = "./experiment_log/run_1/models/epoch0.pth"
exp_name = os.getcwd().split('/')[-1]
device = 'cuda' if torch.cuda.is_available() else 'cpu'
conf = Config()
net = conf.net()
net.load_state_dict(torch.load(sd, map_location=device))
dataset = conf.dataset(train=True, transform=conf.test_transform)

fc_seq = ['fc1', 'fc2', 'fc3']
HM = hessianmodule.HessianModule(net, dataset, fc_seq, RAM_cap=64)
comp_layers = fc_seq
c = HM.Ws[0].shape[0]

W_chains = [torch.eye(c, device=device)] #pylint:disable=no-member
for i, layer in enumerate(fc_seq[:-1]):
    W_chains.append(torch.Tensor.matmul(W_chains[-1], HM.Ws[i]))

W_chain_dict = {}
for i in range(len(fc_seq)):
    print(fc_seq[i], W_chains[-i-1].shape)
    W_chain_dict[fc_seq[i]] = W_chains[-i-1]

def remove_all_one(U):
    # print(U.shape)
    v = torch.Tensor.sum(U, dim=0)
    v /= torch.Tensor.norm(v)

    basis = [v]
    for w in U:
        # print(len(basis))
        w_ = w / torch.norm(w)
        for basis_v in basis:
            w_ -= torch.Tensor.dot(w_, basis_v) * basis_v
        # print(torch.Tensor.norm(w_))
        w_ /= torch.norm(w_)
        # for basis_v in basis:
            # print(torch.Tensor.dot(w_, basis_v))
        basis.append(w_)
    return torch.cat([v.unsqueeze(0) for v in basis[1:-1]], dim=0)

def trace_overlap(s1, s2, device):
    M = s1.to(device)
    V = s2.to(device)
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    n = M.size()[0]
    k = 0
    for i in range(n):
        vi = V[i]
        li = Mt.mv(M.mv(vi))
        ki = torch.dot(li, li) # pylint: disable=no-member
        k += ki
    del Mt, M, V
    torch.cuda.empty_cache()
    return float((k / n).cpu().numpy())

E_A = HM.expectation(HM.decomp.A_comp, fc_seq)[fc_seq[0]]
E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, fc_seq)

for i, layer in enumerate(fc_seq):
    print(layer)
    UTAU = E_UTAUs[layer]
    print(UTAU.shape)
    U_M, S_M, V_M = torch.svd(UTAU)
    # print(S_M)
    # exit()
    W_chain = W_chain_dict[layer]
    est_eigenbasis = remove_all_one(W_chain)
    M_eigenspace = U_M.transpose(0,1)[:c-1]

    layer_number = len(fc_seq) - i - 1 # 0 for the last layer
    est_UTAU = torch.transpose(W_chain, 0, 1).matmul(E_A).matmul(W_chain) / (4 ** layer_number)
    print(est_UTAU.shape)
    U_Me, S_Me, V_Me = torch.svd(est_UTAU)
    print(S_M[:c] / S_Me[:c])
    # print((S_Me - S_M) / S_M)
    # print(est_eigenbasis)
    # v1 = torch.sum(W_chain, dim=0)
    # v1 /= torch.Tensor.norm(v1)
    # for v in M_eigenspace:
    #     print(torch.Tensor.dot(v, v1))
    overlap = trace_overlap(M_eigenspace, est_eigenbasis, device)
    print(overlap)

    overlap2 = trace_overlap(M_eigenspace, U_Me.transpose(0,1)[:c-1], device)
    print(overlap2)