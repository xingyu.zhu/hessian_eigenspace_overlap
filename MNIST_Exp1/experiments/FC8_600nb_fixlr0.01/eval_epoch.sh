#!/bin/bash
python3 sbatch_run.py -x="train-l" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_1/models/final.pth' -lw" --specialinfo="final_eval"
python3 sbatch_run.py -x="train-l" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_2/models/final.pth' -lw" --specialinfo="final_eval"
python3 sbatch_run.py -x="train-l" -r "python3 eval_epoch.py -n 300 -m './experiment_log/run_3/models/final.pth' -lw" --specialinfo="final_eval"