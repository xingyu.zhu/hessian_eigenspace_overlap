import os, sys
import torch
import numpy as np
sys.path.append(os.getcwd())
from collections import OrderedDict
from config import Config # pylint: disable=no-name-in-module
from algos.closeformhessian import hessianmodule

sd = "./experiment_log/run_2/models/final.pth"
real_comp = "./experiment_log/run_2/models/final.pth_LW_ET1000.eval"
exp_name = os.getcwd().split('/')[-1]

device = 'cuda'
conf = Config()
net = conf.net()
net.load_state_dict(torch.load(sd, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, ['fc1', 'fc2', 'fc3'], RAM_cap=64)
#comp_layers = ['fc1', 'fc2', 'fc3']
comp_layers = ['fc1']

eigenthings_real = torch.load(real_comp, map_location='cpu')
eigenvals_real = eigenthings_real['eigenvals_layer']
eigenvecs_real = eigenthings_real['eigenvecs_layer']

def UTL_UTAU_overlap(n, comp_layers, mat=True):
    E_UTLs = HM.E_UTL(comp_layers, dataset_crop=1)
    E_UTAUs = HM.E_UTAU(comp_layers, dataset_crop=1)
    E_L = HM.E_L(comp_layers, dataset_crop=1)
    E_Us = HM.E_U(comp_layers, dataset_crop=1)
    mats_UTAU, mats_UTLmt, mats_EUTELmt = [], [], []
    descs_UTAU, descs_UTLmt, descs_EUTELmt = [], [], []
    name_mat = "UTL_UTAU_{}".format(exp_name)
    overlap_xs, overlap_ys, overlap2_ys, overlap3_ys = [], [], [], []
    name_overlap = 'UTL_UTAU_overlap_{}_{}'.format(exp_name, n)
    name_overlap2 = 'EUTEL_UTAU_overlap_{}_{}'.format(exp_name, n)
    name_overlap3 = 'UTL_EUTEL_overlap_{}_{}'.format(exp_name, n)
    for layer in comp_layers:
        E_U = E_Us[layer]
        E_UTE_L = E_U.transpose(0,1).matmul(E_L) 
        E_UTE_Lmt = E_UTE_L.matmul(E_UTE_L.transpose(0,1))
        E_UTL = E_UTLs[layer]
        E_UTLmt = E_UTL.matmul(E_UTL.transpose(0,1))
        E_UTAU = E_UTAUs[layer]
        _, E_UTAU_eig = torch.symeig(E_UTAU.to(device), eigenvectors=True)
        _, E_UTLmt_eig = torch.symeig(E_UTLmt.to(device), eigenvectors=True)
        _, E_UTE_Lmt_eig = torch.symeig(E_UTE_Lmt.to(device), eigenvectors=True)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, n, r=1.05)
        print(overlap_dim_x)
        overlap_dim_y = HM.measure.trace_overlap_trend(E_UTAU_eig.transpose(0,1).flip(0), E_UTLmt_eig.transpose(0,1).flip(0), overlap_dim_x)
        overlap2_dim_y = HM.measure.trace_overlap_trend(E_UTAU_eig.transpose(0,1).flip(0), E_UTE_Lmt_eig.transpose(0,1).flip(0), overlap_dim_x)
        overlap3_dim_y = HM.measure.trace_overlap_trend(E_UTLmt_eig.transpose(0,1).flip(0), E_UTE_Lmt_eig.transpose(0,1).flip(0), overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
        overlap2_ys.append(overlap2_dim_y)
        overlap3_ys.append(overlap3_dim_y)
        mats_UTAU += [E_UTAU]
        descs_UTAU += ['E[U^TAU]_{}'.format(layer)]
        mats_UTLmt += [E_UTLmt]
        descs_UTLmt += ['E[U^TL]E[U^TL]^T_{}'.format(layer)]
        mats_EUTELmt += [E_UTE_Lmt]
        descs_EUTELmt += ['E[U^T]E[L](E[U^T]E[L])^T_{}'.format(layer)]

    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name_overlap, dpi=150)
    HM.vis.plots(overlap_xs, overlap2_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name_overlap2, dpi=150)
    HM.vis.plots(overlap_xs, overlap3_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name_overlap3, dpi=150)
    mats = [mats_UTAU, mats_UTLmt, mats_EUTELmt]
    descs = [descs_UTAU, descs_UTLmt, descs_EUTELmt]
    if mat:
        HM.vis.plot_matsvd(mats, descs, name_mat, colormap='viridis')

UTL_UTAU_overlap(120, comp_layers, mat=False)