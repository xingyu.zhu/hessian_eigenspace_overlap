import os, sys
sys.path.append(os.getcwd())
import torch
import torchvision
import numpy as np
import datetime
from torch.utils.data import DataLoader
# from torchvision.models._utils import IntermediateLayerGetter
from algos.layerwise_feature import IntermediateFeatureExtractor
from config import Config # pylint: disable=no-name-in-module
from utils import prepare_net, get_image_path

import matplotlib as mpl
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
mpl.use('Agg')

def log(info):
    print("[{}] {}".format(datetime.datetime.now(), info))

conf = Config()
model = conf.net()
model, device, _ = prepare_net(model, use_gpu=True)
dataset = conf.dataset(train=True, transform=conf.test_transform)
data_loader = DataLoader(dataset, batch_size=conf.testing_batchsize, shuffle=True, num_workers=conf.test_provider_count)

def load_eval(eval_path, device):
    meta = torch.load(eval_path, map_location='cpu')
    meta['gen_acc'] = -meta['gen_gap']['acc']
    meta['gen_loss'] = meta['gen_gap']['loss']
    meta['eigenvecs'] = torch.from_numpy(meta["eigenvecs"]).to('cpu') # pylint: disable=no-member
    if 'layerinfo' in meta:
        layer_info = meta['layerinfo']
        for layer in layer_info:
            meta['eigenvecs_layer'][layer] = torch.from_numpy(meta['eigenvecs_layer'][layer]).to('cpu')
    return meta

def overlap_calc(subspace1, subspace2, device):
    M = subspace1.to(device)
    V = subspace2.to(device)
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    n = M.size()[0]
    k = 0
    for i in range(n):
        vi = V[i]
        li = Mt.mv(M.mv(vi))
        ki = torch.dot(li, li) # pylint: disable=no-member
        k += ki
    del Mt, M, V
    torch.cuda.empty_cache()
    return (k / n).to('cpu').numpy() 

def matrix_diag(diagonal):
    N = diagonal.shape[-1]
    shape = diagonal.shape[:-1] + (N, N)
    device, dtype = diagonal.device, diagonal.dtype
    result = torch.zeros(shape, dtype=dtype, device=device) # pylint: disable=no-member
    indices = torch.arange(result.numel(), device=device).reshape(shape) # pylint: disable=no-member
    indices = indices.diagonal(dim1=-2, dim2=-1)
    result.view(-1)[indices] = diagonal
    return result

def sample_midlayer(ife, data_loader, sample_size=1):
    inputs, _ = iter(data_loader).next()
    inputs = inputs[:sample_size].to(device)
    mid_out, final_out = ife(inputs)
    return mid_out, final_out

def E_xxT_comp(net: torch.nn.Module, fc_seq):
    ife = IntermediateFeatureExtractor(net, fc_seq)#, 'fc1':'fc1'})
    batch_sums = {layer:[] for layer in fc_seq}
    for i, (inputs, _) in enumerate(data_loader):
        inputs = inputs.to(device)
        mid_out, final_out = ife(inputs)
        for layer in mid_out.keys():
            feat_in, _ = mid_out[layer][0], mid_out[layer][1]
            feat_in_vectorized = feat_in.view(feat_in.size()[0], -1)
            feat_in_ext = feat_in_vectorized.unsqueeze(1)
            batch_E_xxT = torch.matmul(feat_in_ext.transpose(1, 2), feat_in_ext).sum(axis=[0]).unsqueeze(0) # pylint: disable=no-member
            batch_sums[layer].append(batch_E_xxT)
    ret = {}
    for layer in fc_seq:
        E_xxT = torch.cat(batch_sums[layer]).sum(axis=0) / len(dataset) # pylint: disable=no-member
        ret[layer] = E_xxT
    return ret

def xxT_sample(net: torch.nn.Module, fc_seq, sample_size):
    ife = IntermediateFeatureExtractor(net, fc_seq)#, 'fc1':'fc1'})
    mid_out, _ = sample_midlayer(ife, data_loader, sample_size)
    ret = {}
    for layer in mid_out.keys():
        feat_in, _ = mid_out[layer][0], mid_out[layer][1]
        feat_in_vectorized = feat_in.view(sample_size, -1)
        feat_in_ext = feat_in_vectorized.unsqueeze(1)
        xxTs = torch.matmul(feat_in_ext.transpose(1, 2), feat_in_ext) # pylint: disable=no-member
        ret[layer] = xxTs
    return ret

def E_xxT_analysis(net, fc_seq):
    print(fc_seq)
    sample_size = 2
    rank = 10
    info = 'epoch100'

    E_xxT_vars = E_xxT_comp(net, fc_seq)
    xxT_sample_vars = xxT_sample(net, fc_seq, sample_size)
    for var in fc_seq:
        E_xxT = E_xxT_vars[var]
        xxT_samples = xxT_sample_vars[var]

        mats = [E_xxT] + [m for m in xxT_samples]
        descs = ['E_xxt'] + ['sample{}'.format(i) for i in range(len(xxT_samples))]

def plot_E_xxT(layer, xxTs, eigenvals, descs, info):
    im_name = "{}_{}_E_xxT_{}".format(os.getcwd().split('/')[-1], layer, info)
    plt.figure(figsize=(9, 6 * len(xxTs)))
    gs = gridspec.GridSpec(len(xxTs), 2, width_ratios=[2, 1])
    for i in range(len(xxTs)):
        plt.subplot(gs[i, 0])
        plt.imshow(xxTs[i].to('cpu').numpy())
        plt.title('_'.join([os.getcwd().split('/')[-1], layer, descs[i], info]))
        plt.subplot(gs[i, 1])
        plt.scatter(np.arange(1, len(eigenvals[i]) + 1), eigenvals[i])
        plt.title('Eigenvalues')
    plt.tight_layout()
    plt.savefig(get_image_path(im_name), dpi=150)

def UTAU_comp(ife, Ws, fc_seq, inputs):

    softmax = torch.nn.Softmax(dim=1).to(device)
    inputs = inputs.to(device)
    mid_out, final_out = ife(inputs)
    p = softmax(final_out)

    diag_p = matrix_diag(p)
    p_mat = p.unsqueeze(1)
    ppTs = torch.matmul(p_mat.transpose(1, 2), p_mat) # pylint: disable=no-member
    A = diag_p - ppTs

    Cs = []
    for layer in fc_seq:
        feat_in, feat_out = mid_out[layer][0], mid_out[layer][1]
        Cs.append(matrix_diag((feat_out >= 0).float()))
    
    Us = []
    batch_identity = torch.eye(diag_p.size()[-1]).unsqueeze(0).repeat(diag_p.size()[0], 1, 1).to(device) # pylint: disable=no-member
    Us.append(batch_identity)
    for i in range(len(fc_seq) - 1):
        U_prev = Us[i]
        U_next = U_prev.matmul(Ws[i]).matmul(Cs[i + 1])
        Us.append(U_next)
    
    UTAUs = [Us[i].transpose(1, 2).matmul(A).matmul(Us[i]) for i in range(len(fc_seq))]
    return UTAUs, Us, Cs

def E_UTAU_comp(net, fc_seq):

    softmax = torch.nn.Softmax(dim=1).to(device)
    fc_seq.reverse()
    ife = IntermediateFeatureExtractor(net, fc_seq)
    _, final_out = sample_midlayer(ife, data_loader)
    class_count = final_out.size()[1]
    
    # Sanity Check
    sd = net.state_dict()
    Ws = [sd[layer + '.weight'] for layer in fc_seq]
    assert class_count == Ws[0].size()[0], (class_count, Ws[0].size(), fc_seq)
    for i in range(len(Ws) - 1):
        assert Ws[i].size()[1] == Ws[i + 1].size()[0], "{}-{} weight size mismatch".format(fc_seq[i], fc_seq[i + 1])
    
    # UT * diag(p) - pp^T * U
    sum_UTAUs = {layer: [] for layer in fc_seq}
    sum_Us = {layer: [] for layer in fc_seq}
    for i, (inputs, _) in enumerate(data_loader):
        UTAUs, Us, Cs = UTAU_comp(ife, Ws, fc_seq, inputs)
        for i in range(len(fc_seq)):
            sum_UTAUs[fc_seq[i]].append(UTAUs[i].sum(axis=0).unsqueeze(0))
            sum_Us[fc_seq[i]].append(Us[i].sum(axis=0).unsqueeze(0))
    
    E_UTAU = {}
    E_U = {}
    for layer in fc_seq:
        E_UTAU[layer] = torch.cat(sum_UTAUs[layer], axis=0).sum(axis=0) / len(dataset) # pylint: disable=no-member
        E_U[layer] = torch.cat(sum_Us[layer], axis=0).sum(axis=0) / len(dataset) # pylint: disable=no-member

    fc_seq.reverse()
    return E_UTAU, E_U

def UTAU_sample(net, fc_seq, sample_size=1):

    softmax = torch.nn.Softmax(dim=1).to(device)
    fc_seq.reverse()
    ife = IntermediateFeatureExtractor(net, fc_seq)
    _, final_out = sample_midlayer(ife, data_loader)
    class_count = final_out.size()[1]
    
    # Sanity Check
    sd = net.state_dict()
    Ws = [sd[layer + '.weight'] for layer in fc_seq]
    assert class_count == Ws[0].size()[0], (class_count, Ws[0].size(), fc_seq)
    for i in range(len(Ws) - 1):
        assert Ws[i].size()[1] == Ws[i + 1].size()[0], "{}-{} weight size mismatch".format(fc_seq[i], fc_seq[i + 1])
    
    # UT * diag(p) - pp^T * U
    sum_UTAUs = {layer: [] for layer in fc_seq}
    sum_Us = {layer: [] for layer in fc_seq}
    inputs, _ = iter(data_loader).next()
    inputs = inputs[:sample_size].to(device)
    UTAUs, Us, Cs = UTAU_comp(ife, Ws, fc_seq, inputs)
    UTAU_sample, U_sample = {}, {}
    for i, layer in enumerate(fc_seq):
        UTAU_sample[layer] = UTAUs[i]
        U_sample[layer] = Us[i]
    fc_seq.reverse()
    return UTAU_sample, U_sample

def E_UTAU_analysis(model, fc_seq):
    E_UTAU_vars, E_U_vars = E_UTAU_comp(model, fc_seq)
    descs = []
    mats = []
    im_name = "E_UTAU_epoch0_{}".format(os.getcwd().split('/')[-1])
    for layer in fc_seq:
        mats.append(E_UTAU_vars[layer])
        descs.append(layer)
    plot_matsvd(mats, descs, im_name)

def total_analysis(model, fc_seq, im_name, sample_size=1):
    E_UTAU_vars, E_U_vars = E_UTAU_comp(model, fc_seq)
    UTAU_sample_vars, U_sample_vars = UTAU_sample(model, fc_seq, sample_size)
    E_xxT_vars = E_xxT_comp(model, fc_seq)
    xxT_sample_vars = xxT_sample(model, fc_seq, sample_size)
    print("Calc Finished")

    mats_UTAU, mats_xxT, mats_U = [], [], []
    descs_UTAU, descs_xxT, descs_U = [], [], []
    for layer in fc_seq:
        mats_UTAU += ([E_UTAU_vars[layer]] + [UTAU_sample_vars[layer][i] for i in range(sample_size)])
        descs_UTAU += (['E[U^TAU]_{}'.format(layer)] + ['sample{}_U^TAU_{}'.format(i, layer) for i in range(sample_size)])
        mats_xxT += ([E_xxT_vars[layer]] + [xxT_sample_vars[layer][i] for i in range(sample_size)])
        descs_xxT += (['E[xx^T]_{}'.format(layer)] + ['sample{}_xx^T_{}'.format(i, layer) for i in range(sample_size)])
    
    mats = [mats_UTAU, mats_xxT]
    descs = [descs_UTAU, descs_xxT]
    plot_matsvd_2d(mats, descs, im_name)

def eigenthings_similarity(model, fc_seq, num_eigenthings, eval_meta, name):
    E_UTAU_vars, E_U_vars = E_UTAU_comp(model, fc_seq)
    E_xxT_vars = E_xxT_comp(model, fc_seq)
    log("Calc Finished")

    base_eigenvecs_vars = eval_meta['eigenvecs_layer']
    base_eigenvals_vars = eval_meta['eigenvals_layer']

    for var in fc_seq:
        E_UTAU, E_xxT = E_UTAU_vars[var], E_xxT_vars[var]
        eigenvals_approx, eigenvecs_approx = eigenthings_exp_hessian_approx(E_UTAU, E_xxT, num_eigenthings)
        eigenvals_base, eigenvecs_base = base_eigenvals_vars[var + '.weight'], base_eigenvecs_vars[var + '.weight']
        name_tmp = name + "eigenval_scatter_{}".format(var)
        plot_sim_scatters(eigenvals_approx, eigenvals_base, name_tmp)
        layer_shape = model.state_dict()[var + '.weight'].size()
    
        vec_dims = np.arange(1, 499, 20)
        overlaps = []
        for dim in vec_dims:
            overlap = overlap_calc(eigenvecs_approx[:dim], eigenvecs_base[:dim], device)
            overlaps.append(overlap)
        name_tmp = name + "overlaps_scatter_{}".format(var)
        plot_sim_overlap(vec_dims, overlaps, name_tmp)

        mats_base = []
        descs_base = []
        mats_approx = []
        descs_approx = []
        
        for i in range(5):
            mats_base.append(torch.abs(eigenvecs_base[i].view(layer_shape)))
            descs_base.append("Base l{}={:.4g} {}".format(i, eigenvals_base[i], var))
            mats_approx.append(torch.abs(eigenvecs_approx[i].view(layer_shape)))
            descs_approx.append("Approx l{}={:.4g} {}".format(i, eigenvals_approx[i], var))
        print(descs_base)
        plot_sim_matsvd_2d([mats_base, mats_approx], [descs_base, descs_approx], name + "sample_eigenvecs_{}".format(var))

def plot_sim_matsvd_2d(mats, descs, name):
    im_name = name
    plt.figure(figsize=(9 * len(mats), 4 * len(mats[0])))
    w_rt = []
    for i in range(len(mats)):
        w_rt += [2, 1]
    gs = gridspec.GridSpec(len(mats[0]), 2 * len(mats), width_ratios=w_rt)
    for i in range(len(mats[0])):
        for j in range(len(mats)):
            plt.subplot(gs[i, 2 * j])
            plt.imshow(mats[j][i].to('cpu').numpy(), cmap="Reds")
            plt.title(descs[j][i])
            plt.subplot(gs[i, 2 * j + 1])
            eigenvals = torch.svd(mats[j][i], compute_uv=False)[1].to('cpu').numpy() # pylint: disable=no-member
            plt.scatter(np.arange(1, len(eigenvals) + 1), eigenvals)
            plt.title('Eigenvalues')
    plt.tight_layout()
    plt.savefig(get_image_path(im_name), dpi=100)

def plot_sim_scatters(approx, base, name):
    plt.figure(figsize=(8, 6))
    plt.scatter(np.arange(1, len(approx) + 1), approx, label='Expectation Approx', s=6)
    plt.scatter(np.arange(1, len(base) + 1), base, label='Eigenthings Hessian', s=6)
    plt.legend()
    plt.title(name)
    plt.tight_layout()
    plt.savefig(get_image_path(name), dpi=150)

def plot_sim_overlap(vec_dims, overlaps, name):
    plt.figure(figsize=(8, 6))
    plt.scatter(vec_dims, overlaps, s=6)
    plt.title(name)
    plt.tight_layout()
    plt.savefig(get_image_path(name), dpi=150)

def eigenthings_exp_hessian_approx(E_UTAU, E_xxT, num_eigenthings):
    eigenvecs_UTAU, eigenvals_UTAU, _ = torch.svd(E_UTAU) # pylint: disable=no-member
    eigenvecs_xxT, eigenvals_xxT, _ = torch.svd(E_xxT) # pylint: disable=no-member
    eigenvals_UTAU = eigenvals_UTAU.to('cpu').numpy()
    eigenvals_xxT = eigenvals_xxT.to('cpu').numpy()
    kron_pairs = []
    for i in range(len(eigenvals_UTAU)):
        for j in range(len(eigenvals_xxT)):
            kron_pairs.append((i, j))
    log('pairs generated')
    kron_pairs.sort(key=lambda p: eigenvals_UTAU[p[0]] * eigenvals_xxT[p[1]], reverse=True)
    kron_pairs = kron_pairs[:num_eigenthings]
    log('sorted')
    eigenvals = [eigenvals_UTAU[p[0]] * eigenvals_xxT[p[1]] for p in kron_pairs]
    eigenvecs = []
    for p in kron_pairs:
        eigenvec_approx = eigenvecs_UTAU[p[0]].unsqueeze(1).matmul(eigenvecs_xxT[p[1]].unsqueeze(0)).view(-1)
        eigenvecs.append(eigenvec_approx.unsqueeze(0))
    eigenvecs = torch.cat(eigenvecs, axis=0) # pylint: disable=no-member
    return eigenvals, eigenvecs

def plot_matsvd(mats, descs, name):
    im_name = name
    plt.figure(figsize=(9, 6 * len(mats)))
    gs = gridspec.GridSpec(len(mats), 2, width_ratios=[2, 1])
    for i in range(len(mats)):
        plt.subplot(gs[i, 0])
        plt.imshow(mats[i].to('cpu').numpy())
        plt.title("{}_{}".format(descs[i], im_name))
        plt.subplot(gs[i, 1])
        eigenvals = torch.svd(mats[i], compute_uv=False)[1].to('cpu').numpy() # pylint: disable=no-member
        plt.scatter(np.arange(1, len(eigenvals) + 1), eigenvals)
        plt.title('Eigenvalues')
    plt.tight_layout()
    plt.savefig(get_image_path(im_name), dpi=150)

def plot_matsvd_2d(mats, descs, name):
    im_name = name
    plt.figure(figsize=(9 * len(mats), 6 * len(mats[0])))
    w_rt = []
    for i in range(len(mats)):
        w_rt += [2, 1]
    gs = gridspec.GridSpec(len(mats[0]), 2 * len(mats), width_ratios=w_rt)
    for i in range(len(mats[0])):
        for j in range(len(mats)):
            plt.subplot(gs[i, 2 * j])
            plt.imshow(mats[j][i].to('cpu').numpy())
            plt.title("{}_{}".format(descs[j][i], im_name))
            plt.subplot(gs[i, 2 * j + 1])
            eigenvals = torch.svd(mats[j][i], compute_uv=False)[1].to('cpu').numpy() # pylint: disable=no-member
            plt.scatter(np.arange(1, len(eigenvals) + 1), eigenvals)
            plt.title('Eigenvalues')
    plt.tight_layout()
    plt.savefig(get_image_path(im_name), dpi=100)

if __name__ == "__main__":
    # model_path = "./experiment_log/run_2/models/final.pth"
    model_path = "./experiment_log/run_2/models/epoch10000.pth"
    eval_meta = "./experiment_log/run_2/models/final.pth_LW_ET1000.eval"
    sd = torch.load(model_path, map_location=device)
    fc_seq = ['fc1','fc2','fc3']
    im_name = '{}_{}'.format(os.getcwd().split('/')[-1], model_path.split('/')[-1])
    model.load_state_dict(sd)
    # E_xxT_analysis(model, fc_seq)
    # E_UTAU_analysis(model, fc_seq)
    total_analysis(model, fc_seq, im_name, sample_size=1)
    # print(len(dataset))
    # eigenthings_similarity(model, fc_seq, num_eigenthings=1000, eval_meta=load_eval(eval_meta, 'cpu'), name=im_name)
