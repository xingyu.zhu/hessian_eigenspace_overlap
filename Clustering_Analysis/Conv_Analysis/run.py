import torch
import sys, os, copy
import numpy as np
from tools import file_fetch
from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.closeformhessian.visualization import vis
from algos.closeformhessian.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.closeformhessian.dp import OnDeviceDataLoader
from algos.closeformhessian.utils import *

from decomp.unfoldx import *

dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
layer_seq = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3']
comp_layers = ['conv1', 'conv2']

epoch = -1
run = 1
storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))
snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)

lw_hessian_file = snapshot_file + "_LW_ET1000.eval"
hessian_eval = torch.load(lw_hessian_file, map_location='cpu')
eigenvals, eigenvecs = hessian_eval['eigenvals_layer'], hessian_eval['eigenvecs_layer']
print(eigenvecs['conv1.weight'].shape)
exit()

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

dataset = conf.dataset(train=True, transform=conf.test_transform)
# dataloader = OnDeviceDataLoader(dataset, batchsize=2)
dataloader = DataLoader(dataset, 2048)

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
net, device, handle = prepare_net(net) # type: torch.nn.Module, str, str
vis_module = vis(device=device)
ife = IntermediateFeatureExtractor(net, layer_seq)

conv_modules = {layer: rgetattr(net, layer) for layer in comp_layers}

E_Bx = comp_E_Bx(dataloader, ife, device, conv_modules, comp_layers)#, max_count=10)
mats = [E_Bx[layer] for layer in comp_layers]
descs = [layer for layer in comp_layers]
vis_module.plot_matsvd_zero_centered(mats, descs, name='E[Bx]_' + exp_name, dpi=200)

E_BxBxT = comp_E_BxBxT(dataloader, ife, device, conv_modules, comp_layers)#, max_count=10)
mats = [E_BxBxT[layer] for layer in comp_layers]
descs = [layer for layer in comp_layers]
vis_module.plot_matsvd_zero_centered(mats, descs, name='E[BxBxT]_' + exp_name, dpi=200)

# for layer in comp_layers:
#     vis.plot_matsvd_zero_centered()
#     print(layer)
#     U, S, V = torch.Tensor.svd(E_Bx[layer], compute_uv=False)
#     print(S)

# print(E_Bx[comp_layers[0]])