import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def UxT_overlap(HM: HessianModule,
               comp_layers,
               exp_name,
               crop_ratio=1,
               ):

    crop_ratio = 1
    HDC = HM.get_HDC_iterator(comp_layers, HM.UxT_sample, out_device=HM.device, dataset_crop=crop_ratio)
    # Mean Comp

    sample_count = 0
    st = HM.utils.now()
    means = {}

    E_UxTs = HM.E_UxT(comp_layers, out_device=HM.device, dataset_crop=crop_ratio)
    for layer in comp_layers:
        E_UxT = E_UxTs[layer]
        E_UxT_norm = E_UxT.norm(dim=1, keepdim=True)
        E_UxT.div_(E_UxT_norm)
        E_UxT_norm = E_UxT.norm(dim=1, keepdim=True)
        print(E_UxT[0].dot(E_UxT[2]))
    exit()

    for i, X_batch in enumerate(HDC):
        bUxTs = X_batch
        sample_count += bUxTs[comp_layers[0]].shape[0]

        for layer in comp_layers:
            bUxT = bUxTs[layer]
            print(bUxT)
            exit()
            bUxT.div_(bUxT.norm(dim=2, keepdim=True))
            norm = bUxT.norm(dim=2)
            
            bUxT_sample = bUxT.index_select(0, idx)
            print(bUxT_sample.shape)
            dotprod = torch.mul(bUxT, bUxT_sample)
            print(dotprod.size())

            if layer not in means:
                means[layer] = dotprod
            else:
                means[layer] += dotprod
        if i in HDC.report_inds:
            est(st, (i + 1) / HDC.batch_count)
    exit()
        
    if not single:
        for layer in layers:
            ret[layer] /= sample_count
    else:
        ret /= sample_count
    timer(st, "\tDone Computing Expectation {} with {} samples".format(func.__name__, sample_count))
    return ret
    exit()

    bUxTs = HM.S_UxT(comp_layers, dataset_crop=crop_ratio)
    name = "UxT_overlap_{}".format(exp_name)

    mean = dict()
    variance = dict()
    
    
    print(mean)
    print(variance)

def UxT_dot_expectation(HM: HessianModule,
               comp_layers,
               exp_name,
               crop_ratio=1,
               ):
    dot = HM.E_UxT_dotexp(comp_layers, dataset_crop=crop_ratio)
    dot_norm = HM.E_UxT_norm_dotexp(comp_layers, dataset_crop=crop_ratio)
    cos = HM.E_UxT_cosexp(comp_layers, dataset_crop=crop_ratio)
    gram = HM.E_UxT_gram(comp_layers, dataset_crop=crop_ratio)
    gramn = HM.E_UxT_norm_gram(comp_layers, dataset_crop=crop_ratio)
    cov = HM.Cov_UxT(comp_layers, dataset_crop=crop_ratio)
    cov_norm = HM.Cov_UxT_norm(comp_layers, dataset_crop=crop_ratio)
    covn = HM.CovN_UxT_norm(comp_layers, dataset_crop=crop_ratio)
    mats_dot, mats_dot_norm, mats_cos, mats_gram, mats_gramn, mats_cov, mats_cov_norm, mats_covn = [], [], [], [], [], [], [], []
    descs_dot, descs_dot_norm, descs_cos, descs_gram, descs_gramn, descs_cov, descs_cov_norm, descs_covn = [], [], [], [], [], [], [], []
    name = 'U_dot_expectation_{}'.format(exp_name)
    for layer in comp_layers:
        print(layer)
        mats_dot.append(dot[layer])
        print(dot[layer])
        descs_dot.append('dot_{}'.format(layer))
        mats_dot_norm.append(dot_norm[layer])
        print(dot_norm[layer])
        descs_dot_norm.append('dot_norm_{}'.format(layer))
        mats_cos.append(cos[layer])
        print(cos[layer])
        descs_cos.append('cos_{}'.format(layer))
        mats_gram.append(gram[layer])
        print(gram[layer])
        descs_gram.append('gram_{}'.format(layer))
        mats_gramn.append(gramn[layer])
        print(gramn[layer])
        descs_gramn.append('gramn_{}'.format(layer))
        mats_cov.append(cov[layer])
        print(cov[layer])
        descs_cov.append('cov_{}'.format(layer))
        mats_cov_norm.append(cov_norm[layer])
        print(cov_norm[layer])
        descs_cov_norm.append('cov_norm_{}'.format(layer))
        mats_covn.append(covn[layer])
        print(covn[layer])
        descs_covn.append('covn_{}'.format(layer))

    mats = [mats_dot, mats_dot_norm, mats_cos, mats_gram, mats_gramn, mats_cov, mats_cov_norm, mats_covn]
    descs = [descs_dot, descs_dot_norm, descs_cos, descs_gram, descs_gramn, descs_cov, descs_cov_norm, descs_covn]
    HM.vis.plot_matsvd(mats, descs, name, colormap='viridis')

def U_dot_expectation(HM: HessianModule,
               comp_layers,
               exp_name,
               crop_ratio=1,
               ):
    dot = HM.E_U_dotexp(comp_layers, dataset_crop=crop_ratio)
    dot_norm = HM.E_U_norm_dotexp(comp_layers, dataset_crop=crop_ratio)
    cos = HM.E_U_cosexp(comp_layers, dataset_crop=crop_ratio)
    gram = HM.E_U_gram(comp_layers, dataset_crop=crop_ratio)
    gramn = HM.E_U_norm_gram(comp_layers, dataset_crop=crop_ratio)
    cov = HM.Cov_U(comp_layers, dataset_crop=crop_ratio)
    cov_norm = HM.Cov_U_norm(comp_layers, dataset_crop=crop_ratio)
    covn = HM.CovN_U_norm(comp_layers, dataset_crop=crop_ratio)
    mats_dot, mats_dot_norm, mats_cos, mats_gram, mats_gramn, mats_cov, mats_cov_norm, mats_covn = [], [], [], [], [], [], [], []
    descs_dot, descs_dot_norm, descs_cos, descs_gram, descs_gramn, descs_cov, descs_cov_norm, descs_covn = [], [], [], [], [], [], [], []
    name = 'U_dot_expectation_{}'.format(exp_name)
    for layer in comp_layers:
        print(layer)
        mats_dot.append(dot[layer])
        print(dot[layer])
        descs_dot.append('dot_{}'.format(layer))
        mats_dot_norm.append(dot_norm[layer])
        print(dot_norm[layer])
        descs_dot_norm.append('dot_norm_{}'.format(layer))
        mats_cos.append(cos[layer])
        print(cos[layer])
        descs_cos.append('cos_{}'.format(layer))
        mats_gram.append(gram[layer])
        print(gram[layer])
        descs_gram.append('gram_{}'.format(layer))
        mats_gramn.append(gramn[layer])
        print(gramn[layer])
        descs_gramn.append('gramn_{}'.format(layer))
        mats_cov.append(cov[layer])
        print(cov[layer])
        descs_cov.append('cov_{}'.format(layer))
        mats_cov_norm.append(cov_norm[layer])
        print(cov_norm[layer])
        descs_cov_norm.append('cov_norm_{}'.format(layer))
        mats_covn.append(covn[layer])
        print(covn[layer])
        descs_covn.append('covn_{}'.format(layer))

    mats = [mats_dot, mats_dot_norm, mats_cos, mats_gram, mats_gramn, mats_cov, mats_cov_norm, mats_covn]
    descs = [descs_dot, descs_dot_norm, descs_cos, descs_gram, descs_gramn, descs_cov, descs_cov_norm, descs_covn]
    HM.vis.plot_matsvd(mats, descs, name, colormap='viridis')

def x_dot_expectation(HM: HessianModule,
               comp_layers,
               exp_name,
               crop_ratio=1,
               ):
    dot = HM.E_x_dotexp(comp_layers, dataset_crop=crop_ratio)
    cos = HM.E_x_cosexp(comp_layers, dataset_crop=crop_ratio)
    name = 'x_dot_expectation_{}'.format(exp_name)
    for layer in comp_layers:
        print(layer)
        print(dot[layer])
        print(cos[layer])
