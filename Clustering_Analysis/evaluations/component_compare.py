import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def matvis_E_custom(HM: HessianModule,
                    comp_layers,
                    funcs,
                    labels,
                    exp_name,
                    crop_ratio=1,
                    colormap='viridis'
                    ):
    """
    Visualizing the matrices computed by funcs
    """
    name = "{}_{}".format("_".join(labels), exp_name)
    assert len(funcs) == len(labels)
    
    Es = {label: None for label in labels}
    descs = {label: [] for label in labels}
    mats = {label: [] for label in labels}

    for i, func in enumerate(funcs):
        Es[labels[i]] = func(comp_layers, dataset_crop=crop_ratio)
    
    for layer in comp_layers:
        for i in range(len(funcs)):
            mats[labels[i]] += [Es[labels[i]][layer]]
            descs[labels[i]] += ['E[{}]_{}'.format(labels[i], layer)]
    
    mats = [mats[label] for label in labels]
    descs = [descs[label] for label in labels]
    HM.vis.plot_matsvd(mats, descs, name, colormap=colormap)

def matvis_E_custom_matmul_transpose(HM: HessianModule,
                                     comp_layers,
                                     funcs,
                                     labels,
                                     exp_name,
                                     transposes=None,
                                     crop_ratio=1,
                                     colormap='viridis', 
                                     eigenspace_overlap_dim=None
                                     ):
    """
    Visualizing the matrices computed by funcs
    """

    assert len(funcs) == len(labels)
    if transposes is None:
        transposes = [None for x in funcs]
    assert len(transposes) == len(labels)

    name = "{}_{}".format("_".join(labels), exp_name)
    
    Es = {label: None for label in labels}
    descs = {label: [] for label in labels}
    mats = {label: [] for label in labels}

    for i, func in enumerate(funcs):
        if not isinstance(func, list):
            # When dealing with a single function
            Es[labels[i]] = func(comp_layers, dataset_crop=crop_ratio)
        else:
            Es[labels[i]] = {}
            t_ind = transposes[i]
            if t_ind is None:
                t_ind = transposes[i]
            assert len(t_ind) == len(func), (len(t_ind), len(funcs), func)
            for j, f in enumerate(func):
                mat = f(comp_layers, dataset_crop=crop_ratio)
                for layer in comp_layers:
                    if isinstance(mat, dict):
                        mat_layer = mat[layer].clone()
                    else:
                        mat_layer = mat.clone()
                    if t_ind[j] == 1:
                        mat_layer.transpose_(0, 1)
                    if j == 0:
                        Es[labels[i]][layer] = mat_layer
                    else:
                        Es[labels[i]][layer] = torch.mm(Es[labels[i]][layer], mat_layer)  # pylint: disable=no-member
        
    for i in range(len(funcs)):
        labels_sep = labels[i].split('.')
        for j in range(len(labels_sep)):
            labels_sep[j] = 'E[{}]'.format(labels_sep[j])
            labels_new = '.'.join(labels_sep)
        for layer in comp_layers:
            if isinstance(Es[labels[i]], dict):
                mats[labels[i]] += [Es[labels[i]][layer]]
            else:
                mats[labels[i]] += [Es[labels[i]]]
            descs[labels[i]] += ['{}_{}'.format(labels_new, layer)]
    
    mats = [mats[label] for label in labels]
    descs = [descs[label] for label in labels]
    HM.vis.plot_matsvd(mats, descs, name, colormap=colormap)

    if eigenspace_overlap_dim is None:
        return
    
    assert len(mats) == 2
    assert isinstance(eigenspace_overlap_dim, int)

    name_cross = 'TEO{}_{}'.format(eigenspace_overlap_dim, name)
    overlap_xs, overlap_ys = [], []
    for i, layer in enumerate(comp_layers):
        m1, m2 = mats[0][i], mats[1][i]
        overlap_x, overlap_y = dominate_eigenspace_overlap_trend_mats(HM, m1, m2, eigenspace_overlap_dim, name_cross)
        overlap_xs.append(overlap_x)
        overlap_ys.append(overlap_y)
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name_cross, dpi=150)

def matvis_E_UTAU_xxT(HM: HessianModule,
                    comp_layers,
                    exp_name,
                    crop_ratio=1,
                    ):
    """
    Visualizing the E[UTAU] and E[xxT] matrices
    """
    E_xxTs = HM.E_xxT(comp_layers, dataset_crop=crop_ratio)
    E_UTAUs = HM.E_UTAU(comp_layers, dataset_crop=crop_ratio)
    mats_UTAU, mats_xxT = [], []
    descs_UTAU, descs_xxT = [], []
    name = "UTAU_xxT_{}".format(exp_name)

    for layer in comp_layers:
        mats_UTAU += [E_UTAUs[layer]]
        descs_UTAU += ['E[UTAU]_{}'.format(layer)]
        mats_xxT += [E_xxTs[layer]]
        descs_xxT += ['E[xxT]_{}'.format(layer)]
    
    mats = [mats_UTAU, mats_xxT]
    descs = [descs_UTAU, descs_xxT]
    HM.vis.plot_matsvd(mats, descs, name, colormap='viridis')

def dominate_eigenspace_overlap_trend_mats(HM: HessianModule, m1, m2, dim=1, name='overlap_tmp'):
    assert m1.shape == m2.shape
    assert len(m1.shape) == 2, 'the comparing matrices must be square to be diagonalizable'
    vals1, vecs1 = HM.utils.eigenthings_tensor_utils(m1)
    vals2, vecs2 = HM.utils.eigenthings_tensor_utils(m2)
    overlap_x = np.arange(1, min(m1.shape[1], dim + 1))
    overlap_y = HM.measure.trace_overlap_trend(vecs1, vecs2, overlap_x, HM.device)
    return overlap_x, overlap_y