import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def eigenspace_est_vs_real(HM: HessianModule,
                           dim: int,
                           comp_layers,
                           exp_name,
                           eigenvals_real: dict,
                           eigenvecs_real: dict,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    name = 'Estimate_eigenspace_overlap_{}_{}'.format(exp_name, dim)

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=dim, dataset_crop=crop_ratio)
    
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        print(H_eigenvecs_est.size(), H_eigenvecs.size())
        overlap_dim_y = HM.measure.trace_overlap_trend(H_eigenvecs, H_eigenvecs_est, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)

def singular_vec_overlap_analysis(HM: HessianModule,
                                  topn: int,
                                  comp_layers,
                                  exp_name,
                                  eigenvals_real: dict,
                                  eigenvecs_real: dict,
                                  crop_ratio=1):

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    E_xs = HM.E_x(comp_layers, dataset_crop=crop_ratio)
    E_xxTs = HM.E_xxT(comp_layers, dataset_crop=crop_ratio)
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        name = "SVOA_{}_{}_{}".format(exp_name, layer, topn)
        l_ovls, r_ovls = [], []
        for i in range(topn):
            vec_base_mat = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            vec_est_mat = HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer)
            U_base, S_base, V_base = torch.svd(vec_base_mat, compute_uv=True) # pylint: disable=no-member
            U_est, S_est, V_est = torch.svd(vec_est_mat, compute_uv=True) # pylint: disable=no-member
            for mat in [U_base, V_base, U_est, V_est]:
                mat.transpose_(0, 1)
            l_ovl = U_base[0].dot(U_est[0]) ** 2
            r_ovl = V_base[0].dot(V_est[0]) ** 2
            l_ovls.append(l_ovl.cpu().numpy())
            r_ovls.append(r_ovl.cpu().numpy())
        overlap_x = [list(range(1, topn + 1)) for i in range(2)]
        overlap_y = [l_ovls, r_ovls]
        descs = ['L Singular Vector Dot^2', 'R Singular Vector Dot^2']
        HM.vis.plots(overlap_x, overlap_y, descs, x_label='* of eigenvec', name=name, line_style='', marker_style='.')