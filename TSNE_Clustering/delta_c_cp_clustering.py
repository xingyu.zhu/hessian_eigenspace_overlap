import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from tools import file_fetch
import half_decomposition
from tsne_visualize import *

dirc = '../MNIST_Exp1/experiments/FC2_narrow30_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_Exp1/experiments/FC1_20_small_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01_wd0.0005_m0.9'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/FC3_wide_fixlr0.01'
epoch = 0
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))
remain_labels = None

crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3', 'fc4']
fc_layers = ['fc1', 'fc2', 'fc3', 'fc4']
# comp_layers = ['fc1', 'fc2']
# fc_layers = ['fc1', 'fc2']

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
name = exp_name + '_Δ_ccp_cluster'
print(name)

assert os.path.isdir(dirc)
sys.path.append(dirc)
snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)
C = HM.Ws[0].shape[0]

for layer in comp_layers:

    delta_ccp, delta_ccp_inds = half_decomposition.comp_delta_ccp(HM, layer, C, 128)

    delta_c = delta_ccp.view(10, 10, delta_ccp.shape[-1]).mean(dim=1)
    class_inds = np.concatenate([np.arange(10), delta_ccp_inds])
    class_deltas = torch.cat([delta_c, delta_ccp])
    
    class_dist = half_decomposition.l2_distmat_parallel(class_deltas, no_sqrt=True)
    centers, center_inds, others, other_inds = tsne_c_cp(class_dist, class_inds, C=10)
    visualize_tsne_withcenter_class(centers, center_inds, others, other_inds, "CL_{}_{}".format(name, layer))

    delta_cp = delta_ccp.view(10, 10, delta_ccp.shape[-1]).mean(dim=0)
    logit_inds = np.concatenate([np.arange(10) for i in range(11)])
    logit_deltas = torch.cat([delta_cp, delta_ccp])
    
    logit_dist = half_decomposition.l2_distmat_parallel(logit_deltas, no_sqrt=True)
    centers, center_inds, others, other_inds = tsne_c_cp(logit_dist, logit_inds, C=10)
    visualize_tsne_withcenter_logits(centers, center_inds, others, other_inds, "LL_{}_{}".format(name, layer))


# exit()
# tsne_visualize()
# print(dist_delta_c_ccp)