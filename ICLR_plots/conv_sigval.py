import os, sys
import torch
import numpy as np
from algos.cfhag import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.cfhag.visualization import vis
from algos.cfhag.measures import Measures
from algos.cfhag.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.cfhag.dp import OnDeviceDataLoader
from algos.cfhag.utils import *
import visualization as vis

import numpy as np
import matplotlib as mpl
from matplotlib import rc
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
mpl.use('Agg')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

plt.style.use('seaborn-deep')

dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
epoch = -1
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = True
remain_labels = None
load_prev = True
crop_ratio = 1
layer_seq = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3']
comp_layers = ['conv1', 'conv2']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET')

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64, remain_labels=remain_labels)

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

if load_evals:
    eigenthings_real = torch.load(eval_file, map_location='cpu') 
    eigenvals_real = eigenthings_real['eigenvals_layer']
    eigenvecs_real = eigenthings_real['eigenvecs_layer']
else:
    eigenvals_real, eigenvecs_real = None, None

xxT_snapshot = snapshot_file + "xxT_Conv"
if os.path.isfile(xxT_snapshot) and load_prev:
    xxT_conv = torch.load(xxT_snapshot, map_location='cpu')
    print('Loaded Conv xxT from previous computed result')
else:
    xxT_conv = HM.expectation(HM.decomp.xxTC_comp, comp_layers)
    torch.save(xxT_conv, xxT_snapshot)
UTAU_snapshot = snapshot_file + "UTAU_Conv"
if os.path.isfile(UTAU_snapshot) and load_prev:
    UTAU_conv = torch.load(UTAU_snapshot, map_location='cpu')
    print('Loaded Conv UTAU from previous computed result')
else:
    UTAU_conv = HM.expectation(HM.decomp.UCTAUC_comp, comp_layers, batchsize=32)
    torch.save(UTAU_conv, UTAU_snapshot)
# component_compare.matvis_E_computed(HM, comp_layers, [UTAU_conv, xxT_conv], ['UTAU', 'xxT'], exp_name + 'ConvMX')


def xxT_plot():
    dim = 20
    pic_name = pic_name = 'xxTConv_sigval_d{}_{}'.format(dim, exp_name)
    plt.figure(figsize=(2.66, 2.7))
    E_xxTs = xxT_conv
    gs = gridspec.GridSpec(1, len(comp_layers))

    for i, layer in enumerate(comp_layers):
        plt.subplot(gs[0, i])

        E_xxT = E_xxTs[layer]
        eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxT, symmetric=True)
        vals = eigenvals_xxT[:dim]
        plt.scatter(np.arange(1, len(vals) + 1), vals, s=12)
        
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(layer)

    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout(pad=1.05)
    plt.savefig(image_path, bbox_inches='tight')

xxT_plot()