import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def matvis_E_conf(HM: HessianModule,
            comp_layers,
            funcs,
            labels,
            exp_name,
            crop_ratio=1,
            colormap='viridis',
            confidence=1
            ):
    """
    Visualizing the matrices computed by funcs
    """
    name = "{}_{}".format("_".join(labels), exp_name)
    assert len(funcs) == len(labels)
    
    Es = {label: None for label in labels}
    descs = {label: [] for label in labels}
    mats = {label: [] for label in labels}

    for i, func in enumerate(funcs):
        Es[labels[i]] = func(comp_layers, dataset_crop=crop_ratio)
    
    for layer in comp_layers:
        for i in range(len(funcs)):
            mats[labels[i]] += [Es[labels[i]][layer]]
            descs[labels[i]] += ['E[{}]_{}'.format(labels[i], layer)]
    
    mats = [mats[label] for label in labels]
    descs = [descs[label] for label in labels]
    HM.vis.plot_matsvd(mats, descs, name, colormap=colormap)

def matvis_E_UTAU_xxT_conf(HM: HessianModule,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        confidence=1
                        ):
    """
    Visualizing the E[UTAU] and E[xxT] matrices
    """
    xxT_comp = HM.decomp.xxT_comp
    UTAU_comp = HM.decomp.UTAU_comp
    E_xxTs = HM.expectation(xxT_comp, comp_layers, dscrop=crop_ratio)
    E_UTAUs = HM.expectation(UTAU_comp, comp_layers, dscrop=crop_ratio, y_confidence_scale=confidence, from_cache=False)
    mats_UTAU, mats_xxT = [], []
    descs_UTAU, descs_xxT = [], []
    name = "conf{:.2g}_decomp_{}".format(confidence, exp_name)

    for layer in comp_layers:
        mats_UTAU += [E_UTAUs[layer]]
        descs_UTAU += ['E[UTAU]_{}'.format(layer)]
        mats_xxT += [E_xxTs[layer]]
        descs_xxT += ['E[xxT]_{}'.format(layer)]
    
    mats = [mats_UTAU, mats_xxT]
    descs = [descs_UTAU, descs_xxT]
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def matvis_E_UTAU_conf(HM: HessianModule,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        confidence=1
                        ):
    """
    Visualizing the E[UTAU] and E[xxT] matrices
    """
    UTAU_comp = HM.decomp.UTAU_comp
    E_UTAUs = HM.expectation(UTAU_comp, comp_layers, dscrop=crop_ratio, y_confidence_scale=confidence, from_cache=False, to_cache=False)
    mats_UTAU = []
    descs_UTAU = []
    name = "conf{:.2g}_UTAU_{}".format(confidence, exp_name)

    for layer in comp_layers:
        mats_UTAU += [E_UTAUs[layer]]
        descs_UTAU += ['E[UTAU]_{}'.format(layer)]
    
    mats = [mats_UTAU]
    descs = [descs_UTAU]
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def matvis_E_UTAntU_conf(HM: HessianModule,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        confidence=1
                        ):
    """
    Visualizing the E[UTAntU] and E[xxT] matrices
    """

    A_comp, U_comp = HM.decomp.A_comp, HM.decomp.U_comp
    Ant_comp = HM.tsa.normalize_comp(A_comp, dim=-2)
    UTAntU_comp = HM.tsa.chain_comp([U_comp, Ant_comp, U_comp], [1, 0, 0])
    
    E_UTAntUs = HM.expectation(UTAntU_comp, comp_layers, dscrop=crop_ratio, y_confidence_scale=confidence, from_cache=False, to_cache=False)
    mats_UTAntU = []
    descs_UTAntU = []
    name = "conf{:.2g}_UTAntU_{}".format(confidence, exp_name)

    for layer in comp_layers:
        mats_UTAntU += [E_UTAntUs[layer]]
        descs_UTAntU += ['E[UTAntU]_{}'.format(layer)]
    
    mats = [mats_UTAntU]
    descs = [descs_UTAntU]
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)