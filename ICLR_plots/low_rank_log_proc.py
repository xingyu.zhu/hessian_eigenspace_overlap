import json
import numpy as np

metas = {}

logs = ['./xxT_lowrank_CIFAR10_FC.json', './xxT_lowrank_CIFAR10_LeNet.json', './xxT_lowrank_MNIST.json', './xxT_lowrank_CIFAR100.json', './xxT_lowrank_CIFAR10_VGGnew.json']
logs = ['./xxT_lowrank_BN.json']
for log in logs:
    with open(log) as fl:
        metas.update(json.load(fl))

all_angles_fc = []
all_angles_conv = []

all_gaps_fc = []
all_gaps_conv = []

for net in metas:
    print(net)
    layer_conf, meta = metas[net]

    net_angles_fc = []
    net_angles_conv = []

    net_gaps_fc = []
    net_gaps_conv = []
    # print(meta)
    for i in range(len(layer_conf)):
        if layer_conf[i] == 0:
            if i != 0:
                for angles, gaps in meta:
                    all_angles_conv.append(angles[i])
                    all_gaps_conv.append(gaps[i])
                    net_angles_conv.append(angles[i])
                    net_gaps_conv.append(gaps[i])

        else:
            if i != 0:
                for angles, gaps in meta:
                    # print(angles, gaps)
                    all_angles_fc.append(angles[i])
                    all_gaps_fc.append(gaps[i])
                    net_angles_fc.append(angles[i])
                    net_gaps_fc.append(gaps[i])

    print("{:.3f}\t{:.3f}\t{:.3f}".format(np.mean(net_angles_fc), min(net_angles_fc), max(net_angles_fc)), end='')
    print("\t{:.2f}\t{:.2f}\t{:.2f}".format(np.mean(net_gaps_fc), min(net_gaps_fc), max(net_gaps_fc)))

    if len(net_angles_conv) > 0:
        print("{:.3f}\t{:.3f}\t{:.3f}".format(np.mean(net_angles_conv), min(net_angles_conv), max(net_angles_conv)), end='')
        print("\t{:.2f}\t{:.2f}\t{:.2f}".format(np.mean(net_gaps_conv), min(net_gaps_conv), max(net_gaps_conv)))


print("Overall Stats result")

print(min(all_angles_fc), max(all_angles_fc), np.mean(all_angles_fc))
print(min(all_gaps_fc), max(all_gaps_fc), np.mean(all_gaps_fc))
if len(all_angles_conv) > 0:
    print(min(all_angles_conv), max(all_angles_conv), np.mean(all_angles_conv))
    print(min(all_gaps_conv), max(all_gaps_conv), np.mean(all_gaps_conv))