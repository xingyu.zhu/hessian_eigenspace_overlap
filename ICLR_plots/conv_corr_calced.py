import os, sys
import torch
import numpy as np
from algos.cfhag import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.cfhag.visualization import vis
from algos.cfhag.measures import Measures
from algos.cfhag.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.cfhag.dp import OnDeviceDataLoader
from algos.cfhag.utils import *
import visualization as vis

import numpy as np
import matplotlib as mpl
from matplotlib import rc
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
mpl.use('Agg')
plt.style.use('ggplot')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../CIFAR100/experiments/VGG11NN_nobn_fixlr0.01'
epoch = -1
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = True
remain_labels = None
load_prev = True
crop_ratio = 1

layer_seq = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3']
comp_layers = ['conv1', 'conv2']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_part_t18')

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
layer_seq = []
for name, param in net.named_parameters():
    if ".weight" in name:
        layer_seq.append(name[:-7])
print(layer_seq)
exit()

net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64, remain_labels=remain_labels)

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

if load_evals:
    eigenthings_real = torch.load(eval_file, map_location='cuda' if torch.cuda.is_available() else 'cpu') 
    eigenvals_real = eigenthings_real['eigenvals_layer']
    eigenvecs_real = eigenthings_real['eigenvecs_layer']
else:
    eigenvals_real, eigenvecs_real = None, None

xxT_snapshot = snapshot_file + "xxT_Conv"
if os.path.isfile(xxT_snapshot) and load_prev:
    xxT_conv = torch.load(xxT_snapshot, map_location='cuda' if torch.cuda.is_available() else 'cpu')
    print('Loaded Conv xxT from previous computed result')
else:
    xxT_conv = HM.expectation(HM.decomp.xxTC_comp, comp_layers)
    torch.save(xxT_conv, xxT_snapshot)
UTAU_snapshot = snapshot_file + "UTAU_Conv"
if os.path.isfile(UTAU_snapshot) and load_prev:
    UTAU_conv = torch.load(UTAU_snapshot, map_location='cuda' if torch.cuda.is_available() else 'cpu')
    print('Loaded Conv UTAU from previous computed result')
else:
    UTAU_conv = HM.expectation(HM.decomp.UCTAUC_comp, comp_layers, batchsize=32)
    torch.save(UTAU_conv, UTAU_snapshot)
# component_compare.matvis_E_computed(HM, comp_layers, [UTAU_conv, xxT_conv], ['UTAU', 'xxT'], exp_name + 'ConvMX')

def plot_single(name, mat, title=None):
    
    plt.figure(figsize=(6, 3), dpi=600)
    plt.imshow(mat.cpu(), cmap='Reds', interpolation='nearest')
    plt.grid(False)
    plt.box(on=None)
    # for direction in ["top", "bottom", "left", "right"]:
    #     axes[0].spines[direction].set_visible(False)
    # plt.title(title)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
        print(spine)
    image_path = vis.get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def correspondance_expand(layer, topn=50):

    device = HM.device
    pic_name = 'est_real_corr_expand_t{}_{}_{}'.format(topn, exp_name, layer)
    E_UTAUs = UTAU_conv
    E_xxTs = xxT_conv
    eigenthings_est = HM.hessian_eigenthings_estimate_precomputed(comp_layers, E_UTAUs, E_xxTs, num_eigenthings=topn)
    
    assert layer in comp_layers
    name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
    H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
    layer_weight_ind = layer + '.weight'
    print(eigenvecs_real.keys())
    H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]).to(device) # pylint: disable=no-member

    eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAUs[layer], symmetric=True)
    eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True)
    b_xxT = eigenvecs_xxT.unsqueeze(0)
    b_UTAU = eigenvecs_UTAU.unsqueeze(0)

    M, N = len(eigenvals_xxT), len(eigenvals_UTAU)
    print(M, N)

    vec_est_mat = torch.cat([H_eigenvecs_est[i].view([N, M]).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member
    
    est_UTAU_raw_overlap = torch.Tensor.matmul(b_UTAU, vec_est_mat)# pylint: disable=no-member
    est_UTAU_corr_exp = torch.Tensor.sum(est_UTAU_raw_overlap.square_(), dim=-1, keepdim=False).transpose(0, 1)

    est_xxT_raw_overlap = torch.Tensor.matmul(vec_est_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    est_xxT_corr_exp = torch.Tensor.sum(est_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1)
    
    vec_base_mat = torch.cat([H_eigenvecs[i].view([N, M]).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member

    base_UTAU_raw_overlap = torch.Tensor.matmul(b_UTAU, vec_base_mat)# pylint: disable=no-member
    base_UTAU_corr_exp = torch.Tensor.sum(base_UTAU_raw_overlap.square_(), dim=-1, keepdim=False).transpose(0, 1)

    base_xxT_raw_overlap = torch.Tensor.matmul(vec_base_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    base_xxT_corr_exp = torch.Tensor.sum(base_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1)[:len(base_UTAU_corr_exp)]

    plot_single('UTAU_True' + pic_name, base_UTAU_corr_exp)
    plot_single('UTAU_Approx' + pic_name, est_UTAU_corr_exp)
    plot_single('xxT_True' + pic_name, base_xxT_corr_exp)
    plot_single('xxT_Approx' + pic_name, est_xxT_corr_exp)


correspondance_expand('conv1', 50)
correspondance_expand('conv2', 50)