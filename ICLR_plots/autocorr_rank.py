import os, sys
import torch
import numpy as np
from algos.cfhag import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch
from importlib import reload

from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d, Linear

from algos.cfhag.visualization import vis
from algos.cfhag.measures import Measures
from algos.cfhag.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.cfhag.dp import OnDeviceDataLoader
from algos.cfhag.utils import *
import visualization as vis

import numpy as np
import matplotlib as mpl
from matplotlib import rc
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
mpl.use('Agg')
plt.style.use('ggplot')

import json
# rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
# rc('text', usetex=True)
# params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
# plt.rcParams.update(params)

# dircs = [
#     '../CIFAR100/experiments/VGG11W64New_nobn_fixlr0.01',
#     '../CIFAR100/experiments/VGG11W48New_nobn_fixlr0.01',
#     '../CIFAR100/experiments/VGG11W80New_nobn_fixlr0.01',
#     '../CIFAR100/experiments/Resnet18W48New_nobn_fixlr0.01',
#     '../CIFAR100/experiments/Resnet18W64New_nobn_fixlr0.01',
#     '../CIFAR100/experiments/Resnet18W80_nobn_fixlr0.01',
# ]

dircs = [
    '../MNIST_Exp1/experiments/FC2_fixlr0.01',
    '../MNIST_Exp1/experiments/FC2_600_fixlr0.01',
    '../MNIST_Exp1/experiments/FC2_600copy_fixlr0.01',
    '../MNIST_Exp1/experiments/FC4_600_fixlr0.01',
    '../MNIST_Exp1/experiments/FC8_600_fixlr0.01',
    '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01',
    '../CIFAR10_Exp1/experiments/LeNet5_fc1_80',
    '../CIFAR10_Exp1/experiments/LeNet5_fc1_150',
    '../CIFAR10_Exp1/experiments/LeNet5_fc1_100',
    '../CIFAR10_Exp1/experiments/FC2_600_fixlr0.01',
    '../CIFAR10_Exp1/experiments/FC3_wide_fixlr0.01'
    '../CIFAR100/experiments/VGG11W64New_nobn_fixlr0.01',
    '../CIFAR100/experiments/VGG11W48New_nobn_fixlr0.01',
    '../CIFAR100/experiments/VGG11W80New_nobn_fixlr0.01',
    '../CIFAR100/experiments/Resnet18W48New_nobn_fixlr0.01',
    '../CIFAR100/experiments/Resnet18W64New_nobn_fixlr0.01',
    '../CIFAR100/experiments/Resnet18W80_nobn_fixlr0.01',
]

dircs = [
    '../CIFAR10_Exp1/experiments/VGG11W32_fixlr0.01',
    '../CIFAR10_Exp1/experiments/VGG11W64_fixlr0.01',
    '../CIFAR10_Exp1/experiments/VGG11W200_fxlr0.01',
]

dircs = [
    '../CIFAR10_Exp1/experiments/VGG11W64_withBN_fixlr0.01',
    '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01',
    '../MNIST_ExpBN/experiments/FC2_600BN_fixlr0.01',
    '../MNIST_ExpBN/experiments/FC4_600BN_fixlr0.01',
    '../CIFAR10_Exp1/experiments/LeNet5_BN_fixlr0.01',
]

results = {}

first_round = True

for dirc in dircs:
    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    if first_round:
        import config # pylint: disable=no-name-in-module
        first_round = False
    else:
        config = reload(config)
    conf = config.Config()
    dataset = conf.dataset(train=True, transform=conf.test_transform)

    net = conf.net()
    layer_seq = []
    for key in net.state_dict():
        if key.endswith('.weight'):
            if not key[:-7].endswith('bn'):
                layer_seq.append(key[:-7])
    print(layer_seq)
    LMs = {layer: rgetattr(net, layer) for layer in layer_seq}
    new_layer_seq = []
    for layer in layer_seq:
        if isinstance(LMs[layer], (Conv2d, Linear)):
            new_layer_seq.append(layer)

    layer_seq = new_layer_seq
    print(layer_seq)
    # exit()
    dev = 'cuda' if torch.cuda.is_available() else 'cpu'
    HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64)
    layer_info = []
    for LM in HM.LMs:
        if isinstance(LM, Conv2d):
            layer_info.append(0)
        else:
            layer_info.append(1)

    res = []
    for run in range(1, 6):
        log_path = "{}/experiment_log/run_{}/models/final.pth".format(dirc, run)
        if os.path.isfile(log_path):
            sd = torch.load(log_path, map_location=dev)
        else:
            print("No log for run {}".format(run))
            continue
        print('Loading {}'.format(log_path))
        HM.load_sd(sd)
        ExxTs = HM.expectation(HM.decomp.xxTC_comp, layer_seq, batchsize=64, to_cache=False)
        Exs = HM.expectation(HM.decomp.xC_comp, layer_seq, batchsize=64, to_cache=False)
        
        angles = [0 for x in layer_seq]
        spec_ratios = [0 for x in layer_seq]

        for i, layer in enumerate(layer_seq):
            Ex = Exs[layer].sum(-1)
            ExxT = ExxTs[layer]
            xxT_sigma, xxT_vs = HM.utils.eigenthings_tensor_utils(ExxT, symmetric=True)

            Ex.div_(Ex.norm())
            angles[i] = Ex.dot(xxT_vs[0]).square().item()
            spec_ratios[i] = (xxT_sigma[0] / xxT_sigma[1]).item()
        
        print(angles, spec_ratios)
        res.append([angles, spec_ratios])
    results[dirc] = [layer_info, res]
    sys.path.remove(dirc)
    del HM
    del sd

with open('xxT_lowrank_BN.json', 'w') as outfile:
    json.dump(results, outfile)