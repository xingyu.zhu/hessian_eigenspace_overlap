import os, sys
import torch
import numpy as np
from algos.cfhag import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

from IPython import embed
from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.cfhag.visualization import vis
from algos.cfhag.measures import Measures
from algos.cfhag.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.cfhag.dp import OnDeviceDataLoader
from algos.cfhag.utils import *
import visualization as vis

import numpy as np
import matplotlib as mpl
from matplotlib import rc
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
mpl.use('Agg')
# plt.style.use('ggplot')
# rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
# rc('text', usetex=True)
# params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
# plt.rcParams.update(params)

dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/VGG11NN_nobn_fixlr0.01'
dirc = '../CIFAR100/experiments/Resnet18NN_nobn_fixlr0.01'
dirc = '../CIFAR100/experiments/Resnet18W64ShiftX_nobn_fixlr0.01'
epoch = -1
run = 3

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = True
remain_labels = None
load_prev = True
crop_ratio = 1

comp_layers = ['conv1', 'layer1.0.conv1', 'layer1.0.conv2', 'layer1.1.conv1', 'layer1.1.conv2', 'layer2.0.conv1', 'layer2.0.conv2', 'layer2.0.shortcut.0', 'layer2.1.conv1', 'layer2.1.conv2', 'layer3.0.conv1', 'layer3.0.conv2', 'layer3.1.conv1', 'layer3.1.conv2', 'layer4.0.conv1', 'layer4.0.conv2', 'layer4.1.conv1', 'layer4.1.conv2']#, 'linear']
layer_seq = comp_layers

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    # eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET')
    # eval_file = '../CIFAR10_Exp1/experiments/VGG11_nobn_fixlr0.01/experiment_log/run_5/models/final.pth_LW_part_t4_l200.eval'
    eval_files = ['../CIFAR100/experiments/Resnet18NN_nobn_fixlr0.01/experiment_log/run_2/models/final.pth_LW_part_t100_l500.eval']

# for layer_num in [8, 9, 10, 11, 13, 14, 15, 16, 18, 19, 20]:
    # eval_files.append('../CIFAR100/experiments/Resnet18NN_nobn_fixlr0.01/experiment_log/run_2/models/final.pth_LW_part_layer{}_l200.eval'.format(layer_num))

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64, remain_labels=remain_labels)
Exs_conv = HM.expectation(HM.decomp.x_comp, comp_layers)

for layer in layer_seq:
    print(layer)
    Ex_conv = Exs_conv[layer]
    print("Min:{}, Mean:{}, sample {}".format(torch.min(Ex_conv).item(), torch.mean(Ex_conv).item(), Ex_conv.view(-1)[:3]))
# embed()
exit()
exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

eigenthings_real = torch.load(eval_files[0], map_location='cuda' if torch.cuda.is_available() else 'cpu') 
eigenvals_real = eigenthings_real['eigenvals_layer']
eigenvecs_real = eigenthings_real['eigenvecs_layer']
for i, eval_fl_new in enumerate(eval_files[1:]):
    print("Loading {}".format(eval_fl_new))
    eigenthings_real_ = torch.load(eval_fl_new, map_location='cuda' if torch.cuda.is_available() else 'cpu') 
    eigenvals_real.update(eigenthings_real_['eigenvals_layer'])
    eigenvecs_real.update(eigenthings_real_['eigenvecs_layer'])
xxT_snapshot = snapshot_file + "xxT_Conv"
if os.path.isfile(xxT_snapshot) and load_prev:
    xxT_conv = torch.load(xxT_snapshot, map_location='cuda' if torch.cuda.is_available() else 'cpu')
    print('Loaded Conv xxT from previous computed result')
else:
    xxT_conv = HM.expectation(HM.decomp.xxTC_comp, comp_layers)
    torch.save(xxT_conv, xxT_snapshot)


def plot_single(name, mat, vert, title=None):
    
    plt.figure(figsize=(6, 3), dpi=600)
    plt.imshow(mat.cpu(), cmap='Reds', interpolation='nearest')
    plt.grid(False)
    plt.box(on=None)
    # for direction in ["top", "bottom", "left", "right"]:
    #     axes[0].spines[direction].set_visible(False)
    if title is not None:
        plt.title(title)
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
        print(spine)
    plt.axvline(vert)
    image_path = vis.get_image_path(name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def plot_sigval(name, sigval, title=None):
    
    plt.figure(figsize=(4, 4), dpi=200)
    print(len(np.arange(len(sigval))))
    print(len(sigval))
    print(sigval)
    plt.scatter(np.arange(len(sigval)), sigval)
    plt.title(title)
    image_path = vis.get_image_path(name + "_sigval", store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def correspondance_expand(layer, topn=50):

    device = HM.device
    pic_name = 'xxt_corr_expand_t{}_{}_{}'.format(topn, exp_name, layer)
    E_xxTs = xxT_conv

    
    assert layer in comp_layers
    name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
    layer_weight_ind = layer + '.weight'
    print(eigenvecs_real.keys())
    H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]).to(device) # pylint: disable=no-member
    topn = min(topn, len(H_eigenvecs))

    eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True)
    print(eigenvals_xxT[:30])
    b_xxT = eigenvecs_xxT.unsqueeze(0)

    M = len(eigenvals_xxT)
    N = int(H_eigenvecs[0].view(-1).shape[0] / M)
    print(M, N)

    vec_base_mat = torch.cat([H_eigenvecs[i].view([N, M]).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member

    base_xxT_raw_overlap = torch.Tensor.matmul(vec_base_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    base_xxT_corr_exp = torch.Tensor.sum(base_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1)[:100]

    plot_single('xxT_True' + pic_name, base_xxT_corr_exp, N, title="{}_m={}".format(layer, N))

    U, D, V = torch.svd(vec_base_mat[0])
    plot_sigval('xxT_True' + pic_name, D[:30].cpu().numpy(), title="{}_m={}".format(layer, N))

def approx_rank(layer, topn=50):
    
    device = HM.device
    pic_name = 'approx_rank_expand_t{}_{}_{}'.format(topn, exp_name, layer)
    E_xxTs = xxT_conv

    assert layer in comp_layers
    name = "Pairs_{}_{}_{}".format(exp_name, layer, topn)
    layer_weight_ind = layer + '.weight'
    print(eigenvecs_real.keys())
    H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]).to(device) # pylint: disable=no-member
    topn = min(topn, len(H_eigenvecs))

    eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True)
    print(eigenvals_xxT[:30])
    b_xxT = eigenvecs_xxT.unsqueeze(0)

    M = len(eigenvals_xxT)
    N = int(H_eigenvecs[0].view(-1).shape[0] / M)
    print(M, N)

    vec_base_mat = torch.cat([H_eigenvecs[i].view([N, M]).unsqueeze(0) for i in range(topn)]).to(device) # pylint: disable=no-member

    singular_vecs = []
    for v in vec_base_mat:
        U, D, V = torch.svd(v)
        singular_vecs.append(V[:, :1].transpose_(0, 1))
    
    ranks = []
    for i in range(1, topn):
        subspace = torch.cat(singular_vecs[:i])
        U, D, V = torch.svd(subspace)
        rank = torch.sum(D * D) / (D[0] * D[0])
        ranks.append(rank.item())

    # eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxTs[layer], symmetric=True)
    # print(eigenvals_xxT[:30])
    # b_xxT = eigenvecs_xxT.unsqueeze(0)

    base_xxT_raw_overlap = torch.Tensor.matmul(vec_base_mat, b_xxT.transpose(-1, -2))# pylint: disable=no-member
    base_xxT_corr_exp = torch.Tensor.sum(base_xxT_raw_overlap.square_(), dim=1, keepdim=False).transpose(0, 1)[:100]

    plot_sigval('approx_rank' + pic_name, np.array(ranks), title="{}_m={}".format(layer, N))

for layer in comp_layers:
    approx_rank(layer, 500)
    # correspondance_expand(layer, 500)
# correspondance_expand('features.3', 400)
# correspondance_expand('features.6', 400)
# correspondance_expand('features.8', 400)