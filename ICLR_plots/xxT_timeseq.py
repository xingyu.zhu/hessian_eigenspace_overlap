import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
from matplotlib import rc

from IPython import embed

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
# plt.style.use('seaborn-deep')
plt.rcParams['image.cmap']='rainbow'
plt.rcParams['xtick.labelsize']= 14
plt.rcParams['ytick.labelsize']= 14
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['axes.titlesize'] = 14
plt.rcParams.update(params)

dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_0M/experiments/FC2_200_0M_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2S_20_BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC3BN_nl_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../MNIST_Exp1/experiments/FC2_20_small_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3'] #, 'fc4']
fc_layers = ['fc1', 'fc2', 'fc3'] #, 'fc4']

comp_layers = ['fc1', 'fc2']
fc_layers = ['fc1', 'fc2', 'fc3']

epochs = np.arange(0, 1001, 10)

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()
net = conf.net()

dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)
device = 'cuda' if torch.cuda.is_available() else 'cpu'

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "R{}_{}_{}".format(run, epochs[0], epochs[-1])
dataset = conf.dataset(train=True, transform=conf.test_transform)

eigenvals_real = None
E_UTAUs = None

def comp_xxT_sigval(sd, dim):
    
    HM.load_sd(sd)
    E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, print_log=False, batchsize=2048)
    ret = {}
    for i, layer in enumerate(comp_layers):

        E_xxT = E_xxTs[layer]
        eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxT, symmetric=True)
        vals = eigenvals_xxT[:dim]
        ret[layer] = vals.cpu().numpy()
    
    return ret

def xxT_seq(epochs, dim):

    ret = {l:[] for l in comp_layers}
    for i in epochs:
        print(i)
        snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=i, run_name=run)
        sd = torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu')
        sigvals = comp_xxT_sigval(sd, dim)
        for layer in comp_layers:
            ret[layer].append(np.expand_dims(sigvals[layer], 0))
    for layer in comp_layers:
        ret[layer] = np.concatenate(ret[layer])
    
    return ret

def comp_UTAU_sigval(sd, dim):
    
    HM.load_sd(sd)
    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, print_log=False, batchsize=2048)
    ret = {}
    for i, layer in enumerate(comp_layers):

        E_UTAU = E_UTAUs[layer]
        eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAU, symmetric=True)
        vals = eigenvals_UTAU[:dim]
        ret[layer] = vals.cpu().numpy()
    
    return ret

def UTAU_seq(epochs, dim):

    ret = {l:[] for l in comp_layers}
    for i in epochs:
        print(i)
        snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=i, run_name=run)
        sd = torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu')
        sigvals = comp_UTAU_sigval(sd, dim)
        for layer in comp_layers:
            ret[layer].append(np.expand_dims(sigvals[layer], 0))
    for layer in comp_layers:
        ret[layer] = np.concatenate(ret[layer])
    
    return ret

def plot_normalized(res, epochs_count, name):

    for layer in res:
        vals = res[layer]

        vals = vals / np.expand_dims(vals[:, 0], -1)
        vals_t = np.transpose(vals)
        dim = len(vals_t)
        pic_name = "E{}_trend_{}_{}_{}".format(name, dim, exp_name, layer)
        image_path = vis.get_image_path(pic_name, store_format='pdf')
        plt.figure(figsize=(8, 3), dpi=600)
        plt.set_cmap('rainbow')
        ps = []
        for i, val in enumerate(vals_t):
            p = plt.plot(epochs_count, val, label = r'$\lambda_{' + str(i + 1) + r'}$', color=plt.cm.plasma(i / dim))
            ps.append(p[0])
        plt.tight_layout()
        plt.legend(handles=ps, bbox_to_anchor=(1.01, 1), loc='upper left', ncol=int(dim/10))
        plt.xlabel('Epoch')
        if name == 'xxT':
            plt.title(r"Eigenvalues of $\mathbb{E}[\bm{x}\bm{x}^T]$ (normalized)")
        else:
            plt.title(r"Eigenvalues of $\mathbb{E}[\bm{M}]$ (normalized)")
        plt.savefig(image_path, bbox_inches='tight')


xxT_res = xxT_seq(epochs, 10)
plot_normalized(xxT_res, epochs, 'xxT')

UTAU_res = UTAU_seq(epochs, 20)
plot_normalized(UTAU_res, epochs, 'UTAU')