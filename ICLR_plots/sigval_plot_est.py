import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
from matplotlib import rc

from IPython import embed

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}

plt.style.use('seaborn-deep')
# plt.rcParams['xtick.labelsize']= 12
# plt.rcParams['ytick.labelsize']= 12
# plt.rcParams['axes.labelsize'] = 14
# plt.rcParams['axes.titlesize'] = 14
plt.rcParams.update(params)

dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_20_small_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_0M/experiments/FC2_200_0M_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2S_20_BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC3BN_nl_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
epoch = -1
run = 0

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3'] #, 'fc4']
fc_layers = ['fc1', 'fc2', 'fc3'] #, 'fc4']

comp_layers = ['fc1', 'fc2']
fc_layers = ['fc1', 'fc2', 'fc3']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET')

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)
device = 'cuda' if torch.cuda.is_available() else 'cpu'

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "R{}_E{}".format(run, epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable
eigenvals_real = None
E_UTAUs = None

def xxT_sigval():
    
    dim = 20
    pic_name = 'xxT_sigval_d{}_{}_fc1fc2'.format(dim, exp_name)
    E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers)
    E_xs = HM.expectation(HM.decomp.x_comp, comp_layers)

    plt.figure(figsize=(2.66, 2.7))
    
    gs = gridspec.GridSpec(1, len(comp_layers))

    for i, layer in enumerate(comp_layers):
        plt.subplot(gs[0, i])

        E_x, E_xxT = E_xs[layer], E_xxTs[layer]
        eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxT, symmetric=True)
        vals = eigenvals_xxT[:dim]
        # vals = eigenvals_xxT[10:25].to('cpu')
        # plt.scatter(np.arange(10, len(vals) + 10), vals, s=12)
        plt.scatter(np.arange(1, len(vals) + 1), vals, s=12)
        
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(layer)

    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def UTAU_sigval():
    
    dim = 30
    pic_name = 'UTAU_sigval_d{}_{}_fc1fc2'.format(dim, exp_name)
    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers)

    plt.figure(figsize=(2.66, 2.7))
    
    gs = gridspec.GridSpec(1, len(comp_layers))

    for i, layer in enumerate(comp_layers):
        plt.subplot(gs[0, i])

        E_UTAU = E_UTAUs[layer]
        eigenvals_UTAU, eigenvecs_UTAU = HM.utils.eigenthings_tensor_utils(E_UTAU, symmetric=True)
        vals = eigenvals_UTAU[:dim]
        # vals = eigenvals_UTAU[10:25].to('cpu')
        # plt.scatter(np.arange(10, len(vals) + 10), vals, s=12)
        plt.scatter(np.arange(1, len(vals) + 1), vals, s=12)
        
        plt.xlim([-1, dim + 1])
        plt.xticks([0, 10, 20, 30])
        plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
        plt.xlabel(layer)

    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')


# eigenthings_real = torch.load(eval_file, map_location='cpu')
# E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers) #, auto_grad=True, batchsize=16)
# E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, auto_grad=True, batchsize=16)
# eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=100)
# eigenvals_real = eigenthings_real['eigenvals_layer']
# xxT_UTAU_full_sigval('fc1')
# xxT_UTAU_full_sigval('fc2')

# xxT_sigval()
UTAU_sigval()