import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis
from matplotlib import rc
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
plt.style.use('seaborn-deep')

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.titlesize'] = 14

dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_nl_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC3BN_nl_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC3_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_sigmoid_fixlr0.01'
#dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
#dirc = '../CIFAR10_Exp1/experiments/LeNet5_sigmoid_fixlr0.01'
#dirc = '../CIFAR10_Exp1/experiments/LeNet5_tanh_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_tanh_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2LN_nl_inpnorm_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2_fixlr0.01_dMSE'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.1_dMSE'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_LN_nl_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC4_600_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC3_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_600copy_fixlr0.01'
epoch = -1
run = 1
dim = 200
storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = True
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']
comp_layers = ['fc1', 'fc2', 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']
fc_layers = ['fc1', 'fc2', 'fc3']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET', dim=dim)

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)
device = 'cuda' if torch.cuda.is_available() else 'cpu'

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "R{}_E{}".format(run, epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

if load_evals:
    eigenthings_real = torch.load(eval_file, map_location='cpu') 
    eigenvals_real = eigenthings_real['eigenvals_layer']
    eigenvecs_real = eigenthings_real['eigenvecs_layer']
    for k in eigenvecs_real.keys():
        print(eigenvecs_real[k].shape)


load_prev = False

xxT_snapshot = snapshot_file + "ExxT"
if os.path.isfile(xxT_snapshot) and load_prev:
    E_xxTs = torch.load(xxT_snapshot, map_location='cpu')
    print('Loaded FC xxT from previous computed result')
else:
    E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, auto_grad=False, batchsize=256, y_classification_mode='softmax')
    torch.save(E_xxTs, xxT_snapshot)
# component_compare.matvis_E_computed(HM, comp_layers, [UTAU, xxT], ['UTAU', 'xxT'], exp_name + 'ConvMX')
UTAU_snapshot = snapshot_file + "EUTAU"
if os.path.isfile(UTAU_snapshot) and load_prev:
    E_UTAUs = torch.load(UTAU_snapshot, map_location='cpu')
    print('Loaded FC UTAU from previous computed result')
else:
    #E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers)
    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, auto_grad=False, batchsize=256, y_classification_mode='softmax')
    torch.save(E_UTAUs, UTAU_snapshot)
    
def kron_decomp_traceoverlap(dim=80):
    
    pic_name = 'sample_kron_decomp_traceoverlap_d{}_{}'.format(dim, exp_name)
    overlap_x, overlap_y, _, _ = overlap_analysis.eigenspace_est_vs_real(HM, dim, comp_layers, eigenvals_real, eigenvecs_real)
    # overlap_x, overlap_y, _, _ = overlap_analysis.eigenspace_est_vs_real(HM, dim, comp_layers, eigenvals_real, eigenvecs_real, batchsize=16, auto_grad=True)

    plt.figure(figsize=(4, 3))
    plt.xlabel("Dimension of Dominating Eigenspace")

    for i, layer in enumerate(comp_layers):
        plt.plot(overlap_x[i], overlap_y[i], label=layer)
    plt.legend()
    plt.ylim([0, 1])
    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def kron_decomp_traceoverlap_test(dim=80):
    
    pic_name = 'sample_kron_decomp_traceoverlap_d{}_{}_narrow'.format(dim, exp_name)
    # overlap_x, overlap_y, _, _ = overlap_analysis.eigenspace_est_vs_real(HM, dim, comp_layers, eigenvals_real, eigenvecs_real, symmetric_exact=True)
    overlap_x, overlap_y, _, _ = overlap_analysis.eigenspace_est_vs_real_precomp(HM, dim, comp_layers, eigenvals_real, eigenvecs_real, E_UTAUs, E_xxTs)
    # overlap_x, overlap_y, _, _ = overlap_analysis.eigenspace_est_vs_real(HM, dim, comp_layers, eigenvals_real, eigenvecs_real, batchsize=16, auto_grad=True)

    plt.figure(figsize=(2.5, 2.5))
    plt.xlabel(r"Top $k$ Eigenspace", fontsize=12)

    for i, layer in enumerate(comp_layers):
        plt.plot(overlap_x[i], overlap_y[i], label=layer)
    plt.ylim([0, 1])
    plt.legend()
    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')


def eigenvals_compare():

    topn = 200
    pic_name = 'eigenval_compare_top{}_{}'.format(topn, exp_name)
    _, _, est_eigenval, real_eigenval = overlap_analysis.eigenspace_est_vs_real_precomp(HM, dim, comp_layers, eigenvals_real, eigenvecs_real, E_UTAUs, E_xxTs)
    # _, _, est_eigenval, real_eigenval = overlap_analysis.eigenspace_est_vs_real(HM, 100, comp_layers, eigenvals_real, eigenvecs_real, batchsize=16, auto_grad=True)
    plt.figure(figsize=(10, 6 * len(comp_layers)))
    
    gs = gridspec.GridSpec(len(comp_layers), 1)
    for i, layer in enumerate(comp_layers):
        plt.subplot(gs[i, 0])
        est, real = est_eigenval[i][:topn], real_eigenval[i][:topn]
        plt.scatter(np.arange(1, len(est) + 1), est, label='Approximated')
        plt.scatter(np.arange(1, len(real) + 1), real, label='Real')
        plt.legend()
        plt.title(layer)
    
    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def eigenvals_compare_single(dim=50):

    topn = 20
    pic_name = 'eigenval_compare_top{}_{}_narrow'.format(topn, exp_name)
    _, _, est_eigenval, real_eigenval = overlap_analysis.eigenspace_est_vs_real_precomp(HM, dim, comp_layers, eigenvals_real, eigenvecs_real, E_UTAUs, E_xxTs)
    for i, layer in enumerate(comp_layers):
        plt.figure(figsize=(2.5, 2.5))
        plt.xlabel(r"The $i$-th eigenvalue", fontsize=12)
        est, real = est_eigenval[i][:topn], real_eigenval[i][:topn]
        plt.scatter(np.arange(1, len(est) + 1), est, label='Approximated', s=8, marker='x')
        plt.scatter(np.arange(1, len(real) + 1), real, label='Exact', s=8)
        plt.legend()
        
        image_path = vis.get_image_path(pic_name + "_{}".format(layer), store_format='pdf')
        plt.tight_layout()
        plt.savefig(image_path)

# kron_decomp_traceoverlap(200)
kron_decomp_traceoverlap_test(80)
# eigenvals_compare()
eigenvals_compare_single()