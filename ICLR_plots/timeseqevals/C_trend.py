import torch
from copy import deepcopy
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def C_trend(HM: HessianModule, epochs, snapshots, layers, exp_name):

    assert len(epochs) == len(snapshots)
    inds = list(range(len(epochs)))

    Exp_dict = {layer: [] for layer in layers}
    Var_dict = {layer: [] for layer in layers}

    for i in inds:
        print("Computing Epoch {}".format(epochs[i]))
        sd = torch.load(snapshots[i], map_location=HM.device)
        HM.load_sd(sd)

        Exp = HM.expectation(HM.decomp.c_comp, layers, out_device=HM.device, to_cache=False, print_log=False)
        #Var = HM.variance(HM.decomp.c_comp, layers, out_device=HM.device, print_log=False)
        for layer in layers:
            Exp_dict[layer].append(Exp[layer].unsqueeze(-1))
            Var_dict[layer].append(Exp[layer].mul(1.-Exp[layer]).unsqueeze(-1))
    
    for layer in layers:

        fig_name = "c_expectation_trend_{}_{}".format(exp_name, layer)
        trend_base = torch.cat(Exp_dict[layer], dim=1).cpu().numpy()
        HM.vis.plots([epochs for i in range(len(trend_base))], trend_base, [str(i + 1) for i in range(len(trend_base))], fig_name, x_label='Epoch', y_label='E_c', fig_size=(12, 8))

        fig_name = "c_variance_trend_{}_{}".format(exp_name, layer)
        trend_base = torch.cat(Var_dict[layer], dim=1).cpu().numpy()
        HM.vis.plots([epochs for i in range(len(trend_base))], trend_base, [str(i + 1) for i in range(len(trend_base))], fig_name, x_label='Epoch', y_label='Var_c', fig_size=(12, 8))

def C_trend_label(HM: HessianModule, label, epochs, snapshots, layers, exp_name):

    assert len(epochs) == len(snapshots)
    inds = list(range(len(epochs)))

    Exp_dict = {layer: [] for layer in layers}
    Var_dict = {layer: [] for layer in layers}

    for i in inds:
        print("Computing Epoch {}".format(epochs[i]))
        sd = torch.load(snapshots[i], map_location=HM.device)
        HM.load_sd(sd)
        HM.set_remain_labels([label])
        Exp = HM.expectation(HM.decomp.c_comp, layers, out_device=HM.device, to_cache=False, print_log=False)
        #Var = HM.variance(HM.decomp.c_comp, layers, out_device=HM.device, print_log=False)
        for layer in layers:
            Exp_dict[layer].append(Exp[layer].unsqueeze(-1))
            Var_dict[layer].append(Exp[layer].mul(1.-Exp[layer]).unsqueeze(-1))
            #Var_dict[layer].append(Var[layer].unsqueeze(-1))
    
    for layer in layers:

        fig_name = "c_expectation_trend_{}_label_{}_{}".format(exp_name, label, layer)
        trend_base = torch.cat(Exp_dict[layer], dim=1).cpu().numpy()
        HM.vis.plots([epochs for i in range(len(trend_base))], trend_base, [str(i + 1) for i in range(len(trend_base))], fig_name, x_label='Epoch', y_label='E_c', fig_size=(12, 8))

        fig_name = "c_variance_trend_{}_label_{}_{}".format(exp_name, label, layer)
        trend_base = torch.cat(Var_dict[layer], dim=1).cpu().numpy()
        HM.vis.plots([epochs for i in range(len(trend_base))], trend_base, [str(i + 1) for i in range(len(trend_base))], fig_name, x_label='Epoch', y_label='Var_c', fig_size=(12, 8))
