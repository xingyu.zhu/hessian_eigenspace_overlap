import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, overlap_analysis
from tools import file_fetch
import visualization as vis

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
from matplotlib import rc

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
plt.style.use('ggplot')
plt.rcParams['xtick.labelsize']=14
plt.rcParams['ytick.labelsize']=14

dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
epoch = -1
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = True
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3']

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET')

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)
device = 'cuda' if torch.cuda.is_available() else 'cpu'

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

def xxT_sigval():
    
    dim = 20
    pic_name = 'xxT_sigval_d{}_{}'.format(dim, exp_name)
    E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers)
    E_xs = HM.expectation(HM.decomp.x_comp, comp_layers)

    plt.figure(figsize=(1.5 * len(comp_layers), 3))
    
    gs = gridspec.GridSpec(1, len(comp_layers))

    for i, layer in enumerate(comp_layers):
        plt.subplot(gs[0, i])

        E_x, E_xxT = E_xs[layer], E_xxTs[layer]
        eigenvals_xxT, eigenvecs_xxT = HM.utils.eigenthings_tensor_utils(E_xxT, symmetric=True)
        vals = eigenvals_xxT[:dim]
        plt.scatter(np.arange(1, len(vals) + 1), vals)
        plt.title(layer)
        if i == 0:
            plt.ylabel("Eigenvalue")

    image_path = vis.get_image_path(pic_name, store_format='pdf')
    plt.tight_layout()
    plt.savefig(image_path, bbox_inches='tight')

def test_autograd():

    layer = 'fc1'
    U_comp = HM.decomp.U_comp
    out_U = HM.sample_output(U_comp, [layer], sample_count=2)[layer]
    out_U = HM.sample_output(U_comp, [layer], sample_count=2, auto_grad=True)[layer]
    # print("close")
    # Us = HM.expectation(U_comp, [layer], to_cache=False)
    print("grad")
    Us = HM.expectation(U_comp, [layer], auto_grad=True, to_cache=False, batchsize=256)

    return 

test_autograd()