import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc
import visualization as vis

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

a = np.linspace(1, 10000, 100) + np.random.random(100) * 1000
dim = 10000
plt.figure(figsize=(4, 3))
    
plt.subplot(131)
plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
plt.scatter(a, a, s=12)
plt.xlim([-1, dim + 1])
plt.xlabel(r"$\mathbb{E}[\bm{xx}^T]$")

plt.subplot(132)
plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
plt.scatter(a, a, s=12)
plt.xlim([-1, dim + 1])
plt.xlabel(r"$\mathbb{E}[\bm{M}]$")

plt.subplot(133)
plt.ticklabel_format(axis='y', style='sci', scilimits=[-1, 1], useMathText=False)
plt.scatter(a, a, s=12)
plt.xlim([-1, dim + 1])
plt.xlabel(r"$\bm{H}_{\mathcal{L}}$")

image_path = vis.get_image_path('tmp', store_format='pdf')
plt.tight_layout(pad=1.02)
plt.savefig(image_path, bbox_inches='tight')