import os, sys
import torch
import numpy as np
from algos.cfhag import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.cfhag.visualization import vis
from algos.cfhag.measures import Measures
from algos.cfhag.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.cfhag.dp import OnDeviceDataLoader
from algos.cfhag.utils import *
import visualization as vis

import numpy as np
import matplotlib as mpl
from matplotlib import rc
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
mpl.use('Agg')
plt.style.use('ggplot')
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
params= {'text.latex.preamble' : [r'\usepackage{amsmath}',r'\usepackage{amssymb}', r'\usepackage{bm}']}
plt.rcParams.update(params)

dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
# dirc = '../MNIST_Exp1/experiments/WD_FC8_32_fixlr0.01'
epoch = -1
run = 2

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
load_prev = True
crop_ratio = 1
layer_seq = ['fc1', 'fc2', 'fc3']
# fc_layers = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5', 'fc6', 'fc7', 'fc8', 'fc9']#, 'fc3']
comp_layers = layer_seq

snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
if load_evals:
    eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET')

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64, remain_labels=remain_labels)

exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
dataset = conf.dataset(train=True, transform=conf.test_transform)
if remain_labels is not None:
    exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

if load_evals:
    eigenthings_real = torch.load(eval_file, map_location='cpu') 
    eigenvals_real = eigenthings_real['eigenvals_layer']
    eigenvecs_real = eigenthings_real['eigenvecs_layer']
else:
    eigenvals_real, eigenvecs_real = None, None

xxT_snapshot = snapshot_file + "ExxT"
if os.path.isfile(xxT_snapshot) and load_prev:
    xxTC = torch.load(xxT_snapshot, map_location='cpu')
    print('Loaded FC xxT from previous computed result')
else:
    xxTC = HM.expectation(HM.decomp.xxT_comp, comp_layers)
    torch.save(xxTC, xxT_snapshot)
# component_compare.matvis_E_computed(HM, comp_layers, [UTAU, xxT], ['UTAU', 'xxT'], exp_name + 'ConvMX')

xC_snapshot = snapshot_file + "Ex"
if os.path.isfile(xC_snapshot) and load_prev:
    xC = torch.load(xC_snapshot, map_location='cpu')
    print('Loaded FC xC from previous computed result')
else:
    xC = HM.expectation(HM.decomp.x_comp, comp_layers)
    torch.save(xC, xC_snapshot)

for layer in comp_layers:

    x = xC[layer]
    xxT = xxTC[layer]

    ExExT = torch.Tensor.matmul(x, x.transpose(-1, -2))
    cov = xxT - ExExT
    U, S, V = xxT.svd()
    Uc, Sc, Vc = cov.svd()

    spec_gap = (S[0]/S[1]).item()
    fro_ratio = (S[0]/S.square().sum().sqrt()).item()
    Ux, Sx, Vx = x.svd()
    Uxx, Sxx, Vxx = ExExT.svd()
    x_principle = Ux[:, 0]
    xxT_principle = U[:, 0]
    cov_principle = Uc[:, 0]
    ExExT_principle = Uxx[:, 0]
    overlap = torch.Tensor.dot(xxT_principle, x_principle)
    overlap_cov = torch.Tensor.dot(ExExT_principle, cov_principle)
    overlap_ExExT = torch.Tensor.dot(ExExT_principle, xxT_principle)
    spec_ratio = Sxx[0] / Sc[0]

    print('\n' + layer)
    print("overlap:\t{}".format(overlap))
    print("overlap ExxT, ExExT:\t{}".format(overlap_ExExT))
    print("overlap cov, ExExT:\t{}".format(overlap_cov))
    print("spec ratio:\t{}".format(spec_ratio))
    print("spec gap:\t{}\nfro_ratio:\t{}".format(spec_gap, fro_ratio))
    print("fro cov:\t{}\nfro xxT:\t{}\nfro ExExT:\t{}".format(torch.norm(cov), torch.norm(xxT), torch.norm(ExExT)))