import torch
import sys, os
from IPython import embed
from utils import *

from algos.cfhag import hessianmodule
from algos.closeformhessian import hessianmodule as hessianmodulelinear
from IPython import embed

# MNIST - LeNet5
root_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01'
log_lw_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01/experiment_log/run_1/models/final.pth_ET1000.eval'
log_full_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01/experiment_log/run_1/models/final.pth_ET1000.eval'
model_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01/experiment_log/run_1/models/final.pth'
layer_seq = ['conv1', 'conv2', 'fc1', 'fc2', 'fc3']
fc_layers_seq = ['fc1', 'fc2', 'fc3']
with_bias = True

exp_name = '{}_{}'.format(root_loc.split('/')[-3], root_loc.split('/')[-1])

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

log_full = torch.load(log_full_loc, map_location=dev)
log_lw = torch.load(log_lw_loc, map_location=dev)

vecs_full = torch.from_numpy(log_full['eigenvecs']).to(dev)
# vecs_lw = log_lw['eigenvecs_layer']
# for k in vecs_lw.keys():
#     vecs_lw[k] = torch.from_numpy(vecs_lw[k]).to(dev)
# partitions = log_lw['layerinfo']

assert os.path.isdir(root_loc)
sys.path.append(root_loc)
from config import Config #pylint: disable=no-name-in-module
conf = Config()

shape_info = {}
net = conf.net()
for name, var in net.named_parameters():
    n_entries = len(var.view(-1))
    shape_info[name] = [var.shape[0], n_entries // var.shape[0]]

net.load_state_dict(torch.load(model_loc, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64, fc_layer_seq=fc_layers_seq)

ExxTCs = HM.expectation(HM.decomp.xxTC_comp, layer_seq)
ExCs = HM.expectation(HM.decomp.xC_comp, layer_seq)
# EUCTALPh = HM.expectation(HM.decomp.UCTALPh_comp, layer_seq)
# EFTAF = HM.expectation(HM.decomp.FCTAFC_comp, layer_seq)[layers[0]]

for layer in ['conv1', 'conv2']:
    print(ExxTCs[layer].shape)
    sig, vecs = HM.utils.eigenthings_tensor_utils(ExxTCs[layer])
    print(layer, sig)
    v = vecs[0]
    ExC = ExCs[layer]
    Ex_flatten = torch.sum(ExC, dim=-1)
    Ex_flatten.div_(Ex_flatten.norm())
    print(Ex_flatten.dot(v).square())



embed()
exit()
# EUCT = HM.expectation(HM.decomp.UCT_comp, layer_seq, batchsize=512)
# EU = HML.expectation(HML.decomp.U_comp, fc_layers_seq, batchsize=128)

embed()



# EUCTs = HM.expectation(HM.decomp.UCT_comp, layer_seq, batchsize=256)
exit()
ExxT_eigenvecs = {}
for layer in layer_seq:
    
    Ex = Exs[layer].clone()
    Ex_normalized = Exs[layer].view(-1).div(Exs[layer].norm())

    ExxT = ExxTs[layer]
    xxT_sigma, xxT_vs = HM.utils.eigenthings_tensor_utils(ExxT, symmetric=True)
    xxT_eigenvec = xxT_vs[0]

    print("Layer {}, Dot squared (E[x] vs E[xxT] eigenvec): {}".format(layer, xxT_eigenvec.dot(Ex_normalized)))
    # print(Ex.norm(), xxT_sigma[0].sqrt())
    
    ExxT_eigenvecs[layer] = xxT_vs[0].unsqueeze(-1) * xxT_sigma[0].sqrt()
    print(ExxT_eigenvecs[layer].norm(), Ex.norm())

# exit()
EFTAF = HM.expectation(HM.decomp.FTAF_comp, layer_seq)[layer_seq[0]]

def approx_fulleigenvec(vxs, vFTAF, split_inds):
    ret = []
    for i, layer in enumerate(layer_seq):
        s, e = split_inds[layer]
        vFTAF_crop = vFTAF[s : e]
        v_kron_x = vFTAF_crop.unsqueeze(-1).matmul(vxs[layer].transpose(-1, -2)).view(-1)
        if with_bias:
            v_kron_x = torch.cat([v_kron_x, vFTAF_crop])
        ret.append(v_kron_x)
        # print(len(v_kron_x))
    vec = torch.cat(ret)
    vec.div_(vec.norm())
    return vec


split_inds = {}
s = 0
for i, w in enumerate(HM.Ws.__reversed__()):
    e = s + w.shape[0]
    split_inds[layer_seq[i]] = [s, e]
    s = e

print(split_inds)
approx_vecs = []
out_sigmas, out_vs = HM.utils.eigenthings_tensor_utils(EFTAF, symmetric=True)
# print(out_sigmas, out_vs.shape)
for i, v in enumerate(out_vs[:10]):
    v_full_approx_Ex = approx_fulleigenvec(Exs, v, split_inds)
    v_full_approx_ExxT = approx_fulleigenvec(ExxT_eigenvecs, v, split_inds)
    eigenvec_full_real = vecs_full[i]
    print(i, torch.norm(v_full_approx_Ex), torch.norm(v_full_approx_ExxT), torch.norm(eigenvec_full_real))
    assert len(eigenvec_full_real) == len(v_full_approx_Ex)

    overlaps = [eigenvec_full_real.dot(v_full_approx_Ex).square(), eigenvec_full_real.dot(v_full_approx_ExxT).square()]
    print("Overlap (Ex): {:.5g} Overlap (ExxT): {:.5g}".format(*overlaps))