import torch
import sys, os
from IPython import embed
from utils import *

# # CIFAR100 - VGG
# root_loc = '../CIFAR100/experiments/VGG11NN_nobn_fixlr0.01'
# log_lw_loc = '../CIFAR100/experiments/VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_LW_part_t18_l200.eval'
# log_full_loc = '../CIFAR100/experiments/VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_ET200.eval'
# model_loc = '../CIFAR100/experiments/VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth'

# # CIFAR100 - ResNet18
# root_loc = '../CIFAR100/experiments/Resnet18NN_nobn_fixlr0.01'
# log_lw_loc = '../CIFAR100/experiments/Resnet18NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_LW_part_t100_l500.eval'
# log_full_loc = '../CIFAR100/experiments/Resnet18NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_ET500.eval'
# model_loc = '../CIFAR100/experiments/Resnet18NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth'

# # CIFAR10 - LeNet5
# root_loc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
# log_lw_loc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET1000.eval'
# log_full_loc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET1000.eval'
# model_loc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01/experiment_log/run_1/models/final.pth'

# # CIFAR10 - VGG
# root_loc = '../CIFAR10_Exp1/experiments/VGG11NN_nobn_fixlr0.01'
# log_lw_loc = '../CIFAR10_Exp1/experiments/VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_LW_part_t18_l400.eval'
# log_full_loc = '../CIFAR10_Exp1/experiments/VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_ET400.eval'
# model_loc = '../CIFAR10_Exp1/experiments/VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth'

# MNIST - FC2
root_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
log_lw_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET1000.eval'
log_full_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET1000.eval'
model_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_1/models/final.pth'

# # MNIST - LeNet5
# root_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01'
# log_lw_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01/experiment_log/run_1/models/final.pth_ET1000.eval'
# log_full_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01/experiment_log/run_1/models/final.pth_ET1000.eval'
# model_loc = '../MNIST_Exp1/experiments/LeNet5_fixlr0.01/experiment_log/run_1/models/final.pth'


exp_name = '{}_{}'.format(root_loc.split('/')[-3], root_loc.split('/')[-1])

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

log_full = torch.load(log_full_loc, map_location=dev)
log_lw = torch.load(log_lw_loc, map_location=dev)

vecs_full = torch.from_numpy(log_full['eigenvecs']).to(dev)
vecs_lw = log_lw['eigenvecs_layer']
for k in vecs_lw.keys():
    vecs_lw[k] = torch.from_numpy(vecs_lw[k]).to(dev)
partitions = log_lw['layerinfo']


def orthogonalize_scaled(m, topn=10000):
    print(m.shape)
    topn = min([len(m), len(m[0]), topn])
    m.div_(m.norm(dim=1, keepdim=True))
    scales = torch.zeros(topn)
    scales[0] = 1
    basis = torch.zeros([topn, len(m[0])]).to(dev)
    basis[0] = m[0]

    # Gram Schmit
    for i in range(1, len(basis)):
        v = m[i].clone()
        proj_mat = basis[:i]
        proj_norms = proj_mat.matmul(v.unsqueeze(-1))
        proj_v = proj_norms.t().matmul(proj_mat).squeeze(0)
        v -= proj_v
        scales[i] = v.norm()
        basis[i] = v / scales[i]
    # embed()
    return m[:topn], basis, scales

def overlap_calc(basis, v_lw):
    dot_mat = v_lw.matmul(basis.t())
    overlap_mat = torch.square(dot_mat)
    return overlap_mat

def overlap(v_lw, v_full, seg, layer):
    s, e = seg
    v_corr = v_full[:, s:e].clone()
    vec_set, full_basis, full_basis_scale = orthogonalize_scaled(v_corr)
    
    orth_overlap_mat = overlap_calc(full_basis, v_lw).cpu()
    nonorth_overlap_mat = overlap_calc(vec_set, v_lw).cpu()
    # plot_single("FullvsLW_nonorth_{}_{}".format(exp_name, layer), nonorth_overlap_mat[:20], title='overlap nonorth {} {}'.format(exp_name, layer))
    return orth_overlap_mat, nonorth_overlap_mat, full_basis_scale
    

layers = []
ovlps = {}
ovlps_scaled = {}
nonorth_ovlps = {}
param_size = {}
for layer in partitions.keys():
    s, e = partitions[layer]
    if 'bias' in layer:
        continue
    layers.append(layer)
    ovlp, nonorth_ovlp, basis_scale = overlap(vecs_lw[layer], vecs_full, [s, e], layer)
    ovlps[layer] = ovlp
    print(ovlp.shape, basis_scale.shape)
    ovlps_scaled[layer] = ovlp * basis_scale.unsqueeze(0)
    nonorth_ovlps[layer] = nonorth_ovlp
    param_size[layer] = e - s

plot_overlaps('LW_F_ovlp_' + exp_name, ovlps, layers, param_size, topn=200, subspace_dims=[10, 20, 30, 50, 100, 200])
plot_overlaps('LW_F_ovlp_weighted_' + exp_name, ovlps_scaled, layers, param_size, topn=200, subspace_dims=[10, 20, 30, 50, 100, 200])
# exit()