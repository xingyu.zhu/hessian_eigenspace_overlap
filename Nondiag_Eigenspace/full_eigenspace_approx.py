import torch
import sys, os
from IPython import embed
from utils import *
import argparse

from algos.closeformhessian import hessianmodule

import visualization as vis
from matplotlib import rc
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

import matplotlib as mpl
mpl.use('Agg')
from matplotlib import cm
from matplotlib import gridspec
import matplotlib.pyplot as plt
plt.style.use('seaborn-deep')

plt.style.use('seaborn-deep')
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.titlesize'] = 14

dim = 100



# MNIST - FC2nb
root_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
log_lw_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_2/models/final.pth_LW_ET1000.eval'
log_full_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_2/models/final.pth_LW_ET1000.eval'
model_loc = '../MNIST_Exp1/experiments/FC2_fixlr0.01/experiment_log/run_2/models/final.pth'
layer_seq = ['fc1', 'fc2', 'fc3']

root_loc = '../MNIST_Exp1/experiments/FC3_fixlr0.01'
log_lw_loc = '../MNIST_Exp1/experiments/FC3_fixlr0.01/experiment_log/run_2/models/final.pth_LW_ET1000.eval'
log_full_loc = '../MNIST_Exp1/experiments/FC3_fixlr0.01/experiment_log/run_2/models/final.pth_LW_ET1000.eval'
model_loc = '../MNIST_Exp1/experiments/FC3_fixlr0.01/experiment_log/run_2/models/final.pth'
layer_seq = ['fc1', 'fc2', 'fc3', 'fc4']

# MNIST - FC4nb
#root_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01'
#log_lw_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET500.eval'
#log_full_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth_LW_ET500.eval'
#model_loc = '../MNIST_Exp1/experiments/FC4nb_fixlr0.01/experiment_log/run_1/models/final.pth'
#layer_seq = ['fc1', 'fc2', 'fc3', 'fc4', 'fc5']

exp_name = '{}_{}'.format(root_loc.split('/')[-3], root_loc.split('/')[-1])

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

log_full = torch.load(log_full_loc, map_location=dev)
log_lw = torch.load(log_lw_loc, map_location=dev)

vecs_full = torch.from_numpy(log_full['eigenvecs']).to(dev)
vecs_lw = log_lw['eigenvecs_layer']
for k in vecs_lw.keys():
    vecs_lw[k] = torch.from_numpy(vecs_lw[k]).to(dev)
partitions = log_lw['layerinfo']

assert os.path.isdir(root_loc)
sys.path.append(root_loc)
from config import Config #pylint: disable=no-name-in-module
conf = Config()

shape_info = {}
net = conf.net()
for name, var in net.named_parameters():
    n_entries = len(var.view(-1))
    shape_info[name] = [var.shape[0], n_entries // var.shape[0]]

net.load_state_dict(torch.load(model_loc, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, layer_seq, RAM_cap=64)
Exs = HM.expectation(HM.decomp.x_comp, layer_seq)
ESUTAPhs = HM.expectation(HM.decomp.stack_UTALPh_comp, layer_seq)
EFTAF = HM.expectation(HM.decomp.FTAF_comp, layer_seq)[layer_seq[0]]
#print(EFTAF.shape)
# ExCs = HM.expectation(HM.decomp.xC_comp, layer_seq)

def approx_fulleigenvec(vxs, vFTAF, split_inds):
    ret = []
    for i, layer in enumerate(layer_seq):
        s, e = split_inds[layer]
        vFTAF_crop = vFTAF[s : e]
        v_kron_x = vFTAF_crop.unsqueeze(-1).matmul(vxs[layer].transpose(-1, -2)).view(-1)
        v_kron_x = torch.cat([v_kron_x, vFTAF_crop])
        ret.append(v_kron_x)
        #print(len(v_kron_x))
    return torch.cat(ret)


split_inds = {}
s = 0
for i, w in enumerate(HM.Ws.__reversed__()):
    e = s + w.shape[0]
    split_inds[layer_seq[i]] = [s, e]
    s = e

res= []

print(split_inds)
approx_vecs = []
out_sigmas, out_vs = HM.utils.eigenthings_tensor_utils(EFTAF, symmetric=True)
#print(out_sigmas, out_vs.shape)
eigspace_approx = None
for i, v in enumerate(out_vs[:dim]):
    v_full_approx = approx_fulleigenvec(Exs, v, split_inds)
    v_full_approx.div_(torch.norm(v_full_approx))
    if eigspace_approx is None:
        eigspace_approx = v_full_approx.unsqueeze(1)
    else:
        eigspace_approx = torch.cat([eigspace_approx, v_full_approx.unsqueeze(1)], dim=1)
    #print(len(v_full_approx))
    eigenvec_full_real = vecs_full[i]
    print(i, torch.norm(v_full_approx), torch.norm(eigenvec_full_real))
    assert len(eigenvec_full_real) == len(v_full_approx)
    print(eigenvec_full_real.dot(v_full_approx).square())
    res.append(torch.norm(vecs_full[:i+1].matmul(eigspace_approx)).square().div(i+1).item())

print(res)

pic_name = 'full_eigenspace_approx_traceoverlap_d{}_{}'.format(dim, exp_name)
plt.figure(figsize=(2.5, 2.5))
plt.xlabel(r"Top $k$ Eigenspace", fontsize=12)
plt.plot(torch.arange(dim), res)
plt.ylim([0, 1])
image_path = vis.get_image_path(pic_name, store_format='pdf')
plt.tight_layout()
plt.savefig(image_path, bbox_inches='tight')