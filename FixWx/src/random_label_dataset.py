import os
import torch
from torch.utils.data import Dataset

class LinearData(Dataset):
    def __init__(self, file_name, root_dir ='/usr/xtmp/CSPlus/VOLDNN/Datasets/Linear/', random_label=False, train=True):
        self.file_name = file_name
        self.root_dir = root_dir
        self.random_label = random_label
        self.file_path = os.path.join(root_dir, file_name)
        data_file = torch.load(self.file_path)
        self.d = data_file['d']
        self.o = data_file['o']
        self.W = data_file['W']
        if train:
            self.data = data_file['train']
        else:
            self.data = data_file['test']
             
    def __len__(self):
        return self.data['n']
    
    def __getitem__(self, idx):
        inputs = self.data['x'][idx]
        if self.random_label:
            labels = int(self.data['y_rand'][idx])
        else:
            labels = int(self.data['y'][idx])
        return inputs, labels
        