import os, sys, argparse
import torch
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-run', type=int, help='The indicator of run (default=1)', default=1)
parser.add_argument('-d', type=int, default=100)
parser.add_argument('-o', type=int, default=10)
parser.add_argument('-test', type=int, default=128)
parser.add_argument('-train', type=int, default=128)
args = parser.parse_args()

base_path = '/usr/xtmp/CSPlus/VOLDNN/Datasets/Linear/'
dataset_path = os.path.join(base_path, 'd{}_o{}_tr{}_te{}_r{}.pt'.format(args.d, args.o, args.train, args.test, args.run))

out = dict()
out['d'] = args.d
out['o'] = args.o
W = torch.normal(mean=0.0, std=1.0, size=(args.d, args.o))
out['W'] = W
x_lst=[]
y_lst=[]
y_rand_lst=[]
for i in range(args.train):
    x = torch.normal(mean=1.0, std=1.0, size=(args.d, ))
    x = x.div_(torch.norm(x))
    y = torch.argmax(x.matmul(W)).item()
    y_rand = np.random.randint(args.o)
    x_lst.append(x)
    y_lst.append(y)
    y_rand_lst.append(y_rand)

out['train'] = {'n': args.train,
                'x': torch.stack(x_lst),
                'y': torch.IntTensor(y_lst),
                'y_rand': torch.IntTensor(y_rand_lst)}

x_lst=[]
y_lst=[]
y_rand_lst=[]
for i in range(args.train):
    x = torch.normal(mean=1.0, std=1.0, size=(args.d, ))
    x = x.div_(torch.norm(x))
    y = torch.argmax(x.matmul(W)).item()
    y_rand = np.random.randint(args.o)
    x_lst.append(x)
    y_lst.append(y)
    y_rand_lst.append(y_rand)

out['test'] = { 'n': args.test,
                'x': torch.stack(x_lst),
                'y': torch.IntTensor(y_lst),
                'y_rand': torch.IntTensor(y_rand_lst)}

torch.save(out, dataset_path)
print('dataset saved')
