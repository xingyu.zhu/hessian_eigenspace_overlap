import torch
import torch.nn as nn
import torch.nn.functional as F

class FC1(nn.Module):
    def __init__(self):
        super(FC1, self).__init__()
        self.fc1   = nn.Linear(100, 10)

    def forward(self, x):
        out = self.fc1(x)
        return out