import torch
import torch.nn as nn
import torch.nn.functional as F

class CPFF(nn.Module):
    def __init__(self):
        super(CPFF, self).__init__()
        self.conv1 = nn.Conv2d(1, 12, 5)
        self.fc1   = nn.Linear(12*8*8, 40)
        self.fc2   = nn.Linear(40, 20)
        self.fc3   = nn.Linear(20, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 3)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out