import torch
import numpy as np
import sys

n = 3
B = np.arange(2*n*n).reshape(2,n,n)

b = torch.from_numpy(B)
print(b)
b[1] *= b[1]
print(b)