import os
import sys
import datetime, time
from torch import optim, nn
import torchvision.transforms as transforms
from dataset import LinearData
from networks import Linear
import torch.nn as nn
import numpy as np

class Config():

    def __init__(self, run_number=-1):

        # Basic configuration
        self.base_storage_path = '/usr/xtmp/CSPlus/VOLDNN/Shared/train_log/eigenspace_overlap_linear_100_10'
        self.experiment_path = os.path.join(self.base_storage_path, os.getcwd().split('/')[-3], os.getcwd().split('/')[-1], "run_{}".format(run_number))
        self.experiment_path_root = os.path.join(self.base_storage_path, os.getcwd().split('/')[-3], os.getcwd().split('/')[-1])
        
        self.vis_dir = os.path.join(self.experiment_path, 'vis')
        self.model_path = os.path.join(self.experiment_path, 'models')
        self.log_path = os.path.join(self.experiment_path, 'log')

        self.run_number = run_number
        self.transforms = self.Transforms()
        self.datasets = self.Datasets()
        
        self.use_gpu = True
        self.full_batch_eval = True

        # Basic Information
        self.net = Linear.FC1
        self.dataset = self.datasets.dataset_linear

        # Training
        self.train_provider_count = 1
        self.training_batchsize = 128
        
        self.epoch_num = 100000
        self.snapshot_freq = 1000
        self.eigenthings_topn = 1000
        self.auto_eval_thread_num = -1
        self.layerwise_eval = True

        self.criterion = nn.CrossEntropyLoss()
        self.stop_by_criterion = 1e-6
        self.start_lr = 0.01

        # Testing
        self.test_provider_count = 1
        self.testing_batchsize = 128

        self.train_transform = None
        self.test_transform = None
        
        self.local_explog_softlink = "./experiment_log"
        
        # Creating Symlink to training log under the shared folder.
        if run_number != -1:
            if os.path.islink(self.local_explog_softlink):
                if os.readlink(self.local_explog_softlink) != self.experiment_path_root:
                    os.system("rm -f {}".format(self.local_explog_softlink))
                    time.sleep(1)
                    os.system("ln -s {} {}".format(self.experiment_path_root, self.local_explog_softlink))
                    print("trainlog symlink incorrect, recreated")
                else:
                    print("symlink created already")
            else:
                os.system("ln -s {} {}".format(self.experiment_path_root, self.local_explog_softlink))
                print("trainlog symlink created")

    def eval_policy(self, i: int):
        # if i < 3:
        #     return True
        # if i < 50:
        #     if i % 10 == 0:
        #         return True
        if i % 100 == 0:
            return True
        else:
            return False
            
    def optimizer_conf(self, net):
        # optimizer = optim.SGD(net.parameters(), lr=0.1, momentum=0.9)
        optimizer = optim.SGD(net.parameters(), lr=self.start_lr)
        return optimizer
    
    def lr_scheduler_conf(self, optimizer):
        # lr_scheduler = optim.lr_scheduler.StepLR(optimizer, 50, gamma=0.1)
        lr_scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma=1, last_epoch=-1) # Constant
        return lr_scheduler
    
    class Transforms():

        def __init__(self):
            self.naive_transform = transforms.Compose(
                [transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
            )
            
            self.rgb_normalized_crop_flip = transforms.Compose([
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
            ])

            self.rgb_normalized = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
            ])
    
    class Datasets():

        def __init__(self):
            self.choices = ['CIFAR10']

        def dataset_linear(self, train, transform):
            return LinearData(file_name='d100_o10_tr128_te128_r2.pt', train=train, random_label=True)
