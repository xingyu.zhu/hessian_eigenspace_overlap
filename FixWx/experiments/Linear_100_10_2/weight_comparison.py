import os, sys
import torch
import numpy as np
from collections import OrderedDict
sys.path.append(os.getcwd())
from config import Config # pylint: disable=no-name-in-module
from algos.closeformhessian import hessianmodule

sd = "./experiment_log/run_1/models/final.pth"
real_comp = "./experiment_log/run_1/models/final.pth_LW_ET1000.eval"
exp_name = os.getcwd().split('/')[-1]

conf = Config()
net = conf.net()
net.load_state_dict(torch.load(sd, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, ['fc1'], RAM_cap=64)
comp_layers = ['fc1']

eigenthings_real = torch.load(real_comp, map_location='cpu')
eigenvals_real = eigenthings_real['eigenvals_layer']
eigenvecs_real = eigenthings_real['eigenvecs_layer']

HM.load_Ws()
W = HM.Ws[0]
dW = dataset.W.transpose(0,1)
print(W.shape)
print(dW.shape)
print(W)
print(dW)
W_norm = W
dW_norm = dW
for i in range(W.shape[0]):
    W_norm[i] = W[i]/torch.norm(W[i])
    dW_norm[i] = dW[i]/torch.norm(dW[i])

overlap= HM.measure.trace_overlap(W_norm, dW_norm)
print('colum_overlap: {}'.format(overlap))
for j in range(W.shape[1]):
    W_norm[:,j] = W[:,j]/torch.norm(W[:,j])
    dW_norm[:,j] = dW[:,j]/torch.norm(dW[:,j])

overlap= HM.measure.trace_overlap(W_norm.transpose(0,1), dW_norm.transpose(0,1))
print('row_overlap: {}'.format(overlap))
quotient = W.div(dW.to('cuda'))
print(quotient)
quotient = quotient.div(quotient[0,0])
print(torch.norm(quotient.sub(torch.ones_like(quotient))))