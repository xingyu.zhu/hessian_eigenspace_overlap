import torch
import torch.nn as nn
import torch.nn.functional as F

class FC1S_20_0M_d1(nn.Module):
    def __init__(self):
        super(FC1S_20_0M_d1, self).__init__()
        self.fc1 = nn.Linear(14*14, 20)
        self.fc2 = nn.Linear(20, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc2(out)
        return out

class FC1S_40_0M_d1(nn.Module):
    def __init__(self):
        super(FC1S_40_0M_d1, self).__init__()
        self.fc1 = nn.Linear(14*14, 40)
        self.fc2 = nn.Linear(40, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc2(out)
        return out

class FC2S_20_0M_d1(nn.Module):
    def __init__(self):
        super(FC2S_20_0M_d1, self).__init__()
        self.fc1 = nn.Linear(14*14, 20)
        self.fc2 = nn.Linear(20, 20)
        self.fc3 = nn.Linear(20, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out

class FC2S_40_0M_d1(nn.Module):
    def __init__(self):
        super(FC2S_40_0M_d1, self).__init__()
        self.fc1 = nn.Linear(14*14, 40)
        self.fc2 = nn.Linear(40, 40)
        self.fc3 = nn.Linear(40, 1)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out