# Hessian_eigenspace_overlap

## Investigating the nontrivial overlap of the domination eigenspaces of the hessian of different local minimas.

Current observation shows that for LeNet5 on CIFAR10, the subspace spanned by the eigenvectors corresponding to dominating eigenvalues has a nontrivial overlap. SVD results shows that the overlap is a certain subspace of around 180 dimensions. This experiment aims to further investigate this observation on MNIST and CIFAR10. We will be primarily using LeNet (and its derived structures) and fully connected small networks.

**The Non-trivial Overlap:**
![](https://i.ibb.co/qxBvGJS/base-noconv.jpg)

**The overlapping dimension:**
![](https://i.ibb.co/1GCz2CR/sigma-2-distribution-8models-epoch1000-dim200.jpg)