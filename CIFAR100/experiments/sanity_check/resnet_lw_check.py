import os, sys
import torch
from tools import file_fetch
from algos.closeformhessian.dp import OnDeviceDataLoader
from algos.closeformhessian.utils import prepare_net
from algos.hessian_eigenthings import get_partition

dirc = '../VGG11NN_nobn_fixlr0.01'

epoch = -1
run = 1

assert os.path.isdir(dirc)
sys.path.append(dirc)
from config import Config # pylint: disable=no-name-in-module
conf = Config()

net = conf.net()
net, dev, _ = prepare_net(net)
layer_seq = []
snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)

net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
partition = get_partition(net)
exit()

dataset = conf.dataset(train=True, transform=conf.test_transform)
dl = OnDeviceDataLoader(dataset, device=dev)

# get partition indices



layer_num = 0
for _ in net.named_parameters():
    layer_num += 1

partition = get

