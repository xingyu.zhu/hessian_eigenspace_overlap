import torch
import sys, os
from IPython import embed
from utils import *


root_loc = '../VGG11NN_nobn_fixlr0.01'
log_lw_loc = '../VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_LW_part_t18_l200.eval'
log_full_loc = '../VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth_ET200.eval'
model_loc = '../VGG11NN_nobn_fixlr0.01/experiment_log/run_1/models/final.pth'

exp_name = root_loc.split('/')[-1]

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

log_full = torch.load(log_full_loc, map_location=dev)
log_lw = torch.load(log_lw_loc, map_location=dev)

vecs_full = torch.from_numpy(log_full['eigenvecs']).to(dev)
vecs_lw = log_lw['eigenvecs_layer']
for k in vecs_lw.keys():
    vecs_lw[k] = torch.from_numpy(vecs_lw[k]).to(dev)
partitions = log_lw['layerinfo']


def orthogonalize_scaled(m, topn=10000):
    print(m.shape)
    topn = min([len(m), len(m[0]), topn])
    m.div_(m.norm(dim=1, keepdim=True))
    scales = torch.zeros(len(m[0]))
    scales[0] = 1
    basis = torch.zeros([topn, len(m[0])]).to(dev)
    basis[0] = m[0]

    # Gram Schmit
    for i in range(1, len(basis)):
        v = m[i].clone()
        proj_mat = basis[:i]
        proj_norms = proj_mat.matmul(v.unsqueeze(-1))
        proj_v = proj_norms.t().matmul(proj_mat).squeeze(0)
        v -= proj_v
        scales[i] = v.norm()
        basis[i] = v / scales[i]
    # embed()
    return m[:topn], basis, scales

def overlap_calc(basis, v_lw):
    dot_mat = v_lw.matmul(basis.t())
    overlap_mat = torch.square(dot_mat)
    ovlp = overlap_mat.sum(dim=1)
    return overlap_mat, ovlp.cpu().numpy()

def overlap(v_lw, v_full, seg, layer):
    s, e = seg
    v_corr = v_full[:, s:e].clone()
    vec_set, full_basis, full_basis_scale = orthogonalize_scaled(v_corr, topn=10)
    
    orth_overlap_mat, orth_subspace_overlap = overlap_calc(full_basis, v_lw)
    plot_single("FullvsLW_{}_{}".format(exp_name, layer), orth_overlap_mat[:20], title='overlap {} {}'.format(exp_name, layer))
    print(orth_subspace_overlap[:30])

    nonorth_overlap_mat, nonorth_subspace_overlap = overlap_calc(vec_set, v_lw)
    plot_single("FullvsLW_nonorth_{}_{}".format(exp_name, layer), nonorth_overlap_mat[:20], title='overlap nonorth {} {}'.format(exp_name, layer))
    print(nonorth_subspace_overlap[:30])

for layer in partitions.keys():
    s, e = partitions[layer]
    if 'bias' in layer:
        continue
    print(layer)

    res = overlap(vecs_lw[layer], vecs_full, [s, e], layer)
    # exit()