import numpy as np
import torch

path = './experiment_log/run_1/models/final.pth_LW_ET1000.eval'
rep = 100
data = torch.load(path)
eigenvecs_dict = data['eigenvecs_layer']
for var_name in eigenvecs_dict.keys():
    print(var_name)
    res = []
    eigenvecs = eigenvecs_dict[var_name]
    n = eigenvecs.shape[0]
    for i in range(rep):
        inds = np.random.choice(n, 2)
        if inds[0] == inds[1]:
            continue
        prod = np.inner(eigenvecs[inds[0]], eigenvecs[inds[1]])
        res.append(prod)
    print(res)
    print(np.mean(res))
    print(np.var(res))