import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

dirc = '../CIFAR10_Exp1/experiments/LeNet5_AdamDefault'
dirc = '../MNIST_Exp1/experiments/FC2_narrow30_fixlr0.01'
#dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
#dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
epoch = 500
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
#comp_layers = ['fc1', 'fc2']
fc_layers = ['fc1', 'fc2', 'fc3']

def A_approx_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    
    layers = [comp_layers[0]]
    dp_comp = HM.decomp.dp_comp
    AL_comp = HM.decomp.AL_comp
    ALPh_comp = HM.decomp.ALPh_comp
    # Adcp_comp = HM.decomp.Adcp_comp
    ALTdpAL_comp = HM.tsa.chain_comp([AL_comp, dp_comp, AL_comp], [0, 0, 1])
    ALPhALPhT_comp = HM.tsa.chain_comp([ALPh_comp, ALPh_comp], [0, 1])
    A_comp = HM.decomp.A_comp

    component_compare.matvis_E(HM, layers, [ALTdpAL_comp, A_comp, ALPhALPhT_comp], exp_name + 'A_dcp')
    # component_compare.matvis_E(HM, layers, [Adcp_comp, A_comp], exp_name + 'A_dcp')
    return

def UTAU_approx_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    
    UTALPh_comp = HM.decomp.UTALPh_comp
    UTAU_comp = HM.decomp.UTAU_comp
    UTAU_Chdcp_comp = HM.tsa.chain_comp([UTALPh_comp, UTALPh_comp], [0, 1])

    component_compare.matvis_E(HM, comp_layers, [UTAU_comp, UTAU_Chdcp_comp], exp_name + 'UTAU_dcp')
    # component_compare.matvis_E(HM, layers, [Adcp_comp, A_comp], exp_name + 'A_dcp')
    return

def UTALPh_analysis(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    UTALPh_comp = HM.decomp.UTALPh_comp
    E_UTALPh = HM.expectation(UTALPh_comp, comp_layers)
    print(E_UTALPh)

def tasks(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    # A_approx_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)
    # UTAU_approx_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)
    UTALPh_analysis(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)

def main():
    snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
    if load_evals:
        eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_ET')

    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    if remain_labels is not None:
        exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

    if load_evals:
        eigenthings_real = torch.load(eval_file, map_location='cpu') 
        eigenvals_real = eigenthings_real['eigenvals']
        print(eigenvals_real)
        eigenvecs_real = eigenthings_real['eigenvecs']
    else:
        eigenvals_real, eigenvecs_real = None, None
    
    tasks(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)
    
if __name__ == '__main__':
    main()