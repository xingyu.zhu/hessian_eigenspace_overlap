import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, utau_eig_propagation, samples, misc, full_hessian, correlation
from tools import file_fetch

dirc = '../CIFAR10_Exp1/experiments/LeNet5_AdamDefault'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_Exp1/experiments/FC2_narrow_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_narrow30_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_narrow30_fixlr0.01_RL'
dirc = '../MNIST_TrueBinary/experiments/FC1_20_small_fixlr0.01_pn1'
#dirc = '../MNIST_TrueBinary/experiments/FC1_20_micro_fixlr0.01_pn1'
#dirc = '../MNIST_TrueBinary/experiments/FC2_20_small_fixlr0.01_pn1'
#dirc = '../MNIST_Binary/experiments/FC1_600_sgd0.01m0.9LS_l1d_pic01_labelpn1_bt100'
#dirc = '../MNIST_Binary/experiments/FC2_600_sgd0.01m0.9LS_l1d_pic01_labelpn1_bt100'
#dirc = '../MNIST_Binary/experiments/FC1_600_sgd0.01m0.9LS_l1d_pic01_labelpn1_bt100_RL'
dirc = '../MNIST_Exp1/experiments/FC2_600_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_0M/experiments/FC2_200_0M_fixlr0.01'
dirc = '../MNIST_0M/experiments/FC2S_20_0M_fixlr0.01'
dirc = '../MNIST_TB_0M/experiments/FC1S_20_0M_fixlr0.01_pn1'
dirc = '../MNIST_ExpBN/experiments/FC2BN_nl_fixlr0.01'
epoch = -1
run = 1
dim_start = 130
dim_end = 170
storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))
comp_layer = 'fc2'
load_evals = False
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3']
#comp_layers = ['fc1', 'fc2']
#fc_layers = ['fc1', 'fc2']
#comp_layers = ['fc1', 'fc2', 'fc3', 'fc4']
#fc_layers = ['fc1', 'fc2', 'fc3', 'fc4']

def tasks_component_compare(HM, eigenvals_real, eigenvecs_real, exp_name):

    component_compare.matvis_E_UTAU_xxT(HM, comp_layers, exp_name)
    return

def tasks_propagate(HM, eigenvals_real, eigenvecs_real, exp_name):

    # utau_eig_propagation.A_eig_prop(HM, comp_layers, exp_name, crop_ratio=crop_ratio)
    return

def tasks(HM, eigenvals_real, eigenvecs_real, exp_name):
    tasks_component_compare(HM, eigenvals_real, eigenvecs_real, exp_name)
    #tasks_propagate(HM, eigenvals_real, eigenvecs_real, exp_name)
    # component_compare.print_E_C(HM, comp_layers, exp_name, crop_ratio=0.001)

def main():
    snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
    if load_evals:
        eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_ET')

    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    if remain_labels is not None:
        exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

    if load_evals:
        eigenthings_real = torch.load(eval_file, map_location='cpu') 
        eigenvals_real = eigenthings_real['eigenvals']
        #print(eigenvals_real)
        eigenvecs_real = eigenthings_real['eigenvecs']
    else:
        eigenvals_real, eigenvecs_real = None, None
    
    #tasks(HM, eigenvals_real, eigenvecs_real, exp_name)
    #full_hessian.hessian_layerwise(HM, comp_layers, exp_name, storage_path)
    #full_hessian.hessian_layerwise_approx(HM, comp_layers, exp_name, storage_path)
    #full_hessian.UTAU_xxT_comp(HM, comp_layers, exp_name, storage_path)
    #full_hessian.full_hessian(HM, exp_name, storage_path)
    #correlation.xxT_UTAU_covariance(HM, comp_layer, dim_start, dim_end, exp_name, auto_grad=True)
    #correlation.xxT_UTAU_overlap_covariance(HM, comp_layer, dim_start, dim_end, exp_name, auto_grad=True)

if __name__ == '__main__':
    main()