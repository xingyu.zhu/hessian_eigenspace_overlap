import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

from torch.utils.data import DataLoader
from torch.nn import functional, Conv2d

from algos.closeformhessian.visualization import vis
from algos.closeformhessian.measures import Measures
from algos.closeformhessian.layerwisefeature import IntermediateFeatureExtractor, rgetattr
from algos.closeformhessian.dp import OnDeviceDataLoader
from algos.closeformhessian.utils import *

dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
epoch = 100
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
bn_layers = ['fc1_bn', 'fc2_bn']
fc_layers = ['fc1', 'fc2', 'fc3']

def main():
    snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
    if load_evals:
        eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_ET')

    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    if remain_labels is not None:
        exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

    if load_evals:
        eigenthings_real = torch.load(eval_file, map_location='cpu') 
        eigenvals_real = eigenthings_real['eigenvals']
        print(eigenvals_real)
        eigenvecs_real = eigenthings_real['eigenvecs']
    else:
        eigenvals_real, eigenvecs_real = None, None
    
    ife = IntermediateFeatureExtractor(net, fc_layers)
    bn_layer_modules = {layer: rgetattr(net, layer) for layer in bn_layers}
    print(bn_layer_modules)
    
    for layer in bn_layers:
        bn = bn_layer_modules[layer] # type: torch.nn.BatchNorm1d
        # bn.track_running_stats = False
        print(bn.weight, bn.bias, bn.track_running_stats)

    dl = DataLoader(dataset, batch_size=10)
    for inputs, labels in iter(dl):
        
        mid, final_out = ife(inputs)
        for layer in comp_layers:
            layer_in, layer_out = mid[layer]
            layer_in_flat = layer_in.mean()
            print(layer_in_flat)
        exit()
        

    
if __name__ == '__main__':
    main()