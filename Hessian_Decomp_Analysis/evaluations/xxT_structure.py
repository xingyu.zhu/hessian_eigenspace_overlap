import torch
import os
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def exp_cov_compare(HM: HessianModule,
                           comp_layers,
                           exp_name,
                           storage_path,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):

    xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    xs = HM.expectation(HM.decomp.x_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)

    spec_gap = {}
    fro_ratio = {}
    top_overlap = {}
    cov_spec = {}
    cov_fro = {}
    for layer in comp_layers:
        U, S, V = xxTs[layer].svd()
        S = S.to('cpu')
        spec_gap[layer] = (S[0]/S[1]).item()
        fro_ratio[layer] = (S[0]/S.square().sum().sqrt()).item()
        exp_norm = xs[layer].norm()
        top_eig_norm = U[:,0].norm()
        top_overlap[layer] = torch.dot(U[:,0], xs[layer].flatten()).div(exp_norm * top_eig_norm).abs().to('cpu').item()
        cov = xxTs[layer].sub(xs[layer].matmul(xs[layer].t()))
        _, S, _ = cov.svd(compute_uv=False)
        S = S.to('cpu')
        exp_sig = exp_norm.square().to('cpu')
        cov_spec[layer] = (exp_sig/S[0]).item()
        cov_fro[layer] = (exp_sig/cov.norm().to('cpu')).item()
        #cov_fro[layer] = (exp_sig/S.square().sum().sqrt()).item()
        #cov_fro[layer] = (xs[layer].matmul(xs[layer].t()).norm()/cov.norm()).to('cpu').item()
    print('spec_gap:{}'.format(spec_gap))
    print('fro_ratio:{}'.format(fro_ratio))
    print('top_overlap:{}'.format(top_overlap))
    print('cov_spec:{}'.format(cov_spec))
    print('cov_fro:{}'.format(cov_fro))
    out_dict = {}
    out_dict['spec_gap'] = spec_gap
    out_dict['fro_ratio'] = fro_ratio
    out_dict['top_overlap'] = top_overlap
    out_dict['cov_spec'] = cov_spec
    out_dict['cov_fro'] = cov_fro
    storage_file = os.path.join(storage_path, 'xxT_struct.pth')
    torch.save(out_dict, storage_file)
    print('saved to {}'.format(storage_file))
    return out_dict