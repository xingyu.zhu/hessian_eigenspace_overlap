import torch
import os
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule
from collections import OrderedDict

def xxT_UTAU_covariance(HM: HessianModule, layer, dim_start, dim_end, exp_name, auto_grad=False):
    layers = [layer]
    xxT = HM.expectation(HM.decomp.xxT_comp, layers, out_device=HM.device, print_log=False)
    UTAU = HM.expectation(HM.decomp.UTAU_comp, layers, out_device=HM.device, print_log=False, auto_grad=auto_grad)
    U, _, _ = UTAU[layer].svd()
    vec_UTAU = U[:,0].unsqueeze(-1)
    U, _, _ = xxT[layer].svd()
    vec_xs = [U[:, 0].unsqueeze(-1)]
    for i in range(dim_start, dim_end):
        vec_xs.append(U[:, i].unsqueeze(-1))
    ps = torch.zeros(len(vec_xs))
    ps_cov = torch.zeros(len(vec_xs))
    p_UTAUs = 0
    p_xxTs = torch.zeros(len(vec_xs))
    
    dataloader = HM.dl
    sample_count = 0
    ret = {}
    
    for i, (inputs, labels) in enumerate(dataloader):
        inputs = inputs.to(HM.device)
        labels = labels.to(HM.device)
        sample_count += inputs.size()[0]
        UTAU = HM.comp_output(HM.decomp.UTAU_comp, layers, inputs, out_device=HM.device)[layer]
        U, S, V = UTAU.svd()
        UTAU_pv = U[:,:,0]
        p_UTAU = UTAU_pv.matmul(vec_UTAU).squeeze(-1).square()
        p_UTAUs += p_UTAU.sum()
        x = HM.comp_output(HM.decomp.x_comp, layers, inputs, out_device=HM.device)[layer].squeeze(-1)
        x = x.div(x.norm(dim=1).unsqueeze(-1))
        for j in range(len(vec_xs)):
            p_xxT = x.matmul(vec_xs[j]).squeeze(-1).square()
            p_xxTs[j] += p_xxT.sum()
            ps[j] += p_UTAU.mul(p_xxT).sum()
    
    p_UTAUs /= sample_count
    for j in range(len(vec_xs)):
        p_xxTs[j] /= sample_count
        ps[j] /= sample_count
        ps_cov[j] = ps[j] - p_UTAUs*p_xxTs[j]
    print(ps)
    print(ps_cov)

def xxT_UTAU_overlap_covariance(HM: HessianModule, layer, dim_start, dim_end, exp_name, auto_grad=False):
    layers = [layer]
    xxT = HM.expectation(HM.decomp.xxT_comp, layers, out_device=HM.device, print_log=False)
    UTAU = HM.expectation(HM.decomp.UTAU_comp, layers, out_device=HM.device, print_log=False, auto_grad=auto_grad)
    U, _, _ = UTAU[layer].svd()
    vec_UTAU = U[:,:10].unsqueeze(0)
    U, _, _ = xxT[layer].svd()
    vec_xs = [U[:,1].unsqueeze(-1)]
    for i in range(dim_start, dim_end):
        vec_xs.append(U[:, i].unsqueeze(-1))
    ps = torch.zeros(len(vec_xs))
    ps_cov = torch.zeros(len(vec_xs))
    p_UTAUs = 0
    p_xxTs = torch.zeros(len(vec_xs))
    
    dataloader = HM.dl
    sample_count = 0
    ret = {}
    
    for i, (inputs, labels) in enumerate(dataloader):
        inputs = inputs.to(HM.device)
        labels = labels.to(HM.device)
        sample_count += inputs.size()[0]
        UTAU = HM.comp_output(HM.decomp.UTAU_comp, layers, inputs, out_device=HM.device)[layer]
        U, S, V = UTAU.svd()
        UTAU_pv = U[:,:,:10].transpose(1,2)
        p_UTAU = UTAU_pv.matmul(vec_UTAU).norm(dim=(1,2)).square()
        p_UTAUs += p_UTAU.sum()
        x = HM.comp_output(HM.decomp.x_comp, layers, inputs, out_device=HM.device)[layer].squeeze(-1)
        x = x.div(x.norm(dim=1).unsqueeze(-1))
        for j in range(len(vec_xs)):
            p_xxT = x.matmul(vec_xs[j]).squeeze(-1).square()
            p_xxTs[j] += p_xxT.sum()
            ps[j] += p_UTAU.mul(p_xxT).sum()
    
    p_UTAUs /= sample_count
    for j in range(len(vec_xs)):
        p_xxTs[j] /= sample_count
        ps[j] /= sample_count
        ps_cov[j] = ps[j] - p_UTAUs*p_xxTs[j]
    print(ps)
    print(ps_cov)