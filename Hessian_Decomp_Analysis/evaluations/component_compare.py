import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def matvis_E(HM: HessianModule,
            comp_layers,
            funcs,
            exp_name,
            labels=None,
            crop_ratio=1,
            colormap='viridis'
            ):
    """
    Visualizing the matrices computed by funcs
    """
    if labels is None:
        labels = [func.__name__[:-5] for func in funcs]
    name = "{}_{}".format("_".join(labels), exp_name)
    assert len(funcs) == len(labels)
    
    Es = {label: None for label in labels}
    descs = {label: [] for label in labels}
    mats = {label: [] for label in labels}

    for i, func in enumerate(funcs):
        Es[labels[i]] = HM.expectation(func, comp_layers, dataset_crop=crop_ratio)
    
    for layer in comp_layers:
        for i in range(len(funcs)):
            mats[labels[i]] += [Es[labels[i]][layer]]
            descs[labels[i]] += ['{}_E[{}]'.format(layer, labels[i])]
    
    mats = [mats[label] for label in labels]
    descs = [descs[label] for label in labels]
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def matvis_E_UTAU_xxT(HM: HessianModule,
                    comp_layers,
                    exp_name,
                    crop_ratio=1,
                    ):
    """
    Visualizing the E[UTAU] and E[xxT] matrices
    """
    xxT_comp = HM.decomp.xxT_comp
    UTAU_comp = HM.decomp.UTAU_comp
    E_xxTs = HM.expectation(xxT_comp, comp_layers, dscrop=crop_ratio)
    E_UTAUs = HM.expectation(UTAU_comp, comp_layers, dscrop=crop_ratio)
    mats_UTAU, mats_xxT = [], []
    descs_UTAU, descs_xxT = [], []
    name = "{}".format(exp_name)

    for layer in comp_layers:
        mats_UTAU += [E_UTAUs[layer]]
        descs_UTAU += ['{}_E[UTAU]'.format(layer)]
        mats_xxT += [E_xxTs[layer]]
        descs_xxT += ['{}_E[xxT]'.format(layer)]
    
    mats = [mats_UTAU, mats_xxT]
    descs = [descs_UTAU, descs_xxT]
    # HM.vis.plot_matsvd(mats, descs, name, colormap='viridis')
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def matvis_E_computed(HM: HessianModule,
                    comp_layers,
                    mat_dicts,
                    desc_generals,
                    exp_name,
                    ):
    """
    Visualizing the E[UTAU] and E[xxT] matrices
    """
    mats = [[] for layer in comp_layers]
    descs = [[] for layer in comp_layers]
    name = "{}".format(exp_name)

    for layer in comp_layers:
        for i, mat_dict in enumerate(mat_dicts):
            print(mat_dict)
            mats[i] += [mat_dict[layer]]
            descs[i] += ['{}_E[{}]'.format(layer, desc_generals[i])]
    # HM.vis.plot_matsvd(mats, descs, name, colormap='viridis')
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def dominate_eigenspace_overlap_trend_mats(HM: HessianModule, m1, m2, dim=1, name='overlap_tmp'):
    assert m1.shape == m2.shape
    assert len(m1.shape) == 2, 'the comparing matrices must be square to be diagonalizable'
    vals1, vecs1 = HM.utils.eigenthings_tensor_utils(m1)
    vals2, vecs2 = HM.utils.eigenthings_tensor_utils(m2)
    overlap_x = np.arange(1, min(m1.shape[1], dim + 1))
    overlap_y = HM.measure.trace_overlap_trend(vecs1, vecs2, overlap_x, HM.device)
    return overlap_x, overlap_y

def print_E_C(HM: HessianModule,
                    comp_layers,
                    exp_name,
                    crop_ratio=1,
                    ):
    """
    Visualizing the E[UTAU] and E[xxT] matrices
    """
    Exp = HM.expectation(HM.decomp.c_comp, comp_layers, out_device=HM.device, print_log=False, dscrop=crop_ratio)
    Var = HM.variance(HM.decomp.c_comp, comp_layers, out_device=HM.device, print_log=False, dscrop=crop_ratio)
    print(Exp)
    print(Var)

def matvis_E_UTU(HM: HessianModule,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        ):

    U_comp = HM.decomp.U_comp
    UTU_comp = HM.tsa.chain_comp([U_comp, U_comp], [1, 0])
    E_UTUs = HM.expectation(UTU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    mats_UTU = []
    descs_UTU = []
    name = "UTU_{}".format(exp_name)

    for layer in comp_layers:
        mats_UTU += [E_UTUs[layer]]
        descs_UTU += ['E[UTU]_{}'.format(layer)]
    
    mats = [mats_UTU]
    descs = [descs_UTU]
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def matvis_E_UTU_labels(HM: HessianModule,
                        labels,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        ):

    U_comp = HM.decomp.U_comp
    UTU_comp = HM.tsa.chain_comp([U_comp, U_comp], [1, 0])
    E_UTUs = HM.expectation(UTU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    mats_UTU = []
    descs_UTU = []
    name = "UTU_{}".format(exp_name)
    for layer in comp_layers:
        mats_UTU += [E_UTUs[layer]]
        descs_UTU += ['E[UTU]_{}'.format(layer)]
    mats = [mats_UTU]
    descs = [descs_UTU]
    for label in labels:
        HM.set_remain_labels([label])
        E_UTUs = HM.expectation(UTU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
        mats_UTU = []
        descs_UTU = []
        name = "UTU_{}".format(exp_name)
        for layer in comp_layers:
            mats_UTU += [E_UTUs[layer]]
            descs_UTU += ['E[UTU]_{}_y{}'.format(layer, label)]
        mats.append(mats_UTU)
        descs.append(descs_UTU)

    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def matvis_E_UUT_labels(HM: HessianModule,
                        labels,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        ):

    U_comp = HM.decomp.U_comp
    UUT_comp = HM.tsa.chain_comp([U_comp, U_comp], [0, 1])
    E_UUTs = HM.expectation(UUT_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    mats_UUT = []
    descs_UUT = []
    name = "UUT_{}".format(exp_name)
    for layer in comp_layers:
        mats_UUT += [E_UUTs[layer]]
        descs_UUT += ['E[UUT]_{}'.format(layer)]
    mats = [mats_UUT]
    descs = [descs_UUT]
    for label in labels:
        HM.set_remain_labels([label])
        E_UUTs = HM.expectation(UUT_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
        mats_UUT = []
        descs_UUT = []
        name = "UUT_{}".format(exp_name)
        for layer in comp_layers:
            mats_UUT += [E_UUTs[layer]]
            descs_UUT += ['E[UUT]_{}_y{}'.format(layer, label)]
        mats.append(mats_UUT)
        descs.append(descs_UUT)
        
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def matvis_E_UTAU_labels(HM: HessianModule,
                        labels,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        ):

    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    mats_UTAU = []
    descs_UTAU = []
    name = "UTAU_{}".format(exp_name)
    for layer in comp_layers:
        mats_UTAU += [E_UTAUs[layer]]
        descs_UTAU += ['E[UTAU]_{}'.format(layer)]
    mats = [mats_UTAU]
    descs = [descs_UTAU]
    for label in labels:
        HM.set_remain_labels([label])
        E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
        mats_UTAU = []
        descs_UTAU = []
        name = "UTAU_{}".format(exp_name)
        for layer in comp_layers:
            mats_UTAU += [E_UTAUs[layer]]
            descs_UTAU += ['E[UTAU]_{}_y{}'.format(layer, label)]
        mats.append(mats_UTAU)
        descs.append(descs_UTAU)

    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def cov_exp_UTAU_xxT(HM: HessianModule,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        ):


    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    Var_UTAUs = HM.variance(HM.decomp.UTAU_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    Var_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    
    UTAU_ratio = {}
    xxT_ratio = {}
    product_sum = {}
    product_square_sum = {}
    for layer in comp_layers:
        UTAU_ratio[layer] = Var_UTAUs[layer].div(E_UTAUs[layer])
        xxT_ratio[layer] = Var_xxTs[layer].div(E_xxTs[layer])
        print(UTAU_ratio[layer].max(), UTAU_ratio[layer].min(), UTAU_ratio[layer].mean())
        print(xxT_ratio[layer].max(), xxT_ratio[layer].min(), xxT_ratio[layer].mean())
        product_sum[layer] = HM.utils.kp_2d(UTAU_ratio[layer],xxT_ratio[layer]).sum()
        product_square_sum[layer] = HM.utils.kp_2d(UTAU_ratio[layer].square(),xxT_ratio[layer].square()).sum().sqrt_()

    print(product_sum)
    print(product_square_sum)

def var_UTAU_xxT(HM: HessianModule,
                        comp_layers,
                        exp_name,
                        crop_ratio=1,
                        ):



    Var_UTAUs = HM.variance(HM.decomp.UTAU_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)

    Var_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    
    var_kron_norm = {}
    for layer in comp_layers:
        var_kron_norm[layer] = HM.utils.kp_2d(Var_UTAUs[layer], Var_xxTs[layer]).sum().sqrt()

    print(var_kron_norm)

def top_eigenvector(HM: HessianModule,
                           dim: int,
                           topn: int,
                           comp_layers,
                           exp_name,
                           eigenvals_real: dict,
                           eigenvecs_real: dict,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    overlap_xs, overlap_ys = [], []
    name = 'Top_Eigenvector_rank_{}_{}'.format(exp_name, dim)

    sum_ratio = {}
    gap_ratio = {}
    square_ratio = {}
    for layer in comp_layers:
        print(layer)
        sum_ratio[layer] = []
        gap_ratio[layer] = []
        square_ratio[layer] = []
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        for i in range(dim):
            V = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            _, S, _ = V.svd(compute_uv=False)
            sum_ratio[layer].append(S[:topn].sum()/S.sum())
            gap_ratio[layer].append(S[0]/S[topn])
            square_ratio[layer].append(S[:topn].square().sum().sqrt()/S.square().sum().sqrt())
            print(sum_ratio[layer][-1], gap_ratio[layer][-1], square_ratio[layer][-1])
        sum_ratio[layer] = np.mean(sum_ratio[layer])
        gap_ratio[layer] = np.mean(gap_ratio[layer])
        square_ratio[layer] = np.mean(square_ratio[layer])
        
    print(sum_ratio)
    print(gap_ratio)
    print(square_ratio)