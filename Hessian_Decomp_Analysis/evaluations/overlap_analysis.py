import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def eigenspace_est_vs_real(HM: HessianModule,
                           dim: int,
                           comp_layers,
                           exp_name,
                           eigenvals_real: dict,
                           eigenvecs_real: dict,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    name = 'Estimate_eigenspace_overlap_{}_{}'.format(exp_name, dim)

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=dim, dataset_crop=crop_ratio)
    
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        print(H_eigenvecs_est.size(), H_eigenvecs.size())
        overlap_dim_y = HM.measure.trace_overlap_trend(H_eigenvecs, H_eigenvecs_est, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)

def singular_vec_overlap_analysis(HM: HessianModule,
                                  topn: int,
                                  comp_layers,
                                  exp_name,
                                  eigenvals_real: dict,
                                  eigenvecs_real: dict,
                                  crop_ratio=1):

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    E_xs = HM.E_x(comp_layers, dataset_crop=crop_ratio)
    E_xxTs = HM.E_xxT(comp_layers, dataset_crop=crop_ratio)
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        name = "SVOA_{}_{}_{}".format(exp_name, layer, topn)
        l_ovls, r_ovls = [], []
        for i in range(topn):
            vec_base_mat = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            vec_est_mat = HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer)
            U_base, S_base, V_base = torch.svd(vec_base_mat, compute_uv=True) # pylint: disable=no-member
            U_est, S_est, V_est = torch.svd(vec_est_mat, compute_uv=True) # pylint: disable=no-member
            for mat in [U_base, V_base, U_est, V_est]:
                mat.transpose_(0, 1)
            l_ovl = U_base[0].dot(U_est[0]) ** 2
            r_ovl = V_base[0].dot(V_est[0]) ** 2
            l_ovls.append(l_ovl.cpu().numpy())
            r_ovls.append(r_ovl.cpu().numpy())
        overlap_x = [list(range(1, topn + 1)) for i in range(2)]
        overlap_y = [l_ovls, r_ovls]
        descs = ['L Singular Vector Dot^2', 'R Singular Vector Dot^2']
        HM.vis.plots(overlap_x, overlap_y, descs, x_label='* of eigenvec', name=name, line_style='', marker_style='.')

def UTU_eigenspace_overlap(HM: HessianModule, 
                            label1: int, 
                            label2: int,
                           dim: int,
                           comp_layers,
                           exp_name,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    name = 'Estimate_UTU_eigenspace_overlap_{}_{}_y{}_y{}'.format(exp_name, dim, label1, label2)
    U_comp = HM.decomp.U_comp
    UTU_comp = HM.tsa.chain_comp([U_comp, U_comp], [1, 0])
    if label1 >= 0:
        HM.set_remain_labels([label1])
    else:
        HM.dl.reset_remain_labels()
    E_UTUs_1 = HM.expectation(UTU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    if label2 >=0:
        HM.set_remain_labels([label2])
    else:
        HM.dl.reset_remain_labels()
    E_UTUs_2 = HM.expectation(UTU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    for layer in comp_layers:
        _, eigenvecs1 = HM.utils.eigenthings_tensor_utils(E_UTUs_1[layer], device=HM.device, symmetric=True)
        _, eigenvecs2 = HM.utils.eigenthings_tensor_utils(E_UTUs_2[layer], device=HM.device, symmetric=True)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        overlap_dim_y = HM.measure.trace_overlap_trend(eigenvecs1, eigenvecs2, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)

def UTAU_eigenspace_overlap(HM: HessianModule, 
                            label1: int, 
                            label2: int,
                           dim: int,
                           comp_layers,
                           exp_name,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    name = 'Estimate_UTAU_eigenspace_overlap_{}_{}_y{}_y{}'.format(exp_name, dim, label1, label2)
    if label1 >= 0:
        HM.set_remain_labels([label1])
    else:
        HM.dl.reset_remain_labels()
    E_UTAUs_1 = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    if label2 >=0:
        HM.set_remain_labels([label2])
    else:
        HM.dl.reset_remain_labels()
    E_UTAUs_2 = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    for layer in comp_layers:
        _, eigenvecs1 = HM.utils.eigenthings_tensor_utils(E_UTAUs_1[layer], device=HM.device, symmetric=True)
        _, eigenvecs2 = HM.utils.eigenthings_tensor_utils(E_UTAUs_2[layer], device=HM.device, symmetric=True)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        overlap_dim_y = HM.measure.trace_overlap_trend(eigenvecs1, eigenvecs2, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)

def UTU_UTAU_eigenspace_overlap(HM: HessianModule, 
                            label: int, 
                           dim: int,
                           comp_layers,
                           exp_name,
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    name = 'Estimate_UTU_UTAU_eigenspace_overlap_{}_{}_y{}'.format(exp_name, dim, label)
    U_comp = HM.decomp.U_comp
    UTU_comp = HM.tsa.chain_comp([U_comp, U_comp], [1, 0])
    if label >= 0:
        HM.set_remain_labels([label])
    else:
        HM.dl.reset_remain_labels()
    E_UTUs = HM.expectation(UTU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, dscrop=crop_ratio, from_cache=False, to_cache=False)
    for layer in comp_layers:
        _, eigenvecs1 = HM.utils.eigenthings_tensor_utils(E_UTUs[layer], device=HM.device, symmetric=True)
        _, eigenvecs2 = HM.utils.eigenthings_tensor_utils(E_UTAUs[layer], device=HM.device, symmetric=True)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        overlap_dim_y = HM.measure.trace_overlap_trend(eigenvecs1, eigenvecs2, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)

def H_approx_eigenspace_overlap(HM: HessianModule, 
                            label: int, 
                           dim: int,
                           comp_layers,
                           exp_name,
                           y_classification_mode='softmax',
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    mats_true, mats_approx, mats_diff = [],[],[]
    descs_true, descs_approx, descs_diff = [], [], []
    name = 'H_approx_eigenspace_overlap_{}_{}_y{}'.format(exp_name, dim, label)
    if label >= 0:
        HM.set_remain_labels([label])
    else:
        HM.dl.reset_remain_labels()
    E_Hs = HM.expectation(HM.decomp.H_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False, y_classification_mode=y_classification_mode)
    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers,out_device=HM.device,  dscrop=crop_ratio, from_cache=False, to_cache=False, y_classification_mode=y_classification_mode)
    E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    ratio_diff = {}
    actual_norm = {}
    spec_norm_diff = {}
    actual_spec_norm = {}
    for layer in comp_layers:
        H_approx = HM.utils.kp_2d(E_UTAUs[layer], E_xxTs[layer]).double()
        mats_true.append(E_Hs[layer])
        descs_true.append('{}_True'.format(layer))
        mats_approx.append(H_approx)
        descs_approx.append('{}_Approx'.format(layer))
        mats_diff.append(E_Hs[layer].double().sub(H_approx))
        descs_diff.append('{}_Diff'.format(layer))
        actual_norm[layer] = torch.norm(E_Hs[layer])
        ratio_diff[layer] = torch.norm(mats_diff[-1]).div(actual_norm[layer])
        eigenvals1, eigenvecs1 = HM.utils.eigenthings_tensor_utils(E_Hs[layer].double(), device=HM.device, symmetric=True)
        eigenvals2, eigenvecs2 = HM.utils.eigenthings_tensor_utils(H_approx, device=HM.device, symmetric=True)
        actual_spec_norm[layer] = eigenvals1[0]
        spec_norm_diff[layer] = np.linalg.norm(mats_diff[-1].to('cpu').numpy(), ord=2)/actual_spec_norm[layer]
        eig_dim_x = torch.range(1, dim)
        eig_dim_ys = [eigenvals1[:dim].to('cpu'), eigenvals2[:dim].to('cpu')]
        eig_name = 'H_approx_Eigenvals_{}_{}_{}'.format(exp_name, layer, dim)
        HM.vis.plot_scatter([eig_dim_x, eig_dim_x], eig_dim_ys, ['True', 'Approx'], x_label='dim', y_label='eigenvals', name=eig_name, dpi=150)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        overlap_dim_y = HM.measure.trace_overlap_trend(eigenvecs1, eigenvecs2, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    print('Ratio fro norm: {}'.format(ratio_diff))
    print('Actual fro norm: {}'.format(actual_norm))
    print('Ratio spec norm: {}'.format(spec_norm_diff))
    print('Actual spec norm: {}'.format(actual_spec_norm))
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)
    mats = [mats_true, mats_approx, mats_diff]
    descs = [descs_true, descs_approx, descs_diff]
    name = 'H_approx_{}_{}_y{}'.format(exp_name, dim, label)
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def H_approx_class_eigenspace_overlap(HM: HessianModule, 
                           dim: int,
                           comp_layers,
                           exp_name,
                           class_num = 10,
                           y_classification_mode='softmax',
                           crop_ratio=1,
                           interval_inc_ratio=1.1
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    mats_true, mats_approx, mats_diff = [],[],[]
    descs_true, descs_approx, descs_diff = [], [], []
    name = 'H_approx_class_eigenspace_overlap_{}_{}'.format(exp_name, dim)
    H_approx = {}
    E_Hs = HM.expectation(HM.decomp.H_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False, y_classification_mode=y_classification_mode)
    for layer in comp_layers:
        H_approx[layer] = torch.zeros_like(E_Hs[layer]).double()
    for label in range(class_num):
        HM.set_remain_labels([label])
        E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False, y_classification_mode=y_classification_mode)
        E_xxTs = HM.expectation(HM.decomp.xxT_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
        for layer in comp_layers:
            H_approx[layer] = H_approx[layer].add_(1/class_num*HM.utils.kp_2d(E_UTAUs[layer], E_xxTs[layer]))
    ratio_diff = {}
    actual_norm = {}
    spec_norm_diff = {}
    actual_spec_norm = {}
    for layer in comp_layers:
        mats_true.append(E_Hs[layer].double())
        descs_true.append('{}_True'.format(layer))
        mats_approx.append(H_approx[layer])
        descs_approx.append('{}_Approx'.format(layer))
        mats_diff.append(E_Hs[layer].double().sub(H_approx[layer]))
        descs_diff.append('{}_Diff'.format(layer))
        actual_norm[layer] = torch.norm(E_Hs[layer].double())
        ratio_diff[layer] = torch.norm(mats_diff[-1]).div(actual_norm[layer])
        eigenvals1, eigenvecs1 = HM.utils.eigenthings_tensor_utils(E_Hs[layer].double(), device=HM.device, symmetric=False)
        eigenvals2, eigenvecs2 = HM.utils.eigenthings_tensor_utils(H_approx[layer], device=HM.device, symmetric=False)
        actual_spec_norm[layer] = np.linalg.norm(E_Hs[layer].double().to('cpu').numpy(), ord=2)
        spec_norm_diff[layer] = np.linalg.norm(mats_diff[-1].to('cpu').numpy(), ord=2)/actual_spec_norm[layer]
        eig_dim_x = torch.range(1, dim)
        eig_dim_ys = [eigenvals1[:dim].to('cpu'), eigenvals2[:dim].to('cpu')]
        eig_name = 'H_class_approx_Eigenvals_{}_{}_{}'.format(exp_name, layer, dim)
        HM.vis.plot_scatter([eig_dim_x, eig_dim_x], eig_dim_ys, ['True', 'Approx'], x_label='dim', y_label='eigenvals', name=eig_name, dpi=150)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        overlap_dim_y = HM.measure.trace_overlap_trend(eigenvecs1, eigenvecs2, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    print('Ratio fro norm: {}'.format(ratio_diff))
    print('Actual fro norm: {}'.format(actual_norm))
    print('Ratio spec norm: {}'.format(spec_norm_diff))
    print('Actual spec norm: {}'.format(actual_spec_norm))
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)
    mats = [mats_true, mats_approx, mats_diff]
    descs = [descs_true, descs_approx, descs_diff]
    name = 'H_approx_class_{}_{}'.format(exp_name, dim)
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)

def H_expapprox_eigenspace_overlap(HM: HessianModule, 
                                label: int, 
                                dim: int,
                                comp_layers,
                                exp_name,
                                y_classification_mode='softmax',
                                crop_ratio=1,
                                interval_inc_ratio=1.1
                                ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    dim - number of dimension to compute up to
    """
    
    overlap_xs, overlap_ys = [], []
    mats_true, mats_approx, mats_diff = [],[],[]
    descs_true, descs_approx, descs_diff = [], [], []
    name = 'H_expapprox_eigenspace_overlap_{}_{}_y{}'.format(exp_name, dim, label)
    if label >= 0:
        HM.set_remain_labels([label])
    else:
        HM.dl.reset_remain_labels()
    E_Hs = HM.expectation(HM.decomp.H_comp, comp_layers, dscrop=crop_ratio, out_device=HM.device, from_cache=False, to_cache=False, y_classification_mode=y_classification_mode)
    E_UTAUs = HM.expectation(HM.decomp.UTAU_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False, y_classification_mode=y_classification_mode)
    E_xs = HM.expectation(HM.decomp.x_comp, comp_layers, out_device=HM.device, dscrop=crop_ratio, from_cache=False, to_cache=False)
    ratio_diff = {}
    actual_norm = {}
    spec_norm_diff = {}
    actual_spec_norm = {}
    for layer in comp_layers:
        H_approx = HM.utils.kp_2d(E_UTAUs[layer], E_xs[layer].matmul(E_xs[layer].t())).double()
        mats_true.append(E_Hs[layer])
        descs_true.append('{}_True'.format(layer))
        mats_approx.append(H_approx)
        descs_approx.append('{}_Approx'.format(layer))
        mats_diff.append(E_Hs[layer].double().sub(H_approx))
        descs_diff.append('{}_Diff'.format(layer))
        actual_norm[layer] = torch.norm(E_Hs[layer])
        ratio_diff[layer] = torch.norm(mats_diff[-1]).div(actual_norm[layer])
        eigenvals1, eigenvecs1 = HM.utils.eigenthings_tensor_utils(E_Hs[layer].double(), device=HM.device, symmetric=False)
        eigenvals2, eigenvecs2 = HM.utils.eigenthings_tensor_utils(H_approx, device=HM.device, symmetric=False)
        actual_spec_norm[layer] = eigenvals1[0]
        spec_norm_diff[layer] = np.linalg.norm(mats_diff[-1].to('cpu').numpy(), ord=2)/actual_spec_norm[layer]
        eig_dim_x = torch.range(1, dim)
        eig_dim_ys = [eigenvals1[:dim].to('cpu'), eigenvals2[:dim].to('cpu')]
        eig_name = 'H_expapprox_Eigenvals_{}_{}_{}'.format(exp_name, layer, dim)
        HM.vis.plot_scatter([eig_dim_x, eig_dim_x], eig_dim_ys, ['True', 'Approx'], x_label='dim', y_label='eigenvals', name=eig_name, dpi=150)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, dim, interval_inc_ratio)
        overlap_dim_y = HM.measure.trace_overlap_trend(eigenvecs1, eigenvecs2, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    print('Ratio fro norm: {}'.format(ratio_diff))
    print('Actual fro norm: {}'.format(actual_norm))
    print('Ratio spec norm: {}'.format(spec_norm_diff))
    print('Actual spec norm: {}'.format(actual_spec_norm))
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)
    mats = [mats_true, mats_approx, mats_diff]
    descs = [descs_true, descs_approx, descs_diff]
    name = 'H_expapprox_{}_{}_y{}'.format(exp_name, dim, label)
    HM.vis.plot_matsvd_zero_centered(mats, descs, name)