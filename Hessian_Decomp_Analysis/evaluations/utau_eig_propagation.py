import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def A_eig_prop(HM: HessianModule,
               comp_layers,
               exp_name,
               crop_ratio=1,
               colormap='viridis'
               ):
    """
    propagate the eigenvectors of A to compare with that of UTAU
    """
    A = HM.E_A([comp_layers[0]], out_device=HM.device, dataset_crop=crop_ratio)
    Us = HM.E_U(comp_layers, out_device=HM.device, dataset_crop=crop_ratio)
    UTAUs = HM.E_UTAU(comp_layers, out_device=HM.device, dataset_crop=crop_ratio)
    vals_A, vecs_A = HM.utils.eigenthings_tensor_utils(A, out_device=HM.device, symmetric=True)
    overlap_xs, overlap_ys = [], []
    for layer in comp_layers:
        U, UTAU = Us[layer], UTAUs[layer]
        vals_UTAU, vecs_UTAU = HM.utils.eigenthings_tensor_utils(UTAU, out_device=HM.device, symmetric=True)
        vecs_A, U = vecs_A.to(HM.device), U.to(HM.device)
        eigA_prop = torch.mm(vecs_A, U) # pylint: disable=no-member
        eigA_prop = HM.utils.gram_schmidt(eigA_prop)        
        # eigA_prop = torch.div(eigA_prop, torch.norm_except_dim(eigA_prop, dim=0))
        overlap_x = np.arange(1, min(A.shape[0], len(eigA_prop)) + 1)
        overlap_y = HM.measure.trace_overlap_trend(eigA_prop, vecs_UTAU, overlap_x, HM.device)
        overlap_xs.append(overlap_x)
        overlap_ys.append(overlap_y)
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name="UPropA_" + exp_name, dpi=150)
        
    # print(vals_A, vecs_A)
    

def dominate_eigenspace_overlap_trend_mats(HM: HessianModule, m1, m2, dim=1, name='overlap_tmp'):
    assert m1.shape == m2.shape
    assert len(m1.shape) == 2, 'the comparing matrices must be square to be diagonalizable'
    vals1, vecs1 = HM.utils.eigenthings_tensor_utils(m1)
    vals2, vecs2 = HM.utils.eigenthings_tensor_utils(m2)
    overlap_x = np.arange(1, min(m1.shape[1], dim) + 1)
    overlap_y = HM.measure.trace_overlap_trend(vecs1, vecs2, overlap_x, HM.device)
    return overlap_x, overlap_y