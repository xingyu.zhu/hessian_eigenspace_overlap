import torch
import numpy as np
from algos.closeformhessian.hessianmodule import HessianModule

def eigenspace_est_vs_real(HM: HessianModule,
                           topn: int,
                           comp_layers,
                           exp_name,
                           eigenvals_real: dict,
                           eigenvecs_real: dict,
                           crop_ratio=1,
                           ):
    """
    Computing the trace overlap of the top-n eigenvectors of the eigenspace estimated by UTAU and xxT and the original eigenspace.
    ---
    topn - top n eigenvectors to visualize
    """

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=crop_ratio)
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est, _ = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        mats_base, descs_base = [], []
        mats_est, descs_est = [], []
        name = "Sample_{}_{}".format(exp_name, layer)
        for i in range(topn):
            mats_base.append(HM.utils.reshape_to_layer(H_eigenvecs[i], layer))
            mats_est.append(HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer))
            descs_base.append("Base:{}".format(i))
            descs_est.append("Est:{}".format(i))
        HM.vis.plot_matsvd([mats_base, mats_est], [descs_base, descs_est], name, colormap='Reds')