import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation, pac_bayes_analysis, top_eigen, xxT_structure
from tools import file_fetch

dirc = '../CIFAR10_Exp1/experiments/LeNet5_AdamDefault'
dirc = '../MNIST_Exp1/experiments/FC2_narrow30_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
#dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
#dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
#dirc = '../MNIST_Exp1/experiments/FC1_20_small_fixlr0.01'
#dirc = '../MNIST_TrueBinary/experiments/FC1_20_small_fixlr0.01_pn1'
#dirc = '../MNIST_ExpBN/experiments/FC2BN_fixlr0.01'
#dirc = '../MNIST_ExpBN/experiments/FC2BN_inpnorm_fixlr0.01'
#dirc = '../MNIST_0M/experiments/FC1S_20_0M_fixlr0.01'
#dirc = '../MNIST_TB_0M/experiments/FC1S_20_0M_fixlr0.01_pn1'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
#dirc = '../MNIST_ExpBN/experiments/FC3BN_nl_fixlr0.01'
#dirc = '../MNIST_Binary/experiments/FC2_600_sgd0.01m0.9LS_l1d_pic01_labelpn1_bt100'
#dirc = '../MNIST_Exp1/experiments/FC1_600_fixlr0.01'
#dirc = '../MNIST_Exp1/experiments/FC3_fixlr0.01'
#dirc = '../CIFAR10_Exp1/experiments/FC2_600_fixlr0.01'
#dirc = '../CIFAR10_Exp1/experiments/LeNet5_normnew_fixlr0.01'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_conv2_25'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_BN_nl_fixlr0.01'
dirc = '../MNIST_ExpBN/experiments/FC2BN_nl_fixlr0.01'
epoch = -1
run = 3

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
#comp_layers = ['fc1', 'fc2']
fc_layers = ['fc1', 'fc2', 'fc3']
#fc_layers = ['fc1', 'fc2']
#sigma_file_standard = '../../eigen_pac_bayes/tmp_log/sigma_post/run_FC2_600_final.pth'
#storage_path_run = '../../eigen_pac_bayes/tmp_log/sigma_post/run_FC2_600_final_sorted.pth'
#sigma_file_hessian = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_iterative_FC2_600_10.pth'
#storage_path = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_iterative_FC2_600_10_sorted.pth'
#storage_path_abs = '../../eigen_pac_bayes/tmp_log/sigma_post/hessian_iterative_FC2_600_10_abssorted.pth'


def tasks_component_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    component_compare.matvis_E_UTAU_xxT(HM, comp_layers, exp_name)
    return

def tasks_confidence_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    # for confidence_sq in np.arange(-3, 4, 0.5):
    #     confidence_level = np.power(10.0, confidence_sq)
    for confidence_level in [1, 5, 10, 20, 50, 100]:
        confidence_compare.matvis_E_UTAntU_conf(HM, comp_layers, exp_name, confidence=confidence_level)
    return

def tasks_propagate(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):

    # utau_eig_propagation.A_eig_prop(HM, comp_layers, exp_name, crop_ratio=crop_ratio)
    return

def tasks_UTU(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    #component_compare.matvis_E_UTU(HM, comp_layers, exp_name)
    component_compare.matvis_E_UTU_labels(HM, [0,1,2,5,7], comp_layers, exp_name)
    component_compare.matvis_E_UTAU_labels(HM, [0,1,2,5,7], comp_layers, exp_name)
    #overlap_analysis.UTU_eigenspace_overlap(HM, -1, 0, 100, comp_layers, exp_name)
    #overlap_analysis.UTU_eigenspace_overlap(HM, -1, 1, 100, comp_layers, exp_name)
    #overlap_analysis.UTU_eigenspace_overlap(HM, 0, 1, 100, comp_layers, exp_name)
    #overlap_analysis.UTU_eigenspace_overlap(HM, 1, 7, 100, comp_layers, exp_name)
    #overlap_analysis.UTU_UTAU_eigenspace_overlap(HM, -1, 100, comp_layers, exp_name)
    #overlap_analysis.UTU_UTAU_eigenspace_overlap(HM, 0, 100, comp_layers, exp_name)
    #overlap_analysis.UTU_UTAU_eigenspace_overlap(HM, 1, 100, comp_layers, exp_name)

def tasks(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    tasks_component_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)
    # tasks_confidence_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)
    #tasks_propagate(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)
    # component_compare.print_E_C(HM, comp_layers, exp_name, crop_ratio=0.001)

def main():

    snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
    if load_evals:
        eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_LW_ET')
        print(eval_file)
    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    if remain_labels is not None:
        exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

    if load_evals:
        eigenthings_real = torch.load(eval_file, map_location='cpu') 
        eigenvals_real = eigenthings_real['eigenvals_layer']
        #print(eigenvals_real)
        eigenvecs_real = eigenthings_real['eigenvecs_layer']
    else:
        eigenvals_real, eigenvecs_real = None, None
    
    #component_compare.var_UTAU_xxT(HM, comp_layers, exp_name)
    #overlap_analysis.H_approx_eigenspace_overlap(HM, -1, 3000, comp_layers, exp_name, y_classification_mode='softmax')
    #overlap_analysis.H_approx_eigenspace_overlap(HM, -1, 200, comp_layers, exp_name, y_classification_mode='softmax')
    #overlap_analysis.H_approx_class_eigenspace_overlap(HM, 200, comp_layers, exp_name, y_classification_mode='softmax')
    #component_compare.top_eigenvector(HM, 200, 1, comp_layers, exp_name, eigenvals_real, eigenvecs_real)
    #pac_bayes_analysis.sigma_post_plot_hessian_unsort(HM, sigma_file_hessian, exp_name, storage_path)
    #pac_bayes_analysis.sigma_post_plot_hessian(HM, sigma_file_hessian, exp_name, storage_path)
    #pac_bayes_analysis.sigma_post_plot_hessian_abs(HM, sigma_file_hessian, exp_name, storage_path_abs)
    #top_eigen.top_eigenvector_plot_sep(HM, 1000, 10, comp_layers, exp_name, eigenvals_real, eigenvecs_real)
    xxT_structure.exp_cov_compare(HM, comp_layers, exp_name, storage_path)
if __name__ == '__main__':
    main()