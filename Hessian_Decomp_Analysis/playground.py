import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
# from algos.closeformhessian.hessianmodule.decomposition import *

from evaluations import component_compare, utau_eig_propagation, samples, misc
from tools import file_fetch

dirc = '../CIFAR10_Exp1/experiments/LeNet5_AdamDefault'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../CIFAR5/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_Exp1/experiments/FC2_narrow_fixlr0.01'
epoch = -1
run = 2

load_evals = False
remain_labels = None
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
fc_layers = ['fc1', 'fc2', 'fc3']

def main():
    snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
    if load_evals:
        eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run)

    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    A_comp = HM.decomp.A_comp
    A_fro_norm_comp = HM.tsa.normalize_comp(A_comp)

    A_out = HM.sample_output(A_comp, ['fc1'])
    A_out_n = HM.sample_output(A_fro_norm_comp, ['fc1'])

    print(A_out)
    print(A_out_n)

    print(A_out['fc1'].norm(dim=[1,2]))
    print(A_out_n['fc1'].norm(dim=[1,2]))

    # exp_U = HM.expectation(HM.decomp.U_comp, comp_layers, out_device=HM.device)
    # cov_U = HM.covariance(HM.decomp.U_comp, comp_layers, out_device=HM.device, dim=1)
    

if __name__ == '__main__':
    main()