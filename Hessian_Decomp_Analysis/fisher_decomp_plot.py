import os, sys
import torch
import numpy as np
from algos.closeformhessian import hessianmodule
from evaluations import component_compare, confidence_compare, eigenspace_est, full_hessian, overlap_analysis, utau_eig_propagation
from tools import file_fetch

dirc = '../CIFAR10_Exp1/experiments/LeNet5_AdamDefault'
dirc = '../MNIST_Exp1/experiments/FC2_narrow30_fixlr0.01'
#dirc = '../CIFAR10_RandomLabel/experiments/LeNet5_fixlr0.01_RL'
dirc = '../CIFAR10_Exp1/experiments/LeNet5_fixlr0.01'
dirc = '../MNIST_RandomLabel/experiments/FC2_fixlr0.01_RL'
dirc = '../MNIST_Exp1/experiments/FC2_fixlr0.01'
epoch = 500
run = 1

storage_path = os.path.join(dirc, 'experiment_log/run_{}'.format(run))

load_evals = False
remain_labels = [0]
crop_ratio = 1
comp_layers = ['fc1', 'fc2', 'fc3']
#comp_layers = ['fc1', 'fc2']
fc_layers = ['fc1', 'fc2', 'fc3']

def FA_approx_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    
    A_comp = HM.decomp.A_comp
    U_comp = HM.decomp.U_comp
    FA_comp = HM.decomp.FA_comp
    UTAU_comp = HM.decomp.UTAU_comp
    UTFAU_comp = HM.tsa.chain_comp([U_comp, FA_comp, U_comp], [1, 0, 0])

    layer = comp_layers[0]
    FA_sample = HM.sample_output(FA_comp, comp_layers, sample_count=10)
    A_sample = HM.sample_output(A_comp, comp_layers, sample_count=10)
    As, A_des = [x for x in A_sample[layer]], ['A' for x in A_sample[layer]]
    FAs, FA_des = [x for x in FA_sample[layer]], ['FA' for x in FA_sample[layer]]
    HM.vis.plot_matsvd_zero_centered([As, FAs], [A_des, FA_des], exp_name + "FA_v_A_sample", dpi=100)

    component_compare.matvis_E(HM, comp_layers, [UTFAU_comp, UTAU_comp], exp_name + 'UTFAA')
    component_compare.matvis_E(HM, comp_layers, [FA_comp, A_comp], exp_name + 'FAA')
    return

def tasks(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers):
    FA_approx_compare(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)

def main():
    snapshot_file = file_fetch.fetch_snapshot(dirc, epoch=epoch, run_name=run)
    if load_evals:
        eval_file = file_fetch.fetch_eval_file(dirc, epoch=epoch, run_name=run, arg='_ET')

    assert os.path.isdir(dirc)
    sys.path.append(dirc)
    from config import Config # pylint: disable=no-name-in-module
    conf = Config()

    net = conf.net()
    net.load_state_dict(torch.load(snapshot_file, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    HM = hessianmodule.HessianModule(net, dataset, fc_layers, RAM_cap=64, remain_labels=remain_labels)

    exp_name = dirc.split('/')[-3] + '_' + dirc.split('/')[-1] + "_E{}".format(epoch)
    dataset = conf.dataset(train=True, transform=conf.test_transform)
    if remain_labels is not None:
        exp_name += '_y' + ''.join([str(i) for i in remain_labels]) # pylint: disable=not-an-iterable

    if load_evals:
        eigenthings_real = torch.load(eval_file, map_location='cpu') 
        eigenvals_real = eigenthings_real['eigenvals']
        print(eigenvals_real)
        eigenvecs_real = eigenthings_real['eigenvecs']
    else:
        eigenvals_real, eigenvecs_real = None, None
    
    tasks(HM, eigenvals_real, eigenvecs_real, exp_name, comp_layers)
    
if __name__ == '__main__':
    main()