import torch
import torch.nn as nn
import torch.nn.functional as F

class FC3_200_0M(nn.Module):
    def __init__(self):
        super(FC3_200_0M, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc2    = nn.Linear(200, 200)
        self.fc3    = nn.Linear(200, 200)
        self.fc4    = nn.Linear(200, 10)

    def forward(self, x: torch.Tensor):
        out = x.view(x.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc3(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc4(out)
        return out

class FC2_200_0M(nn.Module):
    def __init__(self):
        super(FC2_200_0M, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc2    = nn.Linear(200, 200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x: torch.Tensor):
        out = x.view(x.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out

class FC2_200_0M_inporig(nn.Module):
    def __init__(self):
        """Do not apply zero mean to image input"""
        super(FC2_200_0M_inporig, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc2    = nn.Linear(200, 200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x: torch.Tensor):
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out

class FC1S_20_0M(nn.Module):
    def __init__(self):
        super(FC1S_20_0M, self).__init__()
        self.fc1    = nn.Linear(14*14, 20)
        self.fc2    = nn.Linear(20, 10)

    def forward(self, x: torch.Tensor):
        x   = F.max_pool2d(x, 2)
        out = x.view(x.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc2(out)
        return out

class FC2S_20_0M(nn.Module):
    def __init__(self):
        super(FC2S_20_0M, self).__init__()
        self.fc1    = nn.Linear(14*14, 20)
        self.fc2    = nn.Linear(20, 20)
        self.fc3    = nn.Linear(20, 10)

    def forward(self, x: torch.Tensor):
        x   = F.max_pool2d(x, 2)
        out = x.view(x.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out

class FC2S_40_0M(nn.Module):
    def __init__(self):
        super(FC2S_40_0M, self).__init__()
        self.fc1    = nn.Linear(14*14, 40)
        self.fc2    = nn.Linear(40, 40)
        self.fc3    = nn.Linear(40, 10)

    def forward(self, x: torch.Tensor):
        x   = F.max_pool2d(x, 2)
        out = x.view(x.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out

class FC2S_20_0M_inporig(nn.Module):
    def __init__(self):
        super(FC2S_20_0M_inporig, self).__init__()
        self.fc1    = nn.Linear(14*14, 20)
        self.fc2    = nn.Linear(20, 20)
        self.fc3    = nn.Linear(20, 10)

    def forward(self, x: torch.Tensor):
        x   = F.max_pool2d(x, 2)
        out = x.view(x.size(0), -1)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out
