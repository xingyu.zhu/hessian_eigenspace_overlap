import torch
import torch.nn as nn
import torch.nn.functional as F

class FC3_bn(nn.Module):
    def __init__(self):
        super(FC3_bn, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200)
        self.fc3    = nn.Linear(200, 100)
        self.fc3_bn = nn.BatchNorm1d(200)
        self.fc4    = nn.Linear(100, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3_bn(F.relu(self.fc3(out)))
        out = self.fc4(out)
        return out

class FC2_bn(nn.Module):
    def __init__(self):
        super(FC2_bn, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_bn_inpnorm(nn.Module):
    def __init__(self):
        super(FC2_bn_inpnorm, self).__init__()
        self.fc0_bn = nn.BatchNorm1d(28*28)
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc0_bn(out)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

