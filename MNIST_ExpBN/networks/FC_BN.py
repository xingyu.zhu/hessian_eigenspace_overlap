import torch
import torch.nn as nn
import torch.nn.functional as F

class FC4_600_bn(nn.Module):
    def __init__(self):
        super(FC4_600_bn, self).__init__()
        self.fc1    = nn.Linear(28*28, 600)
        self.fc1_bn = nn.BatchNorm1d(600)
        self.fc2    = nn.Linear(600, 600)
        self.fc2_bn = nn.BatchNorm1d(600)
        self.fc3    = nn.Linear(600, 600)
        self.fc3_bn = nn.BatchNorm1d(600)
        self.fc4    = nn.Linear(600, 600)
        self.fc4_bn = nn.BatchNorm1d(600)
        self.fc5    = nn.Linear(600, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3_bn(F.relu(self.fc3(out)))
        out = self.fc4_bn(F.relu(self.fc4(out)))
        out = self.fc5(out)
        return out

class FC2_600_bn(nn.Module):
    def __init__(self):
        super(FC2_600_bn, self).__init__()
        self.fc1    = nn.Linear(28*28, 600)
        self.fc1_bn = nn.BatchNorm1d(600)
        self.fc2    = nn.Linear(600, 600)
        self.fc2_bn = nn.BatchNorm1d(600)
        self.fc3    = nn.Linear(600, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_bn(nn.Module):
    def __init__(self):
        super(FC2_bn, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_ln(nn.Module):
    def __init__(self):
        super(FC2_ln, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_ln = nn.LayerNorm(200)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_ln = nn.LayerNorm(200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_ln(F.relu(self.fc1(out)))
        out = self.fc2_ln(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_in(nn.Module):
    def __init__(self):
        super(FC2_in, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_in = nn.InstanceNorm1d(200)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_in = nn.InstanceNorm1d(200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_in(F.relu(self.fc1(out)))
        out = self.fc2_in(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_20_bn(nn.Module):
    def __init__(self):
        super(FC2_20_bn, self).__init__()
        self.fc1    = nn.Linear(14*14, 20)
        self.fc1_bn = nn.BatchNorm1d(20)
        self.fc2    = nn.Linear(20, 20)
        self.fc2_bn = nn.BatchNorm1d(20)
        self.fc3    = nn.Linear(20, 10)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_20_bn_nl(nn.Module):
    def __init__(self):
        super(FC2_20_bn_nl, self).__init__()
        self.fc1    = nn.Linear(14*14, 20)
        self.fc1_bn = nn.BatchNorm1d(20, affine=False)
        self.fc2    = nn.Linear(20, 20)
        self.fc2_bn = nn.BatchNorm1d(20, affine=False)
        self.fc3    = nn.Linear(20, 10)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_40_bn(nn.Module):
    def __init__(self):
        super(FC2_40_bn, self).__init__()
        self.fc1    = nn.Linear(14*14, 40)
        self.fc1_bn = nn.BatchNorm1d(40)
        self.fc2    = nn.Linear(40, 40)
        self.fc2_bn = nn.BatchNorm1d(40)
        self.fc3    = nn.Linear(40, 10)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_40_bn_nl(nn.Module):
    def __init__(self):
        super(FC2_40_bn_nl, self).__init__()
        self.fc1    = nn.Linear(14*14, 40)
        self.fc1_bn = nn.BatchNorm1d(40, affine=False)
        self.fc2    = nn.Linear(40, 40)
        self.fc2_bn = nn.BatchNorm1d(40, affine=False)
        self.fc3    = nn.Linear(40, 10)

    def forward(self, x):
        out = F.max_pool2d(x, 2)
        out = out.view(out.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_bn_nl(nn.Module):
    def __init__(self):
        super(FC2_bn_nl, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200, affine=False)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200, affine=False)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_bn_nl_inpnorm(nn.Module):
    def __init__(self):
        super(FC2_bn_nl_inpnorm, self).__init__()
        self.fc0_bn = nn.BatchNorm1d(28*28, affine=False)
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200, affine=False)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200, affine=False)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = self.fc0_bn(x.view(x.size(0), -1))
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC3_bn_nl(nn.Module):
    def __init__(self):
        super(FC3_bn_nl, self).__init__()
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200, affine=False)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200, affine=False)
        self.fc3    = nn.Linear(200, 200)
        self.fc3_bn = nn.BatchNorm1d(200, affine=False)
        self.fc4    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3_bn(F.relu(self.fc3(out)))
        out = self.fc4(out)
        return out

class FC2_bn_inpnorm(nn.Module):
    def __init__(self):
        super(FC2_bn_inpnorm, self).__init__()
        self.fc0_bn = nn.BatchNorm1d(28*28)
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_bn = nn.BatchNorm1d(200)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_bn = nn.BatchNorm1d(200)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = x.view(x.size(0), -1)
        out = self.fc0_bn(out)
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_ln_nl_inpnorm(nn.Module):
    def __init__(self):
        super(FC2_ln_nl_inpnorm, self).__init__()
        self.fc0_ln = nn.LayerNorm(28*28, elementwise_affine=False)
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_ln = nn.LayerNorm(200, elementwise_affine=False)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_ln = nn.LayerNorm(200, elementwise_affine=False)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = self.fc0_ln(x.view(x.size(0), -1))
        out = self.fc1_ln(F.relu(self.fc1(out)))
        out = self.fc2_ln(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class FC2_in_nl_inpnorm(nn.Module):
    def __init__(self):
        super(FC2_in_nl_inpnorm, self).__init__()
        self.fc0_in = nn.InstanceNorm1d(28*28, affine=False)
        self.fc1    = nn.Linear(28*28, 200)
        self.fc1_in = nn.InstanceNorm1d(200, affine=False)
        self.fc2    = nn.Linear(200, 200)
        self.fc2_in = nn.InstanceNorm1d(200, affine=False)
        self.fc3    = nn.Linear(200, 10)

    def forward(self, x):
        out = self.fc0_in(x.view(x.size(0), -1))
        out = self.fc1_in(F.relu(self.fc1(out)))
        out = self.fc2_in(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out