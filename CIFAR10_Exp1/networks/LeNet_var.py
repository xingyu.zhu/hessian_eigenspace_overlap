import torch
import torch.nn as nn
import torch.nn.functional as F

class LeNet5(nn.Module):
    def __init__(self):
        super(LeNet5, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class LeNet5_mod(nn.Module):
    def __init__(self, conv1=6, conv2=16, fc1=120, fc2=84):
        super(LeNet5_mod, self).__init__()
        self.conv1 = nn.Conv2d(3, conv1, 5)
        self.conv2 = nn.Conv2d(conv1, conv2, 5)
        self.fc1   = nn.Linear(conv2*5*5, fc1)
        self.fc2   = nn.Linear(fc1, fc2)
        self.fc3   = nn.Linear(fc2, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

def fc1_100():
    return LeNet5_mod(fc1=100)

def fc1_150():
    return LeNet5_mod(fc1=150)

def fc1_80():
    return LeNet5_mod(fc1=80)

def conv1_10():
    return LeNet5_mod(conv1=10)

def conv2_25():
    return LeNet5_mod(conv2=25)

def conv2_10():
    return LeNet5_mod(conv2=10)