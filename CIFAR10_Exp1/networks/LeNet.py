import torch
import torch.nn as nn
import torch.nn.functional as F

class LeNet5(nn.Module):
    def __init__(self):
        super(LeNet5, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class LeNet5_sigmoid(nn.Module):
    def __init__(self):
        super(LeNet5_sigmoid, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.sigmoid(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.sigmoid(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.sigmoid(self.fc1(out))
        out = F.sigmoid(self.fc2(out))
        out = self.fc3(out)
        return out

class LeNet5_tanh(nn.Module):
    def __init__(self):
        super(LeNet5_tanh, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.tanh(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.tanh(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.tanh(self.fc1(out))
        out = F.tanh(self.fc2(out))
        out = self.fc3(out)
        return out

class LeNet5_BN_nl(nn.Module):
    def __init__(self):
        super(LeNet5_BN_nl, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.bn_conv1 = nn.BatchNorm2d(6, affine=False)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.bn_fc0 = nn.BatchNorm1d(16*5*5, affine=False)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.bn_fc1 = nn.BatchNorm1d(120, affine=False)
        self.fc2   = nn.Linear(120, 84)
        self.bn_fc2 = nn.BatchNorm1d(84, affine=False)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = self.bn_conv1(F.max_pool2d(out, 2))
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = self.bn_fc0(out.view(out.size(0), -1))
        out = self.bn_fc1(F.relu(self.fc1(out)))
        out = self.bn_fc2(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class LeNet5_BN(nn.Module):
    def __init__(self):
        super(LeNet5_BN, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv1_bn = nn.BatchNorm2d(6)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc0_bn = nn.BatchNorm1d(16*5*5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc1_bn = nn.BatchNorm1d(120)
        self.fc2   = nn.Linear(120, 84)
        self.fc2_bn = nn.BatchNorm1d(84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = self.conv1_bn(F.max_pool2d(out, 2))
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = self.fc0_bn(out.view(out.size(0), -1))
        out = self.fc1_bn(F.relu(self.fc1(out)))
        out = self.fc2_bn(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class LeNet5_LN_nl(nn.Module):
    def __init__(self):
        super(LeNet5_LN_nl, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.ln_conv1 = nn.LayerNorm((6, 14,14),  elementwise_affine=False)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.ln_fc0 = nn.LayerNorm(16*5*5,  elementwise_affine=False)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.ln_fc1 = nn.LayerNorm(120,  elementwise_affine=False)
        self.fc2   = nn.Linear(120, 84)
        self.ln_fc2 = nn.LayerNorm(84,  elementwise_affine=False)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = self.ln_conv1(F.max_pool2d(out, 2))
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = self.ln_fc0(out.view(out.size(0), -1))
        out = self.ln_fc1(F.relu(self.fc1(out)))
        out = self.ln_fc2(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class LeNet5_IN_nl(nn.Module):
    def __init__(self):
        super(LeNet5_IN_nl, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.in_conv1 = nn.InstanceNorm2d(6, affine=False)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.bn_fc0 = nn.BatchNorm1d(16*5*5, affine=False)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.bn_fc1 = nn.BatchNorm1d(120, affine=False)
        self.fc2   = nn.Linear(120, 84)
        self.bn_fc2 = nn.BatchNorm1d(84, affine=False)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = self.in_conv1(F.max_pool2d(out, 2))
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = self.bn_fc0(out.view(out.size(0), -1))
        out = self.bn_fc1(F.relu(self.fc1(out)))
        out = self.bn_fc2(F.relu(self.fc2(out)))
        out = self.fc3(out)
        return out

class LeNet5_0M(nn.Module):
    def __init__(self):
        super(LeNet5_0M, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = x
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.conv1(out))
        out = F.max_pool2d(out, 2)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out

class LeNet5_0M_inporig(nn.Module):
    def __init__(self):
        super(LeNet5_0M_inporig, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = x
        out = F.relu(self.conv1(out))
        out = F.max_pool2d(out, 2)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc1(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = F.relu(self.fc2(out))
        out = out - out.mean(dim=0, keepdim=True)
        out = self.fc3(out)
        return out

class LeNet5_4fc(nn.Module):
    def __init__(self):
        super(LeNet5_4fc, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 84)
        self.fc4   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = F.relu(self.fc3(out))
        out = self.fc4(out)
        return out

class LeNet5_2x(nn.Module):
    def __init__(self):
        super(LeNet5_2x, self).__init__()
        self.conv1 = nn.Conv2d(3, 12, 5)
        self.conv2 = nn.Conv2d(12, 32, 5)
        self.fc1   = nn.Linear(32*5*5, 240)
        self.fc2   = nn.Linear(240, 120)
        self.fc3   = nn.Linear(120, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class LeNet4(nn.Module):
    def __init__(self):
        super(LeNet4, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = self.fc2(out)
        return out

class LeNet6(nn.Module):
    def __init__(self):
        super(LeNet6, self).__init__()
        self.conv1 = nn.Conv2d(3, 12, 5)
        self.conv2 = nn.Conv2d(12, 32, 5)
        self.conv3 = nn.Conv2d(32, 64, 2, 2)
        self.fc1   = nn.Linear(64*5*5, 240)
        self.fc2   = nn.Linear(240, 120)
        self.fc3   = nn.Linear(120, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.relu(self.conv3(out))
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class LeNet6_narrow(nn.Module):
    def __init__(self):
        super(LeNet6_narrow, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.conv3 = nn.Conv2d(16, 16, 2, 2)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 10)

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.relu(self.conv3(out))
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = self.fc3(out)
        return out

class LeNet5_nofc3(nn.Module):
    def __init__(self, W):
        super(LeNet5_nofc3, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.W = W

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = torch.matmul(out, self.W)
        return out

class LeNet5_nofc3_id(nn.Module):
    def __init__(self, l):
        super(LeNet5_nofc3_id, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1   = nn.Linear(16*5*5, 120)
        self.fc2   = nn.Linear(120, 84)
        self.l = l

    def forward(self, x):
        out = F.relu(self.conv1(x))
        out = F.max_pool2d(out, 2)
        out = F.relu(self.conv2(out))
        out = F.max_pool2d(out, 2)
        out = out.view(out.size(0), -1)
        out = F.relu(self.fc1(out))
        out = F.relu(self.fc2(out))
        out = torch.index_select(out, -1, self.l)
        return out

def lenet5_nofc3(W):
    return LeNet5_nofc3(W)

def lenet5_nofc3_id(l):
    return LeNet5_nofc3_id(l)