#!/bin/bash
for i in `seq $1 $2`
do
    echo $i
    python3 sbatch_run.py  --exclude="gpu-compute[1-3]" -x="train-m" -r="python3 eval_epoch.py -n=400 -m='./experiment_log/run_$i/models/final.pth' -lw" --specialinfo="lw_eval"
    sleep 1
done

