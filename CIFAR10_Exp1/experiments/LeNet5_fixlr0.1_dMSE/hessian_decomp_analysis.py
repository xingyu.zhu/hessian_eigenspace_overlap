import os, sys
import torch
import numpy as np
sys.path.append(os.getcwd())
from config import Config # pylint: disable=no-name-in-module
from collections import OrderedDict
from algos.closeformhessian import hessianmodule

sd = "./experiment_log/run_2/models/final.pth"
real_comp = "./experiment_log/run_2/models/final.pth_LW_ET1000.eval"
exp_name = os.getcwd().split('/')[-1]
device = 'cuda'
conf = Config()
net = conf.net()
net.load_state_dict(torch.load(sd, map_location='cuda' if torch.cuda.is_available() else 'cpu'))
dataset = conf.dataset(train=True, transform=conf.test_transform)
HM = hessianmodule.HessianModule(net, dataset, ['fc1', 'fc2', 'fc3'], RAM_cap=64)
comp_layers = ['fc1', 'fc2', 'fc3']
#comp_layers = ['fc1']
# comp_layers = ['fc3']

eigenthings_real = torch.load(real_comp, map_location='cpu')
eigenvals_real = eigenthings_real['eigenvals_layer']
eigenvecs_real = eigenthings_real['eigenvecs_layer']

def overlap_analysis_layer(n, comp_layers):
    overlap_xs, overlap_ys = [], []
    name = 'Estimate_eigenspace_overlap_{}_{}'.format(exp_name, n)

    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=n, dataset_crop=1)
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        overlap_dim_x = HM.utils.increasing_interval_dist(1, n, r=1.05)
        print(overlap_dim_x)
        overlap_dim_y = HM.measure.trace_overlap_trend(H_eigenvecs, H_eigenvecs_est, overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
    
    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name, dpi=150)

def sample_eigenvectors(topn, comp_layers):
    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=1)
    mats = []
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        mats_base, descs_base = [], []
        mats_est, descs_est = [], []
        name = "Sample_{}_{}".format(exp_name, layer)
        for i in range(topn):
            mats_base.append(HM.utils.reshape_to_layer(H_eigenvecs[i], layer))
            mats_est.append(HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer))
            descs_base.append("Base:{}".format(i))
            descs_est.append("Est:{}".format(i))
        HM.vis.plot_matsvd([mats_base, mats_est], [descs_base, descs_est], name, colormap='Reds')

def singular_vec_overlap_analysis(topn, comp_layers):
    eigenthings_est = HM.hessian_eigenthings_estimate(comp_layers, num_eigenthings=topn, dataset_crop=1)
    E_xs = HM.E_x(comp_layers, dataset_crop=1)
    mats = []
    for layer in comp_layers:
        H_eigenvals_est, H_eigenvecs_est = eigenthings_est[layer]
        layer_weight_ind = layer + '.weight'
        H_eigenvals = eigenvals_real[layer_weight_ind]
        H_eigenvecs = torch.from_numpy(eigenvecs_real[layer_weight_ind]) # pylint: disable=no-member
        overlap_x, overlap_y, descs_base = [], [], []
        name = "SVOA_{}_{}_{}".format(exp_name, layer, topn)
        l_ovls, r_ovls, r_ovl_exs = [], [], []
        for i in range(topn):
            vec_base_mat = HM.utils.reshape_to_layer(H_eigenvecs[i], layer)
            vec_est_mat = HM.utils.reshape_to_layer(H_eigenvecs_est[i], layer)
            U_base, S_base, V_base = torch.svd(vec_base_mat, compute_uv=True) # pylint: disable=no-member
            U_est, S_est, V_est = torch.svd(vec_est_mat, compute_uv=True) # pylint: disable=no-member
            U_base.transpose_(0, 1)
            V_base.transpose_(0, 1)
            U_est.transpose_(0, 1)
            V_est.transpose_(0, 1)
            E_x = E_xs[layer][0]
            E_x /= torch.norm(E_x)
            l_ovl = U_base[0].dot(U_est[0]) ** 2
            r_ovl = V_base[0].dot(V_est[0]) ** 2
            r_ovl_ex = V_est[0].dot(E_x) ** 2
            l_ovls.append(l_ovl.cpu().numpy())
            r_ovl_exs.append(r_ovl_ex.cpu().numpy())
            r_ovls.append(r_ovl.cpu().numpy())
            # print(r_ovl, r_ovl_ex, l_ovl)
        overlap_x = [list(range(1, topn + 1)) for i in range(2)]
        overlap_y = [l_ovls, r_ovls]
        descs = ['L Singular Vector Dot^2', 'R Singular Vector Dot^2']
        HM.vis.plots(overlap_x, overlap_y, descs, x_label='* of eigenvec', name=name, line_style=None, marker_style='.')

# sample_eigenvectors(5, comp_layers)
# overlap_analysis_layer(200, comp_layers)
#singular_vec_overlap_analysis(150, comp_layers)

def UTL_UTAU_overlap(n, comp_layers):
    E_UTLs = HM.E_UTL(comp_layers, dataset_crop=1)
    E_UTAUs = HM.E_UTAU(comp_layers, dataset_crop=1)
    mats_UTAU, mats_UTLmt = [], []
    descs_UTAU, descs_UTLmt = [], []
    name_mat = "UTL_UTAU_{}".format(exp_name)
    overlap_xs, overlap_ys = [], []
    name_overlap = 'UTL_UTAU_overlap_{}_{}'.format(exp_name, n)
    for layer in comp_layers:
        E_UTL = E_UTLs[layer]
        E_UTLmt = E_UTL.matmul(E_UTL.transpose(0,1))
        E_UTAU = E_UTAUs[layer]
        _, E_UTAU_eig = torch.symeig(E_UTAU.to(device), eigenvectors=True)
        _, E_UTLmt_eig = torch.symeig(E_UTLmt.to(device), eigenvectors=True)
        overlap_dim_x = HM.utils.increasing_interval_dist(1, n, r=1.05)
        print(overlap_dim_x)
        overlap_dim_y = HM.measure.trace_overlap_trend(E_UTAU_eig.transpose(0,1).flip(0), E_UTLmt_eig.transpose(0,1).flip(0), overlap_dim_x)
        overlap_xs.append(overlap_dim_x)
        overlap_ys.append(overlap_dim_y)
        mats_UTAU += [E_UTAUs[layer]]
        descs_UTAU += ['E[U^TAU]_{}'.format(layer)]
        mats_UTLmt += [E_UTLmt]
        descs_UTLmt += ['E[U^TL]E[U^TL]^T_{}'.format(layer)]

    HM.vis.plots(overlap_xs, overlap_ys, comp_layers, x_label='dim', y_label='trace overlap', name=name_overlap, dpi=150)
    mats = [mats_UTAU, mats_UTLmt]
    descs = [descs_UTAU, descs_UTLmt]
    HM.vis.plot_matsvd(mats, descs, name_mat, colormap='viridis')

UTL_UTAU_overlap(10, comp_layers)