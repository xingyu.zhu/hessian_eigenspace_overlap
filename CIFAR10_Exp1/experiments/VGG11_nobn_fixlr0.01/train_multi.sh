for i in `seq $1 $2`
do
    echo $i
    python3 sbatch_run.py --exclude="gpu-compute[1-3]" -x="train-s" -r="python3 train.py -run=$i -o"
    sleep 1
done

#python3 sbatch_run.py  --exclude="gpu-compute[1-3]" -x="train-s" -r="python3 train.py -run=2"
#python3 sbatch_run.py  --exclude="gpu-compute[1-3]" -x="train-s" -r="python3 train.py -run=3"
#python3 sbatch_run.py  --exclude="gpu-compute[1-3]" -x="train-s" -r="python3 train.py -run=4"
#python3 sbatch_run.py  --exclude="gpu-compute[1-3]" -x="train-s" -r="python3 train.py -run=5"